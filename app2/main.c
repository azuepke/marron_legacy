/* SPDX-License-Identifier: MIT */
/*
 * test.c
 *
 * Example OS -- test code
 *
 * azuepke, 2017-07-17: initial
 */

#include <marron/api.h>
#include <stack.h>
#include <stdio.h>
#include <stdint.h>
#undef NDEBUG /* always keep assert() acivated */
#include <assert.h>
#include "config.h"
/*#define VERBOSE*/
#ifdef VERBOSE
# define vprint(fmt, ...) printf(fmt, ##__VA_ARGS__)
#else
# define vprint(fmt, ...)
#endif

/* stacks */
#define STACK_SIZE 2048
DEFINE_STACK(main_stack, STACK_SIZE);

static inline sys_timeout_t timeout_rel(sys_timeout_t rel)
{
	return sys_time_get() + rel;
}

extern uint32_t _shm1_start[];
void main(void *arg __unused)
{
	err_t err;
	sys_time_t now = sys_time_get();
	volatile uint32_t *shm1 = (volatile uint32_t *) _shm1_start;
	shm1[4]++;
	vprint("- Part 2 is in init run test. %u\n", shm1[3]);
	while (shm1[2] == 0x0) sys_sleep(timeout_rel(1000000));
	vprint("- Hello World from user space in Partition 2\n");
	switch(shm1[3]) {
		case 0:
			for (unsigned int i = 1; i <= 5; i++) {
				now = sys_time_get();
				vprint("- loop %d: now it is %lld ns since boot\n", i, now);
				sys_sleep(now + 1000*1000000);
			}
			break;
		case 1:
			vprint("- Run endless Loop\n");
			for(;;);
			break;
		case 2:
			sys_sleep(sys_time_get() + 3000ULL * 1000000ULL);
			shm1[2] = 0x0; /* lock spinlock */
			vprint("- Restart my partition\n");
			sys_part_restart();
			break;
		case 3:
			vprint("- wait for ping\n");
			err = sys_event_wait(CFG_EVENT_WAIT_ping, 0, sys_time_get() + 2000 * 1000000ULL);
			assert(err == EOK);
			vprint("- send pong\n");
			err = sys_event_send(CFG_EVENT_SEND_pong);
			assert(err == EOK);
			break;
		case 4:
			{
				unsigned int state;
				printf("- Test try to get State from Part 1: ");
				err = sys_part_state_other(1, &state);
				assert(err == EPERM);
				printf("EPERM\n");
				printf("- Test try to restart Part 1: ");
				err = sys_part_restart_other(1);
				assert (err == EPERM);
				printf("EPERM\n");
				printf("- Test try to restart Part 1: ");
				err = sys_part_shutdown_other(1);
				assert (err == EPERM);
				printf("EPERM\n");
				printf("- Test try to shutdown board: ");
				err = sys_bsp_shutdown(2);
				assert (err == EPERM);
				printf("EPERM\n");
				err = sys_event_send(CFG_EVENT_SEND_pong);
				assert(err == EOK);
			}
			break;
		default:
			printf("Unkown test\n");
			assert(0);
	}
	vprint("- Partition Shutdown\n");

	sys_part_shutdown();
}
