# Marron Legacy

This is a legacy version of the Marron kernel.
For the latest version, see the [Marron](https://gitlab.com/azuepke/marron/) repository.

## What is Marron

"Marron" is a small, statically configured microkernel targetting MMU
based systems. The current implementation focusses on MMU-based 32-bit ARM
Cortex-A multicore processors, e.g. Cortex A8, A9 or A15.

The Marron kernel is a C language baseline for operating systems research by
Alex Zuepke of RheinMain University of Applied Sciences https://www.cs.hs-rm.de/
to explore SPARK Ada for teaching and research in the context of the AQUAS
(Aggregated Quality Assurance for Systems) project https://aquas-project.eu/.

This version was under active development during 2017 to 2020.

Marron is licensed under MIT license, see [LICENSE.TXT](LICENSE.TXT).

See the original [README.TXT](README.TXT) for more information.
