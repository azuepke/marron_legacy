/* SPDX-License-Identifier: MIT */
/*
 * exception.c
 *
 * Architecture specific exception handling
 *
 * azuepke, 2013-09-15: initial
 * azuepke, 2017-10-02: imported and adapted
 */

#include <arch.h>
#include <panic.h>
#include <kernel.h>
#include <bsp.h>
#include <assert.h>
#include <stdio.h>
#include <arm_private.h>
#include <arm_insn.h>
#include <arm_cr.h>
#include <arch_context.h>
#include <user.h>
#include <arch_adspace_types.h>
#include <arm_perf.h>


__cold void arch_dump_registers(struct regs *regs)
{
	assert(regs != NULL);

	printf("r0  %lx  r1  %lx  r2  %lx  r3  %lx\n",
	       regs->r[0], regs->r[1], regs->r[2], regs->r[3]);

	printf("r4  %lx  r5  %lx  r6  %lx  r7  %lx\n",
	       regs->r[4], regs->r[5], regs->r[6], regs->r[7]);

	printf("r8  %lx  r9  %lx  r10 %lx  r11 %lx\n",
	       regs->r[8], regs->r[9], regs->r[10], regs->r[11]);

	printf("r12 %lx  sp  %lx  lr  %lx  pc  %lx\n",
	       regs->r[12], regs->sp, regs->lr, regs->pc);

	printf("psr %lx  fsr %lx addr %lx  tls %lx\n",
	       regs->cpsr, regs->fsr, regs->addr, regs->tls1);
}

void arm_handler_undef_user(struct regs *regs)
{
	unsigned long fpexc;
	unsigned long inst;
	unsigned long info;

	assert((regs->cpsr & CPSR_MODE_MASK) == CPSR_MODE_USER);

	inst = 0;
	(void)user_copy_in(&inst, (void*)regs->pc, 4);

	regs->fsr = 0;
	regs->addr = inst;

	/* SIGILL by default */
	info = EX_TYPE_ILLEGAL;
	fpexc = arm_get_fpexc();
	if (fpexc & FPEXC_EN) {
		/* clear VFP excetions */
		if (fpexc & FPEXC_EX) {
			arm_set_fpexc(FPEXC_EN);
			/* raise SIGFPE */
			info = EX_TYPE_FPU_ERROR;
		}
	} else if (regs->cpsr & CPSR_T) {
		/* decode thumb opcode:
		 * - VFP load store
		 * - VFP data op
		 * - coprocessor transfer to c10/c11
		 */
		if (((inst & 0x0000ff10) == 0x0000f900) ||
		    ((inst & 0x0000ef00) == 0x0000ef00) ||
		    ((inst & 0x0e00fc00) == 0x0a00ec00))
		{
			/* FPU is disabled */
			info = EX_TYPE_FPU_UNAVAIL;
		}
	} else {
		/* decode ARM opcode:
		 * - VFP load store
		 * - VFP data op
		 * - coprocessor transfer to c10/c11
		 */
		if (((inst & 0xff100000) == 0xf4000000) ||
		    ((inst & 0xfe000000) == 0xf2000000) ||
		    ((inst & 0x0c000e00) == 0x0c000a00))
		{
			/* FPU is disabled */
			info = EX_TYPE_FPU_UNAVAIL;
		}
	}
	kernel_exception(regs, info);
}

void arm_handler_undef_kern(struct regs *regs)
{
	assert((regs->cpsr & CPSR_MODE_MASK) == CPSR_MODE_SVC);

	panic_regs(regs, "KERNEL UNDEF\n");
}

#ifdef ARM_LPAE
/*
 * ARMv7-LPAE FSR encoding:
 * 0001 LL  Translation fault
 * 0010 LL  Access fault
 * 0011 LL  Permission fault
 * 0100 00  Synchronous external abort
 * 0110 00  Synchronous parity error
 * 0100 01  Asynchronous external abort
 * 0110 01  Asynchronous parity error
 * 0101 LL  Synchronous external abort on translation table walk
 * 0111 LL  Synchronous parity error on memory access on translation table walk
 * 1000 01  Alignment fault
 * 1000 10  Debug event
 */
void arm_handler_dabt_user(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	unsigned int status;
	unsigned long info;

	assert((regs->cpsr & CPSR_MODE_MASK) == CPSR_MODE_USER);

	regs->fsr = fsr;
	regs->addr = addr;

	status = fsr & FSR_FSMASK;

	switch (status >> 2) {
	case 0x8:
		if (status == 0x21) {
			/* alignment fault */
			info = EX_TYPE_ALIGN;
		} else {
			assert(status == 0x22);
			/* debug event */
			info = EX_TYPE_TRAP;
		}
		break;
	case 0x2:
		/* access fault */
		info = EX_TYPE_PAGEFAULT;
		break;
	case 0x3:
		/* permission fault */
		info = EX_TYPE_PAGEFAULT | EX_PERM;
		break;
	default:
		panic_regs(regs, "USER DATA ABORT at 0x%lx, fsr=%lx\n", addr, fsr);
		break;
		/* FIXME: ... add more encodings ... */
	}

	if (fsr & FSR_WRITE) {
		/* write access */
		info |= EX_WRITE;
	} else {
		/* read access */
		info |= EX_READ;
	}

	kernel_exception(regs, info);
}

void arm_handler_pabt_user(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	unsigned int status;
	unsigned long info;

	assert((regs->cpsr & CPSR_MODE_MASK) == CPSR_MODE_USER);

	regs->fsr = fsr;
	regs->addr = addr;

	status = fsr & FSR_FSMASK;

	switch (status >> 2) {
	case 0x8:
		if (status == 0x21) {
			/* alignment fault */
			info = EX_TYPE_ALIGN;
		} else {
			assert(status == 0x22);
			/* debug event */
			info = EX_TYPE_TRAP;
		}
		break;
	case 0x2:
		/* access fault */
		info = EX_TYPE_PAGEFAULT;
		break;
	case 0x3:
		/* permission fault */
		info = EX_TYPE_PAGEFAULT | EX_PERM;
		break;
	default:
		panic_regs(regs, "USER DATA PREFETCH at 0x%lx, fsr=%lx\n", addr, fsr);
		break;
		/* FIXME: ... add more encodings ... */
	}

	/* ifetch is always type exec access */
	info |= EX_EXEC;

	kernel_exception(regs, info);
}

void arm_handler_dabt_kern(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	panic_regs(regs, "KERNEL DATA ABORT: at 0x%lx, fsr=%lx\n", addr, fsr);
}

void arm_handler_pabt_kern(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	panic_regs(regs, "KERNEL PREFETCH ABORT: at 0x%lx, fsr=%lx\n", addr, fsr);
}

#else /* pre-LPAE ARMv7 version */

/*
 * The FSR status bits encoding that does not cause a panic is:
 *
 *  Status	DI	Ext	Description
 *  0 0001	D-	-	alignment fault
 *  0 0010	DI	-	debug event
 *  0 0100	D-	-	Icache maintenance fault
 *  0 01x1	DI	-	translation fault (no mapping)
 *  0 1000	DI	E	synchronous external abort (error on device access)
 *  0 11x1	DI	-	permission fault (due to APx or nX bits)
 *  1 1001	DI	E	synchronous parity error (error on device access)
 *
 * TODO:
 *  0 11?0	D?	E	L1/L2 translation precise external abort
 *  1 11?0	D?	E	L1/L2 translation precise parity error
 *  0 0011	D?	-	access fault, section
 *  0 0110	D?	-	access fault, page
 *  0 1001  D?	-	domain fault, section
 *  0 1011  D?	-	domain fault, page
 *  0 1101	D?	-	permission fault, section
 *  0 1111	D?	-	permission fault, page
 *  0 1000	D?	E	precise external abort, nontranslation
 *  1 0110	D?	E	imprecise external abort
 *  1 1000	D?	E	imprecise parity or ECC error
 */
#define FSR_PANIC	0xfdff5e49

void arm_handler_dabt_user(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	unsigned int status;
	unsigned long info;
	static int once = 1;

	assert((regs->cpsr & CPSR_MODE_MASK) == CPSR_MODE_USER);

	regs->fsr = fsr;
	regs->addr = addr;

	/* construct contiguous abort state */
	status = fsr & FSR_FSMASK;
	if (fsr & FSR_FSBIT4) {
		status |= 0x10;
	}

	if (once == 1 && FSR_PANIC & (1u << status)) {
		return;
	}
	once = 0; /* FIXME  */

	/* catch bad faults */
	if (unlikely(FSR_PANIC & (1u << status))) {
		panic_regs(regs, "USER DATA ABORT: bad fault at 0x%lx, fsr=%lx\n", addr, fsr);
	}

	if (status == 1) {
		/* alignment fault */
		info = EX_TYPE_ALIGN;
	} else if (status == 2) {
		/* debug event */
		info = EX_TYPE_TRAP;
	} else if (status & 0x4) {
		/* standard page fault or icache maintainance fault */
		if (fsr & 0x8) {
			/* permission fault */
			info = EX_TYPE_PAGEFAULT | EX_PERM;
		} else {
			/* no mapping */
			info = EX_TYPE_PAGEFAULT;
		}
	} else {
		/* synchronous external abort or parity error */
		info = EX_TYPE_BUS;
	}

	if (fsr & FSR_WRITE) {
		/* write access */
		info |= EX_WRITE;
	} else {
		/* read access */
		info |= EX_READ;
	}

	kernel_exception(regs, info);
}

void arm_handler_pabt_user(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	unsigned int status;
	unsigned long info;

	assert((regs->cpsr & CPSR_MODE_MASK) == CPSR_MODE_USER);

	regs->fsr = fsr;
	regs->addr = addr;

	/* construct contiguous abort state */
	status = fsr & FSR_FSMASK;
	if (fsr & FSR_FSBIT4) {
		status |= 0x10;
	}

	/* catch bad faults */
	if (unlikely(FSR_PANIC & (1u << status))) {
		panic_regs(regs, "USER PREFETCH ABORT: bad fault at 0x%lx, fsr=%lx\n", addr, fsr);
	}

	if (status == 2) {
		/* debug event */
		info = EX_TYPE_TRAP;
	} else if (status & 0x4) {
		/* standard page fault or icache maintainance fault */
		if (fsr & 0x8) {
			/* permission fault */
			info = EX_TYPE_PAGEFAULT | EX_PERM;
		} else {
			/* no mapping */
			info = EX_TYPE_PAGEFAULT;
		}
	} else {
		/* synchronous external abort or parity error */
		info = EX_TYPE_BUS;
	}

	/* ifetch is always type exec access */
	info |= EX_EXEC;

	kernel_exception(regs, info);
}

void arm_handler_dabt_kern(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	panic_regs(regs, "KERNEL DATA ABORT: at 0x%lx, fsr=%lx\n", addr, fsr);
}

void arm_handler_pabt_kern(struct regs *regs, ulong_t fsr, ulong_t addr)
{
	panic_regs(regs, "KERNEL PREFETCH ABORT: at 0x%lx, fsr=%lx\n", addr, fsr);
}
#endif

void arm_handler_irq(struct regs *regs __unused)
{
	bsp_irq_dispatch(0);
}

void arm_handler_fiq(struct regs *regs)
{
	panic_regs(regs, "FIQ\n");
}

static __init void arm_enable_performance_monitor(void)
{
	unsigned long ctrl;

	/*
	 * ARMv7 defines a cycle counter and up to 32 individual counters
	 * we keep the individual counters disabled
	 * but enable the cycle counter by default
	 */

	/* disable all counters, IRQs, overflows, except CCTR */
	arm_perf_disable_counter(ARM_PERF_MASK_ALL);
	arm_perf_int_mask(ARM_PERF_MASK_ALL);
	arm_perf_int_ack(ARM_PERF_MASK_ALL);

	arm_perf_enable_counter(ARM_PERF_MASK_CCNT);

	/* enable only the cycle counter */
	ctrl = arm_perf_get_ctrl();
	ctrl &= ~ARM_PERF_PMCR_D;
	ctrl |= ARM_PERF_PMCR_C | ARM_PERF_PMCR_E;
	arm_perf_set_ctrl(ctrl);

	/* enable access to performance monitor registers from user space */
	arm_perf_set_useren(ARM_PERF_USEREN);
}

__init void arch_init_exceptions(void)
{
	/* finally switch to direct vectors, if supported */
	arm_set_vbar((unsigned long)arm_vectors);

	arm_enable_performance_monitor();
}

/** change MMU page tables on context switch */
void arch_adspace_switch(const struct arch_adspace_cfg *cfg)
{
/* Only on Cortex-A */
	assert(cfg != NULL);
	/* Cortex-A15 workaround non lpae mode */
	arm_set_asid(0);
	arm_dsb();
	arm_set_ttbr0(cfg->ttbr0);
	arm_dsb();
	arm_set_asid(cfg->asid);
	arm_dsb();
	arm_isb();
}
