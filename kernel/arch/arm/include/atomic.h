/*
 * atomic.h
 *
 * ARM atomic operations.
 *
 * azuepke, 2013-09-11: initial ARM port
 * azuepke, 2015-07-14: split into two files and added 64-bit support
 */

#ifndef __ATOMIC_H__
#define __ATOMIC_H__

#ifdef __aarch64__
#include <atomic64.h>
#else
#include <atomic32.h>
#endif

/* SMP full, read, and write barriers:
 * explicit read or write barriers are not required on arm.
 * NOTE: these barriers are not suitable for device I/O ordering!
 */

static inline void smp_full_barrier(void)
{
	_arm_dmb("ISH");
}

static inline void smp_read_barrier(void)
{
#ifdef __aarch64__
	_arm_dmb("ISHLD");
#else
	_arm_dmb("ISH");
#endif
}

static inline void smp_write_barrier(void)
{
#ifdef __aarch64__
	_arm_dmb("ISHST");
#else
	_arm_dmb("ISHST");
#endif
}


/* Lock acquire and release barriers:
 * read (acquire) and write (release) barriers to be used in locks and mutex
 */

static inline void acquire_barrier(void)
{
	_arm_dmb("ISH");
}

static inline void release_barrier(void)
{
	_arm_dmb("ISH");
}

/* I/O write barrier:
 * to be used when ordering writes to memory mapped devices
 * think of "flush write buffer"
 */

static inline void io_write_barrier(void)
{
	_arm_dsb("SY");
}

/* relax CPU in atomic operations (hyperthreading) */
static inline void atomic_relax(void)
{
	arm_yield();
}

#endif
