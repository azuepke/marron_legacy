/* SPDX-License-Identifier: MIT */
/*
 * arch_syscall.h
 *
 * ARM-specific syscall stubs.
 *
 * azuepke, 2018-01-03: imported && reworked
 */

#ifndef __ARCH_SYSCALL_H__
#define __ARCH_SYSCALL_H__

#define SYSCALL_FUNC(typ, func, sys)	\
	.arm						;\
	.global func				;\
	.type func, %function		;\
	func:						;\
	_SYSCALL_ ## typ(sys)		;\
	.size func, . - func		;

/* ARM macros -- NOTE: due to the use of "#" in ARM assembler,
 * we cannot use the C preprocessor for these macros!
 */

/* uint64_t syscall(a, b, c, d); -- up to 4 arguments in registers */
.macro _SYSCALL_IN4, name
	push	{r4, lr}
	mov 	r12, #\name
	svc	#\name
	pop	{r4, pc}
.endm

#define _SYSCALL_IN0(name)	_SYSCALL_IN4(name)
#define _SYSCALL_IN1(name)	_SYSCALL_IN4(name)
#define _SYSCALL_IN2(name)	_SYSCALL_IN4(name)
#define _SYSCALL_IN3(name)	_SYSCALL_IN4(name)


/* uint64_t syscall(a, b, c, d, e, f); -- e and f are kept on stack */
.macro _SYSCALL_IN6, name
	push	{r4, r5, r6, lr}
	ldrd	r4, r5, [sp, #16]
	mov 	r12, #\name
	svc	#0
	pop	{r4, r5, r6, pc}
.endm

#define _SYSCALL_IN5(name)	_SYSCALL_IN6(name)


/* uint64_t syscall(a, b, c, d, e, f, g, h); -- e to h are kept on stack */
.macro _SYSCALL_IN8, name
	push	{r4, r5, r6, r7, r8, lr}
	ldrd	r4, r5, [sp, #24]
	ldrd	r6, r7, [sp, #32]
	mov 	r12, #\name
	svc	#0
	pop	{r4, r5, r6, r7, r8, pc}
.endm

#define _SYSCALL_IN7(name)	_SYSCALL_IN8(name)


/* void abort(caller_addr) _noreturn */
.macro _SYSCALL_ABORT1, name
	mov	r0, lr
	mov 	r12, #\name
	svc	#0
.endm


/* uint32_t syscall(a, &b) */
.macro _SYSCALL_IN1_OUT1, name
	/* remember "b" in r2 */
	push	{r4, lr}
	mov	r2, r1
	mov 	r12, #\name
	svc	#0
	str	r1, [r2]
	pop	{r4, pc}
.endm

#endif
