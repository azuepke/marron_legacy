/* SPDX-License-Identifier: MIT */
/*
 * arch_percpu_types.h
 *
 * ARM-specific per-CPU state.
 *
 * azuepke, 2017-10-02: initial
 * azuepke, 2018-03-05: renamed from struct arch_cpu
 */

#ifndef __ARCH_PERCPU_TYPES_H__
#define __ARCH_PERCPU_TYPES_H__

#ifndef __ASSEMBLER__

#include <stdint.h>

/** per-CPU structure, kept in TLS2. The kernel stack is put before */
struct arch_percpu {
	int unused;
};

#endif

#endif
