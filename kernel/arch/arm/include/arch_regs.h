/* SPDX-License-Identifier: MIT */
/*
 * arch_regs.h
 *
 * Register accessors in system calls.
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2015-07-13: 64-bit port
 * azuepke, 2017-09-29: imported and simplified
 * azuepke, 2018-03-16: split into arch_regs.h and arch_regs_types.h
 */

#ifndef __ARCH_REGS_H__
#define __ARCH_REGS_H__

#include <marron/regs.h>

/* register accessors */
/* IN registers, e.g. arguments */
#define REG_IN0(regs) ((regs)->r[0])
#define REG_IN1(regs) ((regs)->r[1])
#define REG_IN2(regs) ((regs)->r[2])
#define REG_IN3(regs) ((regs)->r[3])
#define REG_IN4(regs) ((regs)->r[4])
#define REG_IN5(regs) ((regs)->r[5])
#define REG_IN6(regs) ((regs)->r[6])

/* OUT registers, e.g. return values */
#define REG_OUT0(regs) ((regs)->r[0])
#define REG_OUT1(regs) ((regs)->r[1])
#define REG_OUT2(regs) ((regs)->r[2])
#define REG_OUT3(regs) ((regs)->r[3])

/* 64-bit IN/OUT accessors: */
/* 32-bit ARM places 64-bit values in register pairs 0/1 or 2/3 */
#ifdef __aarch64__
#define REG_IN0_64(regs) ((regs)->r[0])
#define REG_IN2_64(regs) ((regs)->r[2])
#define REG_OUT0_64(regs) ((regs)->r[0])
#else
#define REG_IN0_64(regs) (*(uint64_t *)&((regs)->r[0]))
#define REG_IN2_64(regs) (*(uint64_t *)&((regs)->r[2]))
#define REG_OUT0_64(regs) (*(uint64_t *)&((regs)->r[0]))
#endif

#endif
