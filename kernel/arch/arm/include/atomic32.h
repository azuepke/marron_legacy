/*
 * atomic32.h
 *
 * ARM atomic operations.
 *
 * azuepke, 2013-09-11: initial ARM port
 * azuepke, 2015-07-14: split into two files and added 64-bit support
 */

#ifndef __ATOMIC32_H__
#define __ATOMIC32_H__

#include <stdint.h>
#include <arm_insn.h>

#ifdef SMP
#define INSERT_DMB(mode) _arm_dmb(mode)
#else
#define INSERT_DMB(mode) do { } while (0)
#endif

/* these atomic_load,store,add,inc,etc functions are SMP safe
 * (and should rather be renamed to smp_atomic_xxx() ...)
 */

static inline uint32_t atomic_load(uint32_t *addr)
{
	uint32_t val;
	__asm__ volatile ("ldr %0, %1" : "=r"(val) : "m"(*addr) : "memory");
	return val;
}

static inline void atomic_store(uint32_t *addr, uint32_t val)
{
	__asm__ volatile ("str %1, %0" : "+m"(*addr) : "r"(val) : "memory");
}

static inline uint32_t atomic_swap(uint32_t *addr, uint32_t val)
{
	uint32_t err;
	INSERT_DMB("ISH");
	__asm__ volatile (
		"1:	ldrex	%0, [%2]		\n"
		"	strex	%1, %3, [%2]	\n"	/* returns 0 on success, 1 on failure */
		"	teq		%1, #1			\n"
		"	beq		1b				\n"
		: "=&r"(val), "=&r"(err) : "r"(addr), "r"(val) : "memory", "cc");
	INSERT_DMB("ISH");
	return val;
}

static inline void atomic_add(uint32_t *addr, uint32_t val)
{
	uint32_t tmp, after;
	__asm__ volatile (
		"1:	ldrex	%1, [%2]		\n"
		"	add		%1, %3			\n"
		"	strex	%0, %1, [%2]	\n"	/* returns 0 on success, 1 on failure */
		"	teq		%0, #1			\n"
		"	beq		1b				\n"
		: "=&r"(tmp), "=&r"(after) : "r"(addr), "Ir"(val) : "memory", "cc");
}

#define atomic_sub(addr, val) atomic_add((addr), -(val))
#define atomic_inc(addr) atomic_add((addr), 1)
#define atomic_dec(addr) atomic_add((addr), -1)


/* returns the value AFTER incrementing */
static inline uint32_t atomic_add_return(uint32_t *addr, uint32_t val)
{
	uint32_t tmp, after;
	INSERT_DMB("ISH");
	__asm__ volatile (
		"1:	ldrex	%1, [%2]		\n"
		"	add		%1, %3			\n"
		"	strex	%0, %1, [%2]	\n"	/* returns 0 on success, 1 on failure */
		"	teq		%0, #1			\n"
		"	beq		1b				\n"
		: "=&r"(tmp), "=&r"(after) : "r"(addr), "Ir"(val) : "memory", "cc");
	INSERT_DMB("ISH");
	return after;
}

#define atomic_sub_return(addr, val) atomic_add_return((addr), -(val))
#define atomic_inc_return(addr) atomic_add_return((addr), 1)
#define atomic_dec_return(addr) atomic_add_return((addr), -1)


/* returns the value BEFORE incrementing */
static inline uint32_t atomic_return_add(uint32_t *addr, uint32_t val)
{
	return atomic_add_return(addr, val) - val;
}

static inline void atomic_set_mask(uint32_t *addr, uint32_t mask)
{
	uint32_t tmp, after;
	__asm__ volatile (
		"1:	ldrex	%1, [%2]		\n"
		"	orr		%1, %3			\n"
		"	strex	%0, %1, [%2]	\n"	/* returns 0 on success, 1 on failure */
		"	teq		%0, #1			\n"
		"	beq		1b				\n"
		: "=&r"(tmp), "=&r"(after) : "r"(addr), "Ir"(mask) : "memory", "cc");
}

static inline void atomic_clear_mask(uint32_t *addr, uint32_t mask)
{
	uint32_t tmp, after;
	INSERT_DMB("ISH");
	__asm__ volatile (
		"1:	ldrex	%1, [%2]		\n"
		"	bic		%1, %3			\n"
		"	strex	%0, %1, [%2]	\n"	/* returns 0 on success, 1 on failure */
		"	teq		%0, #1			\n"
		"	beq		1b				\n"
		: "=&r"(tmp), "=&r"(after) : "r"(addr), "Ir"(mask) : "memory", "cc");
}

/* compare and swap:
 *   tmp = *addr";
 *   if (tmp == old)
 *     *addr = new;
 *   return tmp;
 */
static inline uint32_t atomic_cas(uint32_t *addr, uint32_t old, uint32_t new)
{
	uint32_t tmp, prev;
	INSERT_DMB("ISH");
	__asm__ volatile (
		"1:	ldrex	%1, [%2]		\n"
		"	subs	%0, %1, %3		\n"
		THUMB_IT("eq")
		"	strexeq	%0, %4, [%2]	\n"	/* returns 0 on success, 1 on failure */
		THUMB_IT("eq")
		"	teqeq	%0, #1			\n"
		"	beq		1b				\n"
		: "=&r"(tmp), "=&r"(prev) : "r"(addr), "Ir"(old), "r"(new) : "memory", "cc");
	if (prev == old) {
		INSERT_DMB("ISH");
	}
	return prev;
}

/* relaxed CAS without any barriers -- caller is responsible for barriers */
static inline uint32_t atomic_cas_relaxed(uint32_t *addr, uint32_t old, uint32_t new)
{
	uint32_t tmp, prev;
	__asm__ volatile (
		"1:	ldrex	%1, [%2]		\n"
		"	subs	%0, %1, %3		\n"
		THUMB_IT("eq")
		"	strexeq	%0, %4, [%2]	\n"	/* returns 0 on success, 1 on failure */
		THUMB_IT("eq")
		"	teqeq	%0, #1			\n"
		"	beq		1b				\n"
		: "=&r"(tmp), "=&r"(prev) : "r"(addr), "Ir"(old), "r"(new) : "memory", "cc");
	return prev;
}

#define atomic_ptr_load atomic_load
#define atomic_ptr_store atomic_store
#define atomic_ptr_cas atomic_cas
#define atomic_ptr_clear atomic_clear_mask

#endif
