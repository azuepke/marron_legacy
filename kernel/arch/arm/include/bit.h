/* SPDX-License-Identifier: MIT */
/*
 * bit.h
 *
 * ARM architecture specific operations on bits.
 *
 * azuepke, 2013-09-11: initial ARM port
 * azuepke, 2015-07-13: 64-bit port
 * azuepke, 2017-09-29: imported
 */

#ifndef __BIT_H__
#define __BIT_H__

#include <stdint.h>
#include <compiler.h>

/** find first bit set */
/** NOTE: unlike POSIX ffs(), the lowest bit is 0 */
/** NOTE: not robust if val is zero! */
static inline __pure unsigned int __bit_ffs(unsigned int val)
{
	unsigned int ret;
#ifdef __aarch64__
	__asm__ ("clz	%w0, %w1" : "=r" (ret) : "r" (val));
#else
	__asm__ ("clz	%0, %1" : "=r" (ret) : "r" (val));
#endif
	return 31 - ret;
}

static inline __pure unsigned int __bit_ffsl(unsigned long val)
{
#ifdef __aarch64__
	unsigned long ret;
	__asm__ ("clz	%0, %1" : "=r" (ret) : "r" (val));
	return 63 - ret;
#else
	unsigned long ret;
	__asm__ ("clz	%0, %1" : "=r" (ret) : "r" (val));
	return 31 - ret;
#endif
}

/** find last bit set */
/** NOTE: unlike POSIX ffs(), the lowest bit is 0 */
/** NOTE: not robust if val is zero! */
static inline __pure unsigned int __bit_fls(unsigned int val)
{
	return __bit_ffs(val & -val);
}

static inline __pure unsigned int __bit_flsl(unsigned long val)
{
	return __bit_ffsl(val & -val);
}


#endif
