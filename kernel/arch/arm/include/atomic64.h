/*
 * atomic64.h
 *
 * ARM atomic operations.
 *
 * azuepke, 2015-07-14: split from 32-bit version
 */

#ifndef __ATOMIC64_H__
#define __ATOMIC64_H__

#include <stdint.h>
#include <arm_insn.h>

#ifdef SMP
#define INSERT_DMB(mode) _arm_dmb(mode)
#else
#define INSERT_DMB(mode) do { } while (0)
#endif

/* these atomic_load,store,add,inc,etc functions are SMP safe
 * (and should rather be renamed to smp_atomic_xxx() ...)
 */

static inline uint32_t atomic_load(uint32_t *addr)
{
	uint32_t val;
	__asm__ volatile ("ldr %w0, %1" : "=r"(val) : "m"(*addr) : "memory");
	return val;
}

static inline void atomic_store(uint32_t *addr, uint32_t val)
{
	__asm__ volatile ("str %w1, %0" : "+m"(*addr) : "r"(val) : "memory");
}

static inline uint32_t atomic_swap(uint32_t *addr, uint32_t val)
{
	uint32_t err;
	INSERT_DMB("ISH");
	__asm__ volatile (
		"1:	ldxr	%w0, [%2]		\n"
		"	stxr	%w1, %w3, [%2]	\n"	/* returns 0 on success, 1 on failure */
		"	cbnz	%w1, 1b			\n"
		: "=&r"(val), "=&r"(err) : "r"(addr), "r"(val) : "memory", "cc");
	INSERT_DMB("ISH");
	return val;
}

static inline void atomic_add(uint32_t *addr, uint32_t val)
{
	uint32_t tmp, after;
	__asm__ volatile (
		"1:	ldxr	%w1, [%2]		\n"
		"	add		%w1, %w1, %w3	\n"
		"	stxr	%w0, %w1, [%2]	\n"	/* returns 0 on success, 1 on failure */
		"	cbnz	%w0, 1b			\n"
		: "=&r"(tmp), "=&r"(after) : "r"(addr), "Ir"(val) : "memory", "cc");
}

#define atomic_sub(addr, val) atomic_add((addr), -(val))
#define atomic_inc(addr) atomic_add((addr), 1)
#define atomic_dec(addr) atomic_add((addr), -1)


/* returns the value AFTER incrementing */
static inline uint32_t atomic_add_return(uint32_t *addr, uint32_t val)
{
	uint32_t tmp, after;
	INSERT_DMB("ISH");
	__asm__ volatile (
		"1:	ldxr	%w1, [%2]		\n"
		"	add		%w1, %w1, %w3	\n"
		"	stxr	%w0, %w1, [%2]	\n"	/* returns 0 on success, 1 on failure */
		"	cbnz	%w0, 1b			\n"
		: "=&r"(tmp), "=&r"(after) : "r"(addr), "Ir"(val) : "memory", "cc");
	INSERT_DMB("ISH");
	return after;
}

#define atomic_sub_return(addr, val) atomic_add_return((addr), -(val))
#define atomic_inc_return(addr) atomic_add_return((addr), 1)
#define atomic_dec_return(addr) atomic_add_return((addr), -1)


/* returns the value BEFORE incrementing */
static inline uint32_t atomic_return_add(uint32_t *addr, uint32_t val)
{
	return atomic_add_return(addr, val) - val;
}

static inline void atomic_set_mask(uint32_t *addr, uint32_t mask)
{
	uint32_t tmp, after;
	__asm__ volatile (
		"1:	ldxr	%w1, [%2]		\n"
		"	orr		%w1, %w1, %w3	\n"
		"	stxr	%w0, %w1, [%2]	\n"	/* returns 0 on success, 1 on failure */
		"	cbnz	%w0, 1b			\n"
		: "=&r"(tmp), "=&r"(after) : "r"(addr), "Kr"(mask) : "memory", "cc");
}

static inline void atomic_clear_mask(uint32_t *addr, uint32_t mask)
{
	uint32_t tmp, after;
	__asm__ volatile (
		"1:	ldxr	%w1, [%2]		\n"
		"	bic		%w1, %w1, %w3 	\n"
		"	stxr	%w0, %w1, [%2]	\n"	/* returns 0 on success, 1 on failure */
		"	cbnz	%w0, 1b			\n"
		: "=&r"(tmp), "=&r"(after) : "r"(addr), "Kr"(mask) : "memory", "cc");
}

/* compare and swap:
 *   tmp = *addr";
 *   if (tmp == old)
 *     *addr = new;
 *   return tmp;
 */
static inline uint32_t atomic_cas(uint32_t *addr, uint32_t old, uint32_t new)
{
	uint32_t tmp, prev;
	INSERT_DMB("ISH");
	__asm__ volatile (
		"1:	ldxr	%w1, [%2]		\n"
		"	subs	%w0, %w1, %w3	\n"
		"	b.ne	2f				\n"
		"	stxr	%w0, %w4, [%2]	\n"	/* returns 0 on success, 1 on failure */
		"	cbnz	%w0, 1b			\n"
		"2:							\n"
		: "=&r"(tmp), "=&r"(prev) : "r"(addr), "Ir"(old), "r"(new) : "memory", "cc");
	if (prev == old) {
		INSERT_DMB("ISH");
	}
	return prev;
}

/* relaxed CAS without any barriers -- caller is responsible for barriers */
static inline uint32_t atomic_cas_relaxed(uint32_t *addr, uint32_t old, uint32_t new)
{
	uint32_t tmp, prev;
	__asm__ volatile (
		"1:	ldxr	%w1, [%2]		\n"
		"	subs	%w0, %w1, %w3	\n"
		"	b.ne	2f				\n"
		"	stxr	%w0, %w4, [%2]	\n"	/* returns 0 on success, 1 on failure */
		"	cbnz	%w0, 1b			\n"
		"2:							\n"
		: "=&r"(tmp), "=&r"(prev) : "r"(addr), "Ir"(old), "r"(new) : "memory", "cc");
	return prev;
}


/* 64-bit variants for pointers */
static inline addr_t atomic_ptr_load(addr_t *addr)
{
	addr_t val;
	__asm__ volatile ("ldr %0, %1" : "=r"(val) : "m"(*addr) : "memory");
	return val;
}

static inline void atomic_ptr_store(addr_t *addr, addr_t val)
{
	__asm__ volatile ("str %1, %0" : "+m"(*addr) : "r"(val) : "memory");
}

static inline addr_t atomic_ptr_cas(addr_t *addr, addr_t old, addr_t new)
{
	addr_t tmp, prev;
	INSERT_DMB("ISH");
	__asm__ volatile (
		"1:	ldxr	%1, [%2]		\n"
		"	subs	%0, %1, %3		\n"
		"	b.ne	2f				\n"
		"	stxr	%w0, %4, [%2]	\n"	/* returns 0 on success, 1 on failure */
		"	cbnz	%w0, 1b			\n"
		"2:							\n"
		: "=&r"(tmp), "=&r"(prev) : "r"(addr), "Ir"(old), "r"(new) : "memory", "cc");
	if (prev == old) {
		INSERT_DMB("ISH");
	}
	return prev;
}

static inline void atomic_ptr_clear(addr_t *addr, addr_t mask)
{
	addr_t tmp, after;
	__asm__ volatile (
		"1:	ldxr	%1, [%2]		\n"
		"	bic		%1, %1, %3		\n"
		"	stxr	%w0, %1, [%2]	\n"	/* returns 0 on success, 1 on failure */
		"	cbnz	%w0, 1b			\n"
		: "=&r"(tmp), "=&r"(after) : "r"(addr), "Lr"(mask) : "memory", "cc");
}

#endif
