/* SPDX-License-Identifier: MIT */
/*
 * arm_insn.h
 *
 * ARM architecture specific instructions.
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2015-07-14: split into two files and added 64-bit support
 * azuepke, 2017-09-29: imported and merged again
 */

#ifndef __ARM_INSN_H__
#define __ARM_INSN_H__

#include <arm_cr.h>

/** Thumb ITE block macros for 32-bit ARM */
#ifdef __thumb__
#define THUMB_IT(c)			"it		" c ";\n"
#define THUMB_ITE(c)		"ite	" c ";\n"
#define THUMB_ITEE(c)		"itee	" c ";\n"
#define THUMB_ITEEE(c)		"iteee	" c ";\n"
#define THUMB_ITT(c)		"itt	" c ";\n"
#define THUMB_ITTE(c)		"itte	" c ";\n"
#define THUMB_ITTEE(c)		"ittee	" c ";\n"
#define THUMB_ITTT(c)		"ittt	" c ";\n"
#define THUMB_ITTTE(c)		"ittte	" c ";\n"
#define THUMB_ITTTT(c)		"itttt	" c ";\n"
#else
#define THUMB_IT(c)			";\n"
#define THUMB_ITE(c)		";\n"
#define THUMB_ITEE(c)		";\n"
#define THUMB_ITEEE(c)		";\n"
#define THUMB_ITT(c)		";\n"
#define THUMB_ITTE(c)		";\n"
#define THUMB_ITTEE(c)		";\n"
#define THUMB_ITTT(c)		";\n"
#define THUMB_ITTTE(c)		";\n"
#define THUMB_ITTTT(c)		";\n"
#endif

/** ISB */
#define _arm_isb(o)	__asm__ volatile ("isb " o : : : "memory")
#define _arm_dsb(o)	__asm__ volatile ("dsb " o : : : "memory")
#define _arm_dmb(o)	__asm__ volatile ("dmb " o : : : "memory")

#define arm_isb() _arm_isb("SY")
#define arm_dsb() _arm_dsb("SY")
#define arm_dmb() _arm_dmb("SY")

#define arm_clrex()	__asm__ volatile ("clrex" : : : "memory")
#define arm_yield()	__asm__ volatile ("yield" : : : "memory")
#define arm_sev()	__asm__ volatile ("sev" : : : "memory")
#define arm_wfe()	__asm__ volatile ("wfe" : : : "memory")
#define arm_wfi()	__asm__ volatile ("wfi" : : : "memory")


/** Set CONTEXT ID (ASID) */
/* NOTE: requires special synchronization! */
static inline void arm_set_asid(unsigned long val)
{
	__asm__ volatile ("mcr p15, 0, %0, c13, c0, 1" : : "r"(val) : "memory");
}

/** Set TTBR0 (user page table) */
/* NOTE: requires special synchronization! */
static inline void arm_set_ttbr0(unsigned long val)
{
	__asm__ volatile ("mcr p15, 0, %0, c2, c0, 0" : : "r"(val) : "memory");
}

/** Set VBAR_EL1 (exception vector base register) */
static inline void arm_set_vbar(unsigned long val)
{
#ifdef __aarch64__
	__asm__ volatile ("msr VBAR_EL1, %0" : : "r"(val) : "memory");
#else
	__asm__ volatile ("mcr p15, 0, %0, c12, c0, 0" : : "r"(val) : "memory");
#endif
}

/** Enable exceptions (but not interrupts) */
static inline void arm_enable_exceptions(void)
{
#ifdef __aarch64__
	__asm__ volatile ("msr DAIFCLR, #0xd" : : : "memory");
#else
	__asm__ volatile ("cpsie af" : : : "memory");
#endif
}

/** Get MPIDR (multiprocessor ID register, for MPCore) */
static inline unsigned long arm_get_mpidr(void)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, MPIDR_EL1" : "=r"(val));
#else
	__asm__ volatile ("mrc p15, 0, %0, c0, c0, 5" : "=r"(val));
#endif
	return val;
}

/** clean a data cache line */
static inline void arm_clean_dcache(void *addr)
{
#ifdef __aarch64__
	__asm__ volatile ("dc CVAC, %0" : : "r"(addr) : "memory");
	arm_dsb();
#else
	__asm__ volatile ("mcr p15, 0, %0, c7, c10, 1" : : "r"(addr) : "memory");
	arm_dsb();
#endif
}


/** Get TPIDRURW / TPIDR_EL0 (user writable context register) */
static inline unsigned long arm_get_tls0(void)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, TPIDR_EL0" : "=r"(val));
#else
	__asm__ volatile ("mrc p15, 0, %0, c13, c0, 2" : "=r"(val));
#endif
	return val;
}

/** Set TPIDRURW / TPIDR_EL0 (user writable context register) */
static inline void arm_set_tls0(unsigned long val)
{
#ifdef __aarch64__
	__asm__ volatile ("msr TPIDR_EL0, %0" : : "r"(val) : "memory");
#else
	__asm__ volatile ("mcr p15, 0, %0, c13, c0, 2" : : "r"(val) : "memory");
#endif
}

/** Get TPIDRURO / TPIDRRO_EL0 (user readable context register) */
static inline unsigned long arm_get_tls1(void)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, TPIDRRO_EL0" : "=r"(val));
#else
	__asm__ volatile ("mrc p15, 0, %0, c13, c0, 3" : "=r"(val));
#endif
	return val;
}

/** Set TPIDRURO / TPIDRRO_EL0 (user readable context register) */
static inline void arm_set_tls1(unsigned long val)
{
#ifdef __aarch64__
	__asm__ volatile ("msr TPIDRRO_EL0, %0" : : "r"(val) : "memory");
#else
	__asm__ volatile ("mcr p15, 0, %0, c13, c0, 3" : : "r"(val) : "memory");
#endif
}


/** Get TPIDRPRW / TPIDR_EL1 (user inaccessible context register) */
static inline unsigned long arm_get_tls2(void)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, TPIDR_EL1" : "=r"(val));
#else
	__asm__ volatile ("mrc p15, 0, %0, c13, c0, 4" : "=r"(val));
#endif
	return val;
}

/** Set TPIDRPRW / TPIDR_EL1 (user inaccessible context register) */
static inline void arm_set_tls2(unsigned long val)
{
#ifdef __aarch64__
	__asm__ volatile ("msr TPIDR_EL1, %0" : : "r"(val) : "memory");
#else
	__asm__ volatile ("mcr p15, 0, %0, c13, c0, 4" : : "r"(val) : "memory");
#endif
}


#ifdef __arm__

/** get FPEXC (32-bit only) */
static inline unsigned long arm_get_fpexc(void)
{
	unsigned long ret;
	__asm__ volatile ("mrc p10, 7, %0, c8, c0, 0" : "=r" (ret));
	return ret;
}

/** set FPEXC (32-bit only) */
static inline void arm_set_fpexc(unsigned long val)
{
	__asm__ volatile ("mcr p10, 7, %0, c8, c0, 0" : : "r" (val) : "memory");
	arm_isb();
}

#endif

#endif
