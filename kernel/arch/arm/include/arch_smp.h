/* SPDX-License-Identifier: MIT */
/*
 * arch_smp.h
 *
 * Architecture abstraction layer, SMP barriers.
 *
 * azuepke, 2018-04-10: initial
 */

#ifndef __ARCH_SMP_H__
#define __ARCH_SMP_H__

#include <arm_insn.h>

/** SMP pause (for hyperthreading) */
static inline void arch_smp_pause(void)
{
	arm_yield();
}

/** SMP read barrier */
static inline void arch_smp_read_barrier(void)
{
	arm_dmb();
}

/** SMP write barrier */
static inline void arch_smp_write_barrier(void)
{
	arm_dmb();
}

#endif
