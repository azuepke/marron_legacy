/* SPDX-License-Identifier: MIT */
/*
 * arm_io.h
 *
 * ARM specific I/O accessors.
 *
 * NOTE: uses a different order than Linux:
 *   outb(port, value);
 *   writel(addr, value);
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2015-07-13: 64-bit port
 * azuepke, 2015-07-15: split 32-bit and 64-bit version
 * azuepke, 2017-07-17: adapted to mini-OS
 * azuepke, 2017-09-29: imported and merged again (as io.h)
 */

#ifndef __ARM_IO_H__
#define __ARM_IO_H__

#include <stdint.h>

#ifdef __aarch64__
#define W0 "%w0"
#else
#define W0 "%0"
#endif

static inline uint8_t readb(const volatile void *addr)
{
	uint8_t ret;
	__asm__ volatile("ldrb " W0 ", %1" : "=r" (ret)
	                 : "m" (*(const volatile uint8_t *)addr) : "memory");
	return ret;
}

static inline uint16_t readw(const volatile void *addr)
{
	uint16_t ret;
	__asm__ volatile("ldrh " W0 ", %1" : "=r" (ret)
	                 : "m" (*(const volatile uint16_t *)addr) : "memory");
	return ret;
}

static inline uint32_t readl(const volatile void *addr)
{
	uint32_t ret;
	__asm__ volatile("ldr " W0 ", %1" : "=r" (ret)
	                 : "m" (*(const volatile uint32_t *)addr) : "memory");
	return ret;
}

#ifdef __aarch64__
/* NOTE: LDRD is atomic on ARMv7 with LPAE, but not on older ones */
static inline uint64_t readq(const volatile void *addr)
{
	uint64_t ret;
	__asm__ volatile("ldr %0, %1" : "=r" (ret)
	                 : "m" (*(const volatile uint64_t *)addr) : "memory");
	return ret;
}
#endif

static inline void writeb(volatile void *addr, uint8_t val)
{
	__asm__ volatile ("strb " W0 ", %1" : : "r" (val),
	                  "m" (*(volatile uint8_t *)addr) : "memory");
}

static inline void writew(volatile void *addr, uint16_t val)
{
	__asm__ volatile ("strh " W0 ", %1" : : "r" (val),
	                  "m" (*(volatile uint16_t *)addr) : "memory");
}

static inline void writel(volatile void *addr, uint32_t val)
{
	__asm__ volatile ("str " W0 ", %1" : : "r" (val),
	                  "m" (*(volatile uint32_t *)addr) : "memory");
}

#ifdef __aarch64__
/* NOTE: STRD is atomic on ARMv7 with LPAE, but not on older ones */
static inline void writeq(volatile void *addr, uint64_t val)
{
	__asm__ volatile ("str %0, %1" : : "r" (val),
	                  "m" (*(volatile uint64_t *)addr) : "memory");
}
#endif

#endif
