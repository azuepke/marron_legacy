/*
 * arm_perf.h
 *
 * ARM performance monitoring.
 *
 * azuepke, 2014-03-03: initial
 */

#ifndef __ARM_PERF_H__
#define __ARM_PERF_H__

#ifdef ARM_V6
#include <arm_perf_v6.h>
#else
#include <arm_perf_v7.h>
#endif

#endif
