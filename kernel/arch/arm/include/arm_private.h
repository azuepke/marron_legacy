/* SPDX-License-Identifier: MIT */
/*
 * arm_private.h
 *
 * ARM private functions
 *
 * azuepke, 2017-10-02: initial
 */

#ifndef __ARM_PRIVATE_H__
#define __ARM_PRIVATE_H__

#include <marron/regs.h>

/* entry.S */
void arch_entry(unsigned long kernel_stack, unsigned long fiq_stack);
extern char arm_vectors[];
void arm_fpu_save(void *fpu_regs);
void arm_fpu_restore(const void *fpu_regs);

/* exception.S */
void arm_handler_undef_user(struct regs *regs);
void arm_handler_undef_kern(struct regs *regs);
void arm_handler_pabt_user(struct regs *regs, ulong_t ifsr, ulong_t ifar);
void arm_handler_pabt_kern(struct regs *regs, ulong_t ifsr, ulong_t ifar);
void arm_handler_dabt_user(struct regs *regs, ulong_t dfsr, ulong_t dfar);
void arm_handler_dabt_kern(struct regs *regs, ulong_t dfsr, ulong_t dfar);
void arm_handler_irq(struct regs *regs);
void arm_handler_fiq(struct regs *regs);

#endif
