/* SPDX-License-Identifier: MIT */
/*
 * arm_cr.h
 *
 * Bits in ARM specific control registers, safe to include from assembler.
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2015-07-14: split into two files and added 64-bit support
 * azuepke, 2017-09-29: imported and merged again
 */

#ifndef __ARM_CR_H__
#define __ARM_CR_H__

/** CPU 32-bit modes in CPSRs */
#define CPSR_MODE_USER		0x10
#define CPSR_MODE_FIQ		0x11
#define CPSR_MODE_IRQ		0x12
#define CPSR_MODE_SVC		0x13
#define CPSR_MODE_MONITOR	0x16
#define CPSR_MODE_ABORT		0x17
#define CPSR_MODE_HYP		0x1a
#define CPSR_MODE_UNDEF		0x1b
#define CPSR_MODE_SYSTEM	0x1f
#define CPSR_MODE_MASK		0x1f

/** CPU 64-bit modes in CPSRs (bit 3..2: EL, bit 0: handler stack) */
#define CPSR_MODE_EL3h		0xd
#define CPSR_MODE_EL3t		0xc
#define CPSR_MODE_EL2h		0x9
#define CPSR_MODE_EL2t		0x8
#define CPSR_MODE_EL1h		0x5
#define CPSR_MODE_EL1t		0x4
#define CPSR_MODE_EL0t		0x0

/** CPSR bits */
#define CPSR_32BIT			0x00000010	/* 32-bit mode */
#define CPSR_T				0x00000020	/* Thumb enable */
#define CPSR_F				0x00000040	/* FIQ disable */
#define CPSR_I				0x00000080	/* IRQ disable */
#define CPSR_A				0x00000100	/* asynchronous abort disable */
#define CPSR_E				0x00000200	/* big endian */
#define CPSR_D				0x00000200	/* ARMv8: debug disable */

#define CPSR_IT1			0x0000fc00	/* Thumb2 IT flags */
#define CPSR_GE				0x000f0000	/* GE SIMD flags */

#define CPSR_IL				0x00100000	/* ARMv8: illegal execution state */
#define CPSR_SS				0x00200000	/* ARMv8: software step */
#define CPSR_J				0x01000000	/* Jazelle */
#define CPSR_IT2			0x06000000	/* Thumb2 IT flags */
#define CPSR_Q				0x08000000	/* Q flag */
#define CPSR_V				0x10000000	/* V flag */
#define CPSR_C				0x20000000	/* C flag */
#define CPSR_Z				0x40000000	/* Z flag */
#define CPSR_N				0x80000000	/* N flag */


/** CPSR default user bits */
#ifdef BIG_ENDIAN
#define CPSR_USER32_BITS	0x00000210	/* 32-bit user mode */
#define CPSR_USER64_BITS	0x00000200	/* 64-bit user mode on thread stack */
#else
#define CPSR_USER32_BITS	0x00000010	/* 32-bit user mode */
#define CPSR_USER64_BITS	0x00000000	/* 64-bit user mode on thread stack */
#endif

/** CPSR bits the user can change */
#define CPSR_USER32_MASK	0xfe0ffc20	/* all flags + Thumb, except Jazelle */
/* FIXME: what about debug flags? CPSR_SS and CPSR_D */
#define CPSR_USER64_MASK	0xf0000000	/* NZCV flags only */


/** FPEXC bits (32-bit only) */
#define FPEXC_EX			0x80000000	/* exception, save FPINST */
#define FPEXC_EN			0x40000000	/* enable */
#define FPEXC_DEX			0x20000000	/* unsupported vector instruction */
#define FPEXC_FP2V			0x10000000	/* exception, save FPINST2 */


/** ESR exception class encoding (64-bit only) */
#define ESR_EC_UNKNOWN		0x00
#define ESR_EC_WFI_WFE		0x01
#define ESR_EC_ILL			0x0e
#define ESR_EC_SVC32		0x11
#define ESR_EC_HVC32		0x12
#define ESR_EC_SMC32		0x13
#define ESR_EC_SVC64		0x15
#define ESR_EC_HVC64		0x16
#define ESR_EC_SMC64		0x17
#define ESR_EC_IABT_USER	0x20
#define ESR_EC_IABT_KERN	0x21
#define ESR_EC_PC_ALIGN		0x22
#define ESR_EC_DABT_USER	0x24
#define ESR_EC_DABT_KERN	0x25
#define ESR_EC_SP_ALIGN		0x26
#define ESR_EC_FP_EXC32		0x28
#define ESR_EC_FP_EXC64		0x2c

#define ESR_EC_SHIFT		26


/** CPACR -- Coprocessor Access Control Register (cp 15, c1, c0, 1) */
#define CPACR_CP10			0x00300000	/* enable access to CP10 */
#define CPACR_CP11			0x00c00000	/* enable access to CP11 */
#define CPACR_TTA			0x10000000	/* trap user access to trace regs (ARMv8) */
#define CPACR_D32DIS		0x40000000	/* disable D16-D31 (ARMv7) */
#define CPACR_ASEDIS		0x80000000	/* disable SIMD (ARMv7) */


/** DFSR and IFSR status bits */
#ifdef ARM_LPAE
#define FSR_FSMASK			0x003f
#else
#define FSR_FSMASK			0x000f
#define FSR_FSBIT4			0x0400
#endif
#define FSR_WRITE			0x0800
#define FSR_EXTABT			0x1000


/** SCTLR -- System Control Register (cp15, c1, c0, 0) */
/* NOTE: enables a feature when the bit is set, otherwise documented */
#define SCTLR_M				0x00000001	/* MMU */
#define SCTLR_A				0x00000002	/* strict alignment */
#define SCTLR_C				0x00000004	/* data/unified cache */
#define SCTLR_SA			0x00000008	/* enable stack alignment check (ARMv8) */
#define SCTLR_SA0			0x00000010	/* enable EL0 stack alignment check (ARMv8) */
#define SCTLR_CP15BEN		0x00000020	/* CP15 barrier enable */
#define SCTLR_ITD			0x00000080	/* IT instruction disable (ARMv8) */
#define SCTLR_SED			0x00000100	/* SETEND instruction disable (ARMv8) */
#define SCTLR_UMA			0x00000200	/* enable to mask interrupts in EL0 */
#define SCTLR_SW			0x00000400	/* SWP (ARMv7) */
#define SCTLR_Z				0x00000800	/* branch prediction enable (ARMv7) */
#define SCTLR_I				0x00001000	/* instruction cache */
#define SCTLR_V				0x00002000	/* high vectors (ARMv7) */
#define SCTLR_RR			0x00004000	/* round-robin replacement in caches (ARMv7) */
#define SCTLR_DZE			0x00004000	/* enable DC ZVA (dcache zero) instruction at EL0 (ARMv8) */
#define SCTLR_UCT			0x00008000	/* enable access to CTR at EL0 (ARMv8) */

#define SCTLR_nTWI			0x00010000	/* WFI non-trapping at EL0 (ARMv8) */
#define SCTLR_BR			0x00020000	/* MPU background region enable (MPU) */
#define SCTLR_HA			0x00020000	/* hardware access flag (ARMv7) */
#define SCTLR_nTWE			0x00040000	/* WFE non-trapping at EL0 (ARMv8) */

#define SCTLR_WXN			0x00080000	/* write implies execute never */
#define SCTLR_UWXN			0x00100000	/* user write implies execute never for EL1 (ARMv7) */
#define SCTLR_FI			0x00200000	/* low interrupt latency (ARMv7) */

#define SCTLR_VE			0x01000000	/* vectored interrupts (ARMv7) */
#define SCTLR_EE			0x02000000	/* big endian exception entry */

#define SCTLR_E0E			0x01000000	/* EL0 data access is big endian (ARMv8) */
#define SCTLR_UCI			0x04000000	/* user cache instructions (ARMv8) */
#define SCTLR_NMFI			0x08000000	/* non-maskable FIQs (ARMv7) */

#define SCTLR_TRE			0x10000000	/* TEX remapping (ARMv7) */
#define SCTLR_AFE			0x20000000	/* force AP bit (ARMv7) */
#define SCTLR_TE			0x40000000	/* thumb exception (ARMv7) */


/** SCTLR default bits */
#define SCTLR_CLR_32BIT		(SCTLR_A | SCTLR_V | \
							 SCTLR_TE | SCTLR_EE | \
							 SCTLR_AFE | SCTLR_TRE | SCTLR_HA | SCTLR_WXN | SCTLR_UWXN | \
							 SCTLR_NMFI | SCTLR_FI | SCTLR_VE | SCTLR_RR | \
							 SCTLR_CP15BEN)
#ifdef BIG_ENDIAN
#define SCTLR_SET_32BIT		(SCTLR_M | SCTLR_C | SCTLR_I | SCTLR_Z | SCTLR_EE)
#else
#define SCTLR_SET_32BIT		(SCTLR_M | SCTLR_C | SCTLR_I | SCTLR_Z)
#endif

#define SCTLR_CLR_64BIT		(SCTLR_A | \
							 SCTLR_EE | \
							 SCTLR_WXN | \
							 SCTLR_nTWE | SCTLR_nTWI | SCTLR_UMA | \
							 SCTLR_SED | SCTLR_ITD | SCTLR_CP15BEN)
#ifdef BIG_ENDIAN
#define SCTLR_SET_64BIT		(SCTLR_M | SCTLR_C | SCTLR_I | \
							 SCTLR_UCI | SCTLR_UCT | SCTLR_DZE | \
							 SCTLR_SA0 | SCTLR_SA | SCTLR_EE)
#else
#define SCTLR_SET_64BIT		(SCTLR_M | SCTLR_C | SCTLR_I | \
							 SCTLR_UCI | SCTLR_UCT | SCTLR_DZE | \
							 SCTLR_SA0 | SCTLR_SA)
#endif

#endif
