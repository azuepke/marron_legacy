/* SPDX-License-Identifier: MIT */
/*
 * arch.h
 *
 * Architecture abstraction layer.
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2015-07-13: 64-bit port
 * azuepke, 2017-07-17: reduced version for new kernel
 * azuepke, 2017-09-29: imported and adapted
 */

#ifndef __ARCH_H__
#define __ARCH_H__

/* forward */
struct regs;
struct arch_adspace_cfg;

/* exception.c */
void arch_dump_registers(struct regs *regs);
void arch_init_exceptions(void);
void arch_adspace_switch(const struct arch_adspace_cfg *cfg);


/* for kernel boot message */
#if defined(ARM_V8)
#define ARCH_BANNER_NAME "ARMv8"
#elif defined(ARM_V7) && defined(ARM_LPAE)
#define ARCH_BANNER_NAME "ARMv7 LPAE"
#else
#define ARCH_BANNER_NAME "ARMv7"
#endif

/** architecture specific alignment, minimum a cache-line */
#define ARCH_ALIGN 64

#endif
