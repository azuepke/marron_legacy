/* SPDX-License-Identifier: MIT */
/*
 * arch_bsp.h
 *
 * Architecture abstraction layer.
 *
 * azuepke, 2013-09-11: initial ARM port
 * azuepke, 2017-09-29: imported and adapted
 */

#ifndef __ARCH_BSP_H__
#define __ARCH_BSP_H__

#include <stdint.h>

/** private interface between architecture layer and BSP */
struct arch_bsp {
	/** physical start of RAM */
	unsigned long mem_start;
	/** physical end of RAM */
	unsigned long mem_end;
#ifdef __aarch64__
	/** end of physical address space */
	unsigned long phys_end;
#endif

	/** size of a cache line */
	uint32_t cache_line_size;

	/** Pointer to 16K boot page directory (l2 table on LPAE),
	 * or l1 table for kernel in TTBR1 on AARCH64.
	 */
	void *boot_pgdir;
};

#endif
