/* SPDX-License-Identifier: MIT */
/*
 * arch_tls.h
 *
 * ARM specific TLS code (for user space), valid vor ARMv6k+ and AARCH64
 *
 * azuepke, 2013-09-11: initial ARM port
 * azuepke, 2015-07-14: split into two files and added 64-bit support
 * azuepke, 2017-09-29: imported
 */

#ifndef __ARCH_TLS_H__
#define __ARCH_TLS_H__

#include <stdint.h>
#include <compiler.h>

static inline unsigned long __tls_base(void)
{
	unsigned long val;
#ifdef __aarch64__
	__asm__ volatile ("mrs %0, TPIDR_EL0" : "=r" (val));
#else
	__asm__ volatile ("mrc p15, 0, %0, c13, c0, 3" : "=r" (val));
#endif
	return val;
}

#define _TLS_PTR_1(offset) ((uint8_t  *)(__tls_base() + (offset)))
#define _TLS_PTR_2(offset) ((uint16_t *)(__tls_base() + (offset)))
#define _TLS_PTR_4(offset) ((uint32_t *)(__tls_base() + (offset)))
#define _TLS_PTR_8(offset) ((uint64_t *)(__tls_base() + (offset)))

/* getter */
static inline uint8_t tls_get_1(unsigned long offset)
{
	return *_TLS_PTR_1(offset);
}

static inline uint16_t tls_get_2(unsigned long offset)
{
	return *_TLS_PTR_2(offset);
}

static inline uint32_t tls_get_4(unsigned long offset)
{
	return *_TLS_PTR_4(offset);
}

static inline uint64_t tls_get_8(unsigned long offset)
{
	return *_TLS_PTR_8(offset);
}


/* setter */
static inline void tls_set_1(unsigned long offset, uint8_t val)
{
	*_TLS_PTR_1(offset)  = val;
}

static inline void tls_set_2(unsigned long offset, uint16_t val)
{
	*_TLS_PTR_2(offset)  = val;
}

static inline void tls_set_4(unsigned long offset, uint32_t val)
{
	*_TLS_PTR_4(offset)  = val;
}

static inline void tls_set_8(unsigned long offset, uint64_t val)
{
	*_TLS_PTR_8(offset)  = val;
}


/* pointers */
#ifdef __aarch64__
#define tls_get_p(offset) ((void *)tls_get_8(offset))
#define tls_set_p(offset, val) tls_set_8((offset), (uint64_t)(val))
#else
#define tls_get_p(offset) ((void *)tls_get_4(offset))
#define tls_set_p(offset, val) tls_set_4((offset), (uint32_t)(val))
#endif

#endif
