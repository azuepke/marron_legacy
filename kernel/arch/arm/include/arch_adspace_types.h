/* SPDX-License-Identifier: MIT */
/*
 * arch_adspace.h
 *
 * ARM-specific MMU configuration data.
 *
 * azuepke, 2018-03-23: initial
 */

#ifndef __ARCH_MMU_TYPES_H__
#define __ARCH_MMU_TYPES_H__

#include <stdint.h>

/** partition MMU configuration data */
struct arch_adspace_cfg {
	/* the following data relates to the generated MMU page tables! */

	/** physical address of page tables */
	unsigned long ttbr0;
	/** address space ID */
	uint32_t asid;
};

#endif
