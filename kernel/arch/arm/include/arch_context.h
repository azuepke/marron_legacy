/* SPDX-License-Identifier: MIT */
/*
 * arch_context.h
 *
 * ARM register context switching and helper functions
 *
 * azuepke, 2013-09-11: initial
 * azuepke, 2015-07-14: 64-bit port
 * azuepke, 2017-09-29: imported & adapted
 */

#ifndef __ARCH_CONTEXT_H__
#define __ARCH_CONTEXT_H__

#include <arch_regs.h>
#include <arm_cr.h>
#include <arm_insn.h>
#include <arm_private.h>


/** initialize an idle thread's register context */
static inline void arch_reg_init_idle(
	struct regs *regs,
	ulong_t entry,
	ulong_t stack)
{
#ifdef __aarch64__
	for (unsigned int i = 0; i < 30; i++) {
		regs->r[i] = 0;
	}

	/* keep stack aligned to 16 bytes */
	regs->sp = stack & -16ul;
	/* let function return to NULL address */
	regs->lr = 0;
	regs->pc = entry;
	/* run in super visor mode, but use user mode stack */
	regs->cpsr = CPSR_USER64_BITS | CPSR_MODE_EL1t;
	regs->esr = 0;
	regs->addr = 0;
	regs->info = 0;
	regs->tls0 = 0;
	regs->tls1 = 0;
#else
	for (unsigned int i = 0; i < 13; i++) {
		regs->r[i] = 0;
	}

	/* keep stack aligned to 8 bytes */
	regs->sp = stack & -8ul;
	/* let function return to NULL address */
	regs->lr = 0;
	regs->pc = entry;
	/* run in super visor mode, but use user mode stack */
	regs->cpsr = CPSR_USER32_BITS | CPSR_MODE_SYSTEM;
	if (entry & 0x1ul) {
		/* enable thumb mode */
		regs->cpsr |= CPSR_T;
	}
	regs->fsr = 0;
	regs->addr = 0;
	regs->info = 0;
	regs->tls0 = 0;
	regs->tls1 = 0;
#endif
}

/** initialize a user space thread's register context */
static inline void arch_reg_init(
	struct regs *regs,
	ulong_t entry,
	ulong_t stack,
	ulong_t arg,
	ulong_t tls)
{
#ifdef __aarch64__
	for (unsigned int i = 0; i < 30; i++) {
		regs->r[i] = 0;
	}
	regs->r[0] = arg;

	/* keep stack aligned to 16 bytes */
	regs->sp = stack & -16ul;
	/* let function return to NULL address */
	regs->lr = 0;
	regs->pc = entry;
	/* run in user mode */
	regs->cpsr = CPSR_USER64_BITS;
	regs->esr = 0;
	regs->addr = 0;
	regs->info = 0;
	regs->tls0 = tls;
	regs->tls1 = 0;
#else
	for (unsigned int i = 0; i < 13; i++) {
		regs->r[i] = 0;
	}
	regs->r[0] = arg;

	/* keep stack aligned to 8 bytes */
	regs->sp = stack & -8ul;
	/* let function return to NULL address */
	regs->lr = 0;
	regs->pc = entry;
	/* run in user mode */
	regs->cpsr = CPSR_USER32_BITS;
	if (entry & 0x1ul) {
		/* enable thumb mode */
		regs->cpsr |= CPSR_T;
	}
	regs->fsr = 0;
	regs->addr = 0;
	regs->info = 0;
	regs->tls0 = 0;
	regs->tls1 = tls;
#endif
}

/** save additional registers, e.g. TLS */
static inline void arch_reg_save(struct regs *regs)
{
	regs->tls0 = arm_get_tls0();
	regs->tls1 = arm_get_tls1();
}

/** restore additional registers, e.g. TLS */
static inline void arch_reg_restore(const struct regs *regs)
{
	arm_set_tls0(regs->tls0);
	arm_set_tls1(regs->tls1);
}

/** additional cleanup on context switch */
static inline void arch_reg_switch(void)
{
	/* clear exclusive state */
	arm_clrex();
}


/** initialize a thread's FPU state to sane defaults */
static inline void arch_fpu_init(struct regs *regs)
{
#ifdef __aarch64__
	regs->fpsr = 0;
	regs->fpcr = 0;
	for (unsigned int i = 0; i < 32; i++) {
		regs->fpregs[i] = 0;
	}
#else
	regs->fpscr = 0;
	regs->fp_padding = 0;
	for (unsigned int i = 0; i < 32; i++) {
		regs->fpregs[i] = 0;
	}
#endif
}

/** turn FPU on */
static inline void arch_fpu_enable(void)
{
#ifdef __aarch64__
	/* FPU always enabled for kernel */
#else
	arm_set_fpexc(FPEXC_EN);
#endif
}

/** turn FPU off */
static inline void arch_fpu_disable(void)
{
#ifdef __aarch64__
	/* FPU always enabled for kernel */
#else
	arm_set_fpexc(0);
#endif
}

/** save FPU state */
static inline void arch_fpu_save(struct regs *regs)
{
#ifdef __aarch64__
	arm_fpu_save(&regs->fpregs[0]);
#else
	arm_fpu_save(&regs->fpscr);
#endif
}

/** restore FPU state */
static inline void arch_fpu_restore(const struct regs *regs)
{
#ifdef __aarch64__
	arm_fpu_restore(&regs->fpregs[0]);
#else
	arm_fpu_restore(&regs->fpscr);
#endif
}

/** test if FPU is enabled */
static inline int arch_fpu_enabled(void)
{
#ifdef __aarch64__
	/* FPU always enabled for kernel */
	return 1;
#else
	return arm_get_fpexc() & FPEXC_EN;
#endif
}

/** allocate a register context frame on stack for a signal handler */
static inline addr_t arch_signal_alloc_frame(const struct regs *regs)
{
	addr_t stack;

	/* allocate space for struct regs */
	stack = regs->sp - sizeof(struct regs);

	/* align stack 16 bytes */
	stack &= -16ul;

	return stack;
}

/** make register context consistent for signal handling */
static inline void arch_signal_prepare(struct regs *regs)
{
	/* save TLS registers */
	arch_reg_save(regs);
}

/** setup registers to call signal handler of type sig_handler_t */
static inline void arch_signal_init(
	struct regs *regs,
	ulong_t handler,
	ulong_t frame,
	uint32_t sig_mask,
	unsigned int sig)
{
	regs->r[0] = frame;
	regs->r[1] = sig_mask;
	regs->r[2] = sig;

	/* the stack points to the saved register frame */
	regs->sp = frame;

	/* let function return to NULL address */
	regs->lr = 0;

	/* keep stack as is -- already properly aligned */
	regs->pc = handler;

#ifdef __aarch64__
	regs->cpsr = CPSR_USER64_BITS;
#else
	regs->cpsr = CPSR_USER32_BITS;
	if (handler & 0x1ul) {
		/* enable thumb mode */
		regs->cpsr |= CPSR_T;
	}
#endif
}

/** fixup registers after signal handling */
static inline void arch_signal_fixup(struct regs *regs)
{
#ifdef __aarch64__
	/* fixup CPSR */
	regs->cpsr &= CPSR_USER64_MASK;
	regs->cpsr |= CPSR_USER64_BITS;
#else
	regs->cpsr &= CPSR_USER32_MASK;
	regs->cpsr |= CPSR_USER32_BITS;
	if (regs->pc & 0x1ul) {
		/* enable thumb mode */
		regs->cpsr |= CPSR_T;
	}
#endif

	/* restore TLS registers */
	arch_reg_restore(regs);
}

#endif
