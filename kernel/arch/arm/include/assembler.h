/* SPDX-License-Identifier: MIT */
/*
 * assembler.h
 *
 * ARM specific assembler definitions
 *
 * azuepke, 2017-09-29: initial
 */

#ifndef __ASSEMBLER_H__
#define __ASSEMBLER_H__

/** load a 32-bit immediate
 *
 * This pseudo-instruction is available in the ARM compiler, but not in GCC.
 */
.macro mov32 reg val
	movw	\reg, #:lower16:\val
	movt	\reg, #:upper16:\val
.endm

#endif
