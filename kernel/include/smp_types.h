/* SPDX-License-Identifier: MIT */
/*
 * smp_types.h
 *
 * SMP remote call data types.
 *
 * azuepke, 2018-04-10: initial
 */

#ifndef __SMP_TYPES_H__
#define __SMP_TYPES_H__

#include <marron/types.h>
#include <stdint.h>
#include <arch.h>
#include <compiler.h>

// FIXME: this is currently limited to 4 CPUs!
#define SMP_MAX_CPUS 4

/** SMP remote call handler type */
typedef void (*smp_call_handler_t)(void *arg);

/** SMP call object */
struct smp_call {
	/** handler function to call on a remote CPU */
	smp_call_handler_t handler;
	/** argument to pass to the handler */
	void *arg;

	/** Counter for target CPU for an outgoing request.
	 * The value is set after each outgoing request is ready.
	 * The value encodes both an ever incrementing counter and the target CPU.
	 */
	uint32_t request;

	/** Counters for each source CPU:
	 * The counter is set to the last value of "request" of a source CPU
	 * after an incoming request was handled.
	 */
	uint32_t handled_id[SMP_MAX_CPUS];
} __aligned(ARCH_ALIGN);

/* generated data */
extern struct smp_call smp_call_dyn[];

#endif
