/* SPDX-License-Identifier: MIT */
/*
 * smp.h
 *
 * SMP remote calls.
 *
 * azuepke, 2018-04-10: initial
 */

#ifndef __SMP_H__
#define __SMP_H__

#include <smp_types.h>

#ifdef SMP

/** initialize SMP remote calls */
void smp_init(void);

/** SMP remote call of "handler" with argument "arg" on CPU "cpu" */
void smp_call(unsigned int cpu, smp_call_handler_t handler, void *arg);

/** Handle incoming SMP remote calls */
void smp_interrupt(void);

#endif

#endif
