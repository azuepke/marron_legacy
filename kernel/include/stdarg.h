/* SPDX-License-Identifier: MIT */
/*
 * stdarg.h
 *
 * GCC specific variable argument handling.
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2017-10-02: imported
 */

#ifndef __STDARG_H__
#define __STDARG_H__

#define va_start(v, l)		__builtin_va_start(v, l)
#define va_arg(v, l)		__builtin_va_arg(v, l)
#define va_end(v)			__builtin_va_end(v)
#define va_copy(d, s)		__builtin_va_copy(d, s)
typedef __builtin_va_list	va_list;

#endif
