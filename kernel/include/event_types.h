/* SPDX-License-Identifier: MIT */
/*
 * event_types.h
 *
 * Event data types.
 *
 * azuepke, 2018-04-10: initial
 */

#ifndef __EVENT_TYPES_H__
#define __EVENT_TYPES_H__

#include <marron/types.h>
#include <stdint.h>

/* forward declarations */
struct event_wait;
struct thread;

/** event_send configuration data */
struct event_send_cfg {
	/** pointer to waiter side */
	struct event_wait *event_wait;
	/** target CPU of event waiter */
	uint8_t target_cpu;
};

/** event_wait object */
struct event_wait {
	/** Currently waiting thread, if any */
	struct thread *waiter;
	/** Event pending */
	bool_t pending;
};

/* generated data */
extern const uint8_t event_send_num;
extern const struct event_send_cfg event_send_cfg[];

extern const uint8_t event_wait_num;
extern struct event_wait event_wait_dyn[];

#endif
