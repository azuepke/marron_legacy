/* SPDX-License-Identifier: MIT */
/*
 * stack.h
 *
 * User space stack handling
 *
 * azuepke, 2018-04-10: initial
 */

#ifndef __STACK_H__
#define __STACK_H__

#include <compiler.h>

/** define an opaque stack object "name" with "size_in_byte" stack inside */
#define DEFINE_STACK(name, size_in_bytes)	\
	struct {	\
		char stack[size_in_bytes];	\
	} name __aligned(16)	\

/** get the stack stack point for a stack object "name" */
#define GET_STACK(name) ((void*)(((addr_t)&(name)) + (sizeof(name))))

#endif
