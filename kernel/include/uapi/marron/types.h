/* SPDX-License-Identifier: MIT */
/*
 * marron/types.h
 *
 * Kernel types.
 *
 * azuepke, 2018-01-02: initial
 * azuepke, 2018-12-21: standalone version
 */

#ifndef __TYPES_H__
#define __TYPES_H__

/* partition states */
#define PART_STATE_IDLE			0	/**< partition is idle */
#define PART_STATE_RUNNING		1	/**< partition is running */

/* thread FPU states */
#define FPU_STATE_OFF	0	/**< FPU disabled for thread */
#define FPU_STATE_AUTO	1	/**< automatically enable the FPU on demand */
#define FPU_STATE_ON	2	/**< FPU enabled for thread */

/** number of supported priorities */
#define NUM_PRIOS 256

/** null timeout */
#define TIMEOUT_NULL 0ull

/** infinite timeout */
#define TIMEOUT_INFINITE 0xffffffffffffffffull

/* non-standard types */

/** integer type with the width of a machine register */
typedef unsigned long int ulong_t;

/** boolean */
typedef int bool_t;

/** timeout and time types */
#if defined __x86_64__ || \
    defined __aarch64__ || \
    defined __riscv_xlen && (__riscv_xlen == 64)
typedef unsigned long int sys_timeout_t;
typedef unsigned long int sys_time_t;
#else
typedef unsigned long long int sys_timeout_t;
typedef unsigned long long int sys_time_t;
#endif

/** error type */
typedef unsigned int err_t;

/** IRQ modes
 *
 * The bits are encoded in the following way:
 * - bit 2:  edge-triggered if set (level triggered else)
 * - bit 1:  active on high-level/rising edge
 * - bit 0:  active on low-level/falling edge
 */
typedef enum {
	/** Use IRQ default setting */
	IRQ_MODE_AUTO = 0x0,
	/** Level-triggered IRQ in low level */
	IRQ_MODE_LEVEL_LOW = 0x1,
	/** Level-triggered IRQ on high level */
	IRQ_MODE_LEVEL_HIGH = 0x2,
	/* mode 0x3 is not assigned -- this would encode "level both" */
	/* mode 0x4 is not assigned -- this would encode "edge nothing" */
	/** Edge-triggered IRQ on falling edge */
	IRQ_MODE_EDGE_FALLING = 0x5,
	/** Edge-triggered IRQ on rising edge */
	IRQ_MODE_EDGE_RISING = 0x6,
	/** Edge-triggered IRQ on both raising and falling edge */
	IRQ_MODE_EDGE_BOTH = 0x7,
} irq_mode_t;

/** Cache operations */
typedef enum {
	/** Clean data cache content and invalidate instruction caches
	 * for application loading. The BSP shall handle the caches up
	 * to the "point of unification" in the memory hierarchy,
	 * i.e. where split data and instruction caches fall back to a single
	 * unified cache or memory.
	 */
	CACHE_OP_INVAL_ICACHE_RANGE = 0,
	/** Flush data cache content for DMA.
	 * The BSP shall handle the caches up to the "point of coherency"
	 * in the memory hierarchy, i.e. where other DMA busmasters access
	 * the memory.
	 */
	CACHE_OP_FLUSH_DCACHE_RANGE = 1,
	/** Clean data cache content up to the "point of coherency" for DMA. */
	CACHE_OP_CLEAN_DCACHE_RANGE = 2,
	/** Invalidate data cache content up to the "point of coherency" for DMA.
	 * The referenced memory region must be writable to perform invalidation.
	 * If not properly aligned, the beginning and/or the end of the affected
	 * memory region is flushed.
	 */
	CACHE_OP_INVAL_DCACHE_RANGE = 3,
} cache_op_t;


/* Signals and exceptions
 *
 * This lists the signals and exceptions reported by the kernel.
 * All signals and exceptions must fit into a single 32-bit variable,
 * which forms a per-thread signal mask.
 * The signals which cannot be nested by user space comprise all exceptions
 * and the abort signal (see SIG_MASK_EXCEPTION).
 * For compatibility with POSIX, signal 0 is not used.
 */
#define SIG_ILLEGAL		1	/**< Illegal instruction exception */
#define SIG_DEBUG		2	/**< Breakpoint or watchpoint excepion */
#define SIG_TRAP		3	/**< Trap or breakpoint instruction exception */
#define SIG_ARITHMETIC	4	/**< Integer arithmetic error or overflow exception */
#define SIG_PAGEFAULT	5	/**< Invalid or unmapped memory address referenced exception */
#define SIG_ALIGN		6	/**< Unaligned memory access exception */
#define SIG_BUS			7	/**< Bus error (hardware reported exception) */
#define SIG_FPU_UNAVAIL	8	/**< FPU unavailable / disabled exception */
#define SIG_FPU_ERROR	9	/**< FPU error exception */
#define SIG_SYSCALL		10	/**< System call exception */
#define SIG_ABORT		11	/**< sys_abort() called (exception) */
#define SIG_CANCEL		12	/**< thread cancellation (signal) */

/** Number of supported signals */
#define NUM_SIGS 32

/** Set of all signals */
#define SIG_MASK_ALL	0xffffffff

/** Set of no signals */
#define SIG_MASK_NONE	0x00000000

/** Convert a signal to a signal mask value */
#define SIG_TO_MASK(sig) (1u<<(sig))

/** Mask of signals and exceptions which raise a partition error on nesting */
#define SIG_MASK_EXCEPTION	(\
							SIG_TO_MASK(SIG_ILLEGAL) | \
							SIG_TO_MASK(SIG_DEBUG) | \
							SIG_TO_MASK(SIG_TRAP) | \
							SIG_TO_MASK(SIG_ARITHMETIC) | \
							SIG_TO_MASK(SIG_PAGEFAULT) | \
							SIG_TO_MASK(SIG_ALIGN) | \
							SIG_TO_MASK(SIG_BUS) | \
							SIG_TO_MASK(SIG_FPU_UNAVAIL) | \
							SIG_TO_MASK(SIG_FPU_ERROR) | \
							SIG_TO_MASK(SIG_SYSCALL) | \
							SIG_TO_MASK(SIG_ABORT) | \
							0)

/* Exceptions
 *
 * For exceptions, the kernel further encodes information about the type of
 * the trap or exception in a virtual register named "exception":
 * - exception type (the exception types match their signal numbers)
 * - status flags, e.g. read, write, or execution faults
 *
 * The lower 4 bits encode the exception type.
 * Bits 4..15 decode common exception information bits.
 * Bits >= 16 may decode architecture specific information.
 */
#define EX_TYPE_NONE		0	/**< No fault ocurred (default exection state) */
#define EX_TYPE_ILLEGAL		1	/**< Illegal instruction exception */
#define EX_TYPE_DEBUG		2	/**< Breakpoint or watchpoint excepion */
#define EX_TYPE_TRAP		3	/**< Trap or breakpoint instrucion exception */
#define EX_TYPE_ARITHMETIC	4	/**< Integer arithmetic error or overflow exception */
#define EX_TYPE_PAGEFAULT	5	/**< Invalid or unmapped memory address referenced exception */
#define EX_TYPE_ALIGN		6	/**< Unaligned memory access exception */
#define EX_TYPE_BUS			7	/**< Bus error (hardware reported exception) */
#define EX_TYPE_FPU_UNAVAIL	8	/**< FPU unavailable / disabled exception */
#define EX_TYPE_FPU_ERROR	9	/**< FPU error exception */
#define EX_TYPE_SYSCALL		10	/**< System call exception */

/** Extract exception type */
#define EX_TYPE(x) ((x) & 0x000f)

/** Exception on read memory access.
 * Always valid for: PAGEFAULT
 * Optionally valid for: ALIGN, BUS, DEBUG
 */
#define EX_READ				0x0010

/** Exception on write memory access.
 * Always valid for: PAGEFAULT
 * Optionally valid for: ALIGN, BUS, DEBUG
 */
#define EX_WRITE			0x0020

/** Exception on instruction fetch memory access.
 * Always valid for: PAGEFAULT
 * Optionally valid for: ALIGN, BUS, DEBUG
 */
#define EX_EXEC				0x0040

/** Permission exception on memory access.
 * A TLB entry or page table entry exists, but the current access permissions
 * forbit the current access.
 * Always valid for: PAGEFAULT
 */
#define EX_PERM				0x0080

/** FPU enabled.
 * In the register context of the thread, the FPU registers are enabled.
 * Always valid for all exceptions
 */
#define EX_FPU_ENABLED		0x0100

#endif
