/* SPDX-License-Identifier: MIT */
/*
 * marron/api.h
 *
 * Kernel user API.
 *
 * azuepke, 2018-01-02: initial
 * azuepke, 2018-01-04: cleaned up for API documentation
 * azuepke, 2018-12-21: standalone version
 */

#ifndef __API_H__
#define __API_H__

#include <marron/types.h>
#include <marron/error.h>

/* compiler specific */
#define __noreturn __attribute__((__noreturn__))

/* forward */
typedef struct regs regs_t;

/** Abort execution of current partition
 *
 * \note A call to this function does not return.
 */
void sys_abort(void) __noreturn;

/** Print a character to the system console
 *
 * This function tries to print a character to the system console.
 * If the system console is currently busy, e.g. the serial FIFO is full,
 * the function returns an error.
 *
 * \param [in] c			Character to print
 *
 * \retval EOK				Success
 * \retval EBUSY			Console is busy, try again later
 */
err_t sys_putc(unsigned char c);

/** Start a thread at runtime with variable parameters
 *
 * This function activates thread "thr_id" if it is currently not active.
 * The thread's user space register context is initialized
 * with the given entry point, argument, stack pointer, and TLS pointer.
 * The initial priority is limited to the partition's maximum priority.
 * The assigned CPU must be within the set of CPUs available to the partition.
 * The thread's FPU state is set to FPU_STATE_AUTO.
 *
 * \param [in] thr_id		Thread ID
 * \param [in] entry		Entry point
 * \param [in] arg			Argument passed to thread
 * \param [in] stacḱ		Initial stack pointer
 * \param [in] tls			Pointer to TLS
 * \param [in] prio			Initial scheduling priority
 * \param [in] cpu_id		Assigned CPU
 *
 * \retval EOK				Success
 * \retval ELIMIT			If the given thread ID is out of bounds
 * \retval ESTATE			If the thread is currently active
 * \retval ELIMIT			If the given CPU ID is out the partition's CPU mask
 *
 * \see sys_thread_exit()
 */
err_t sys_thread_create(
          unsigned int thr_id,
          void (*entry)(void *),
          void *arg,
          void *stack,
          void *tls,
          unsigned int prio,
          unsigned int cpu_id);

/** Terminate the current thread
 *
 * This function terminates the current thread.
 *
 * \note A call to this function does not return.
 *
 * \see sys_thread_create()
 */
void sys_thread_exit(void) __noreturn;

/** Retrieve thread ID of calling thread
 *
 * This function returns the thread ID of the calling thread.
 *
 * \return Thread ID
 *
 * \see sys_part_self()
 */
unsigned int sys_thread_self(void);

/** Retrieve priority of calling thread
 *
 * This function returns the schedulign priority of the calling thread.
 *
 * \return Priority
 *
 * \see sys_prio_set()
 * \see sys_cpu_get()
 * \see sys_cpu_set()
 */
unsigned int sys_prio_get(void);

/** Change priority of calling thread
 *
 * This function changes the scheduling priority of the calling thread
 * to the given priority and returns its previous schedulign priority.
 * The priority is capped to the partition's maximum priority.
 *
 * \param [in] new_prio		New scheduling priority
 *
 * \return Priority
 *
 * \see sys_prio_get()
 * \see sys_cpu_get()
 * \see sys_cpu_set()
 */
unsigned int sys_prio_set(unsigned int new_prio);

/** Retrieve assigned CPU of calling thread
 *
 * This function returns the assigned CPU of the calling thread.
 *
 * \return CPU ID
 *
 * \see sys_prio_get()
 * \see sys_prio_set()
 * \see sys_cpu_set()
 * \see sys_part_cpus()
 */
unsigned int sys_cpu_get(void);

/** Change assigned CPU of calling thread
 *
 * This function changes the assigned CPU of the calling thread.
 * The assigned CPU must be within the set of CPUs available to the partition.
 * After migration, the thread is enqueued
 * at the tail its priority's ready queue.
 *
 * \param [in] cpu_id		New assigned CPU
 *
 * \retval EOK				Success
 * \retval ELIMIT			If the given CPU ID is out of bounds
 *
 * \see sys_prio_get()
 * \see sys_prio_set()
 * \see sys_cpu_get()
 * \see sys_part_cpus()
 */
err_t sys_cpu_set(unsigned int cpu_id);

/** Yield calling thread
 *
 * This function yields the calling thread on its current scheduling priority.
 * Other threads with the same scheduling priority become eligible
 * for scheduling.
 */
void sys_yield(void);

/** Retrieve current system time
 *
 * This function returns the current system time in nanoseconds since boot.
 *
 * \return Current system time in nanoseconds
 *
 * \see sys_time_resolution()
 * \see sys_sleep()
 */
sys_time_t sys_time_get(void);

/** Retrieve resolution of system timer
 *
 * This function returns the resolution of the system timer in nanoseconds.
 *
 * \return Resolution of system timer in nanoseconds
 *
 * \see sys_time_get()
 * \see sys_sleep()
 */
sys_time_t sys_time_resolution(void);

/** Sleep until timeout expires
 *
 * This function blocks the calling thread in the scheduler
 * until the given timeout in nanoseconds since boot expires.
 * If the timeout is in the past, the thread will at least yield.
 *
 * \param [in] timeout		Expiry time in nanoseconds, or TIMEOUT_INFINITE
 *
 * \retval EOK				Success (given timeout expired)
 *
 * \see sys_time_get()
 * \see sys_time_resolution()
 */
err_t sys_sleep(sys_timeout_t timeout);

/** Suspend calling thread
 *
 * Suspend the calling thread if there is no pending resume request.
 * In case of a pending resume request, consume the resume request
 * and return immediately.
 *
 * \retval EOK				Success (thread woken up from suspended state)
 * \retval EAGAIN			A suspension wakeup request was pending
 *
 * \see sys_thread_resume()
 */
err_t sys_thread_suspend(void);

/** Resume suspended thread
 *
 * Resume a suspended thread, or set a pending resume request if the thread
 * is not suspended.
 *
 * \retval EOK				Success
 * \retval ESTATE			If the thread is dead
 * \retval ELIMIT			If the given thread ID is out of bounds
 *
 * \see sys_thread_suspend()
 */
err_t sys_thread_resume(unsigned int thr_id);

/** Retrieve partition ID of calling thread
 *
 * This function returns the partition ID of the calling thread.
 *
 * \return Partition ID
 *
 * \see sys_thread_self()
 */
unsigned int sys_part_self(void);

/** Retrieve bitmask of CPUs available to the caller's partition
 *
 * This function returns the bitmask of CPUs
 * available to the caller's partition.
 *
 * \return Bitmask of available CPU
 *
 * \see sys_thread_self()
 */
ulong_t sys_part_cpus(void);

/** Retrieve caller's partition state
 *
 * A call to this function returns the status of the caller's partition.
 *
 * \return Partition state (0: IDLE, 1: RUNNING)
 */
unsigned int sys_part_state(void);

/** Shutdown caller's partition
 *
 * A call to this function shuts down the caller's partition.
 * The partition transitions to IDLE state.
 *
 * \note A call to this function does not return.
 */
void sys_part_shutdown(void) __noreturn;

/** Restart caller's partition
 *
 * A call to this function restarts the caller's partition.
 * The partition first transitions to IDLE state, then to RUNNING state again.
 *
 * \note A call to this function does not return.
 */
void sys_part_restart(void) __noreturn;

/** Retrieve partition state (privileged)
 *
 * A call to this function returns the status of the given partition.
 *
 * This operation is privileged and requires the PART_PERM_PART_OTHER privilege.
 *
 * \param [in] part_id		Partition ID
 * \param [out] state		Partition state (0: IDLE, 1: RUNNING)
 *
 * \retval EOK				Success
 * \retval EPERM			Calling partition is not privileged
 * \retval ELIMIT			If the given partition ID is out of bounds
 * \retval ELIMIT			If the partition ID references the idle partition
 */
err_t sys_part_state_other(unsigned int part_id, unsigned int *state);

/** Shutdown a partition (privileged)
 *
 * A call to this function shuts down the given partition.
 * The partition transitions to IDLE state (if not already in this state).
 *
 * This operation is privileged and requires the PART_PERM_PART_OTHER privilege.
 *
 * \param [in] part_id		Partition ID
 *
 * \retval EOK				Success
 * \retval EPERM			Calling partition is not privileged
 * \retval ELIMIT			If the given partition ID is out of bounds
 * \retval ELIMIT			If the partition ID references the idle partition
 *
 * \note A call to this function does not return
 * if the caller's partition is affected.
 */
err_t sys_part_shutdown_other(unsigned int part_id);

/** Restart a partition (privileged)
 *
 * A call to this function restarts the given partition.
 * The partition first transitions to IDLE state (if not already in this state),
 * then to RUNNING state again.
 *
 * This operation is privileged and requires the PART_PERM_PART_OTHER privilege.
 *
 * \param [in] part_id		Partition ID
 *
 * \retval EOK				Success
 * \retval EPERM			Calling partition is not privileged
 * \retval ELIMIT			If the given partition ID is out of bounds
 * \retval ELIMIT			If the partition ID references the idle partition
 *
 * \note A call to this function does not return
 * if the caller's partition is affected.
 */
err_t sys_part_restart_other(unsigned int part_id);

/** Reset or shutdown the system (privileged)
 *
 * Calling this function will reset or shutdown the system, depending on mode:
 * - If mode is 0, the system is reset.
 * - If mode is 1, the system is halted.
 * - If mode is 2, the system is powered off.
 *
 * This operation is privileged and requires the PART_PERM_SHUTDOWN privilege.
 *
 * \param [in] mode			Halt mode
 *
 * \retval EPERM			Calling partition is not privileged
 * \retval EINVAL			Invalid mode requested
 *
 * \note A call to this function does not return on success.
 */
err_t sys_bsp_shutdown(unsigned int mode);

/** Get BSP name
 *
 * This function returns the name of the BSP. The caller must specify
 * a string buffer which is large enough to hold the name.
 *
 * \param [out] name		Name string
 * \param [in] size			Size of string
 *
 * \retval EOK				Success
 * \retval ELIMIT			Provided string buffer size too short
 * \retval EFAULT			Invalid address or page fault
 */
err_t sys_bsp_name(char *name, ulong_t size);

/** Enable interrupt
 *
 * This function checks if an interrupt is available to the calling partition
 * and configures the interrupt with the given mode
 * in the hardware's interrupt controller.
 *
 * The interrupt itself is not enabled (unmasked) by this call, instead,
 * a thread must actively start waiting for the interrupt to unmask
 * the interrupt.
 *
 * When unsure about the interrupt mode, use IRQ_MODE_AUTO.
 *
 * \param [in] irq_id		IRQ ID
 * \param [in] mode			Interrupt mode
 *
 * \retval EOK				Success
 * \retval ELIMIT			If the given IRQ ID is not available to the partition
 * \retval ESTATE			If the interrupt is already enabled
 * \retval EBUSY			If the interrupt is not available on the platform
 * \retval ETYPE			If the requested interrupt mode is not supported
 *
 * \see sys_irq_disable()
 * \see sys_irq_wait()
 */
err_t sys_irq_enable(unsigned int irq_id, irq_mode_t mode);

/** Disable interrupt
 *
 * This function disables interrupt handling.
 * Any threads waiting in sys_irq_wait() are woken up with error code ECANCEL
 * and the interrupt is disabled (masked) in the hardware's interrupt
 * controller.
 *
 * \param [in] irq_id		IRQ ID
 *
 * \retval EOK				Success
 * \retval ELIMIT			If the given IRQ ID is not available to the partition
 * \retval ESTATE			If the interrupt is not enabled
 *
 * \see sys_irq_enable()
 * \see sys_irq_wait()
 */
err_t sys_irq_disable(unsigned int irq_id);

/** Wait for interrupt
 *
 * With this function, the calling thread waits for an interrupt to happen
 * or the given timeout to expire.
 * Calling this thread enables (unmasks) the interrupt in the interrupt
 * controller until the interrupt arrives and becomes disabled (masked) again
 * by the kernel.
 * The flags parameter is currently unused and must be set to 0.
 *
 * \param [in] irq_id		IRQ ID
 * \param [in] flags		Flags
 * \param [in] timeout		Expiry time in nanoseconds, or TIMEOUT_INFINITE
 *
 * \retval EOK				Success
 * \retval ELIMIT			If the given IRQ ID is not available to the partition
 * \retval ESTATE			If the interrupt is not enabled
 * \retval EAGAIN			If another thread is already waiting
 * \retval ECANCEL			If the interrupt was disabled while waiting
 * \retval EINVAL			If a non-zero flags parameter is set
 * \retval ETIMEOUT			If the timeout expired before the thread
 *							was woken up by the interrupt
 *
 * \see sys_irq_enable()
 * \see sys_irq_disable()
 */
err_t sys_irq_wait(unsigned int irq_id, unsigned int flags, sys_timeout_t timeout);

/** Cache management
 *
 * Perform a cache management operation on the given virtual memory region
 * denoted by "start" and "size". The "alias" argument is only used for
 * instruction cache handling to handle potential aliases in the instruction
 * caches and may refer to an address in a currently not active address space.
 * If the cache aliases, it is recommended to invalidate the whole instruction
 * cache if "alias" is not equal to "start".
 *
 * \param [in] op			Cache operation
 * \param [in] start		Start of memory region
 * \param [in] size			Size of memory region
 * \param [in] alias		Alias address in another address space
 *
 * \retval EOK				Success
 * \retval EFAULT			Invalid address or page fault
 */
err_t sys_cache(cache_op_t op, ulong_t start, ulong_t size, ulong_t alias);


/** Signal and exception handler type
 *
 * The kernel invokes a registered signal or exception handler of this type.
 * Besides the cause of the signal or exception, the handler receives
 * the previously interrupted register contexts of the current thread
 * and a mask of previously blocked signals as arguments.
 * For signals, the register context reflects the state where the thread
 * was interrupted.
 * For exceptions, the register context refers to the state where the thread
 * caused the exception.
 * The handler may change the register context or the signal mask.
 *
 * While an signal or exception handler is executing,
 * the currently handled signal is added to the list of blocked signals.
 * If the kernel encounters an exception which is currently blocked,
 * the exception is escalated to a partition error.
 *
 * \param [in] regs			Previous register context
 * \param [in] sig_mask		Mask of blocked signals
 * \param [in] sig			ID of current signal or exception
 *
 * \note The handler must not return, but finish its execution by tail-calling
 * sys_sig_return() to restore previous registers and the mask of previously
 * blocked signals.
 *
 * \see sys_sig_return()
 * \see sys_sig_register()
 * \see sys_sig_mask()
 * \see sys_sig_send()
 */
typedef void (*sig_handler_t)(regs_t *regs, unsigned int sig_mask, unsigned int sig);

/** Signal and exception context restorer
 *
 * A signal or exception handler uses this call to restore the register context
 * of the calling thread with the register context in "regs"
 * and the thread's previous mask of blocked signals in "mask".
 *
 * \param [in] regs			Register context to restore
 * \param [in] sig_mask		Mask of blocked signals to restore
 *
 * \note A call to this function does not return.
 *
 * \note On failures restoring the register context,
 * the exception is escalated to a partition error.
 *
 * \see sig_handler_t()
 * \see sys_sig_register()
 * \see sys_sig_mask()
 * \see sys_sig_send()
 */
void sys_sig_return(const regs_t *regs, unsigned int sig_mask) __noreturn;

/** Register signal or exception handler function
 *
 * This function registers the given handler function "handler" for a signal
 * or exception "sig".
 * The registered handler is used by all threads in the calling partition.
 * If "handler" is NULL, any previously registered handler is overwritten,
 * and, if the particular signal or exception is raised,
 * the signal or exception is directly escalated to a partition error.
 * While the registered handler is executing, additionally to the signal
 * or exception "sig", other signals in "sig_mask" will be blocked.
 * The flags parameter is currently unused and must be set to 0.
 *
 * \param [in] sig			ID of signal or exception to register a handler for
 * \param [in] handler		Handler callback for signal or exception
 * \param [in] sig_mask		Mask of blocked signals while handler is active
 * \param [in] flags		Flags
 *
 * \retval EOK				Success
 * \retval EINVAL			If "sig" refers to an invalid signal or exception
 * \retval EINVAL			If a non-zero flags parameter is set
 *
 * \see sig_handler_t()
 * \see sys_sig_return()
 * \see sys_sig_mask()
 * \see sys_sig_send()
 */
err_t sys_sig_register(unsigned int sig, sig_handler_t handler, unsigned int sig_mask, unsigned int flags);

/** Change set of blocked signals
 *
 * This function changes the set of blocked signals for the current thread:
 * The parameter "clear_mask" describes the set of signals to clear/allow,
 * while the parameter "set_mask" describes the set of signals to set/block.
 * The kernel first clears the thread's signal mask with the signals
 * from "clear_mask", then sets the signals in "set_mask".
 *
 * \param [in] clear_mask	Set of signals to clear (allow)
 * \param [in] set_mask		Set of signals to set (block)
 *
 * \return Previous signal mask before modification
 *
 * \note This function can be used in different ways:
 * - To retrieve the current set of blocked signals,
 *   use sys_sig_mask(SIG_MASK_NONE, SIG_MASK_NONE).
 * - To additional block a signal "sig",
 *   use sys_sig_mask(SIG_MASK_NONE, SIG_TO_MASK(sig)).
 * - To allow the signal "sig" again,
 *   use sys_sig_mask(SIG_TO_MASK(sig), SIG_MASK_NONE).
 * - To restore a previously saved signal mask "previous_mask",
 *   use sys_sig_mask(SIG_MASK_ALL, previous_mask).
 *
 * \see sig_handler_t()
 * \see sys_sig_return()
 * \see sys_sig_register()
 * \see sys_sig_send()
 */
unsigned int sys_sig_mask(unsigned int clear_mask, unsigned int set_mask);

/** Send signal to thread
 *
 * This function sends the signal "sig" to thread "thr_id".
 * If the signal is currently masked by the thread, the signal is recorded
 * and marked as pending until the thread enables the signal again.
 * Sending a signal with the ID of an exception (see SIG_MASK_EXCEPTION)
 * is not possible.
 * Sending signal 0 checks if the thread is alive and does not cause
 * a signal to be delivered.
 * Unlike POSIX, sending a signal to a thread does not unblock the thread.
 *
 * \param [in] thr_id		Thread ID
 * \param [in] sig			ID of signal
 *
 * \retval EOK				Success
 * \retval EINVAL			If the given signal ID is invalid
 * \retval EINVAL			If the given signal ID refers to an exception
 * \retval ELIMIT			If the given thread ID is out of bounds
 * \retval ESTATE			If the thread is dead
 *
 * \see sig_handler_t()
 * \see sys_sig_return()
 * \see sys_sig_register()
 * \see sys_sig_mask()
 */
err_t sys_sig_send(unsigned int thr_id, unsigned int sig);


/** Block current thread on user space locking object
 *
 * This function let the calling thread wait on the user space locking object
 * "futex" with the given timeout. Before waiting, the kernel compares
 * "futex" with the expected "compare" value and prevents the thread from
 * waiting if these values differ.
 * Multiple threads waiting on the same user space locking object are queued
 * in the order of their scheduling priority. If multiple threads have the same
 * priority, they get ordered by the time of their arrival in the wait queue.
 *
 * \param [in] futex		User space lock object
 * \param [in] compare		Compare value
 * \param [in] timeout		Expiry time in nanoseconds, or TIMEOUT_INFINITE
 *
 * \retval EOK				Success
 * \retval EINVAL			If the futex address refers to a NULL pointer
 * \retval EINVAL			If the futex address is not aligned to 4 bytes
 * \retval EFAULT			Invalid address or page fault accessing futex
 * \retval EAGAIN			If the futex does not have the expected value
 * \retval ETIMEOUT			If the timeout expired before the thread
 *							was woken up or requeued
 *
 * \see sys_futex_wake()
 * \see sys_futex_requeue()
 */
err_t sys_futex_wait(
	const unsigned int *futex,
	unsigned int compare,
	sys_timeout_t timeout);

/** Wake threads waiting on user space locking object
 *
 * This function wakes up up to "count" threads
 * currently waiting on the user space locking object "futex".
 * The call succeeds even if no threads are woken up.
 *
 * Waiting threads with the highest scheduling priority are woken up first.
 * If multiple threads have the same scheduling priority,
 * they are woken up in order of the time of their arrival in the wait queue.
 *
 * \param [in] futex		User space lock object
 * \param [in] count		Number of threads to wake up
 *
 * \retval EOK				Success
 * \retval EINVAL			If the futex address refers to a NULL pointer
 * \retval EINVAL			If the futex address is not aligned to 4 bytes
 *
 * \note If no further threads are waiting on the user space locking object
 * after wakeup, the kernel frees the internal associated wait queue.
 *
 * \see sys_futex_wait()
 * \see sys_futex_requeue()
 */
err_t sys_futex_wake(
	const unsigned int *futex,
	unsigned int count);

/** Wake and/or requeue threads waiting on user space locking object
 *
 * This function wakes up up to "count" threads
 * currently waiting on the user space locking object "futex"
 * if the value of "futex" still matches "compare".
 * After waking up the threads, the function additionally requeues (moves)
 * "count2" threads to a second user space locking object "futex2".
 * The call succeeds even if no threads are woken up or requeued.
 *
 * Waiting threads with the highest scheduling priority are woken up
 * or requeued first. On requeueing,
 * multiple threads waiting on the same user space locking object are queued
 * in the order of their scheduling priority. If multiple threads have the same
 * priority, they get ordered by the time of their arrival in the wait queue.
 * Also, requeued threads have their timeouts cleared.
 *
 * \param [in] futex		First user space lock object to wake/move threads from
 * \param [in] count		Number of threads to wake up
 * \param [in] compare		Compare value for first user space lock object
 * \param [in] futex2		Second user space lock object to requeue threads to
 * \param [in] count2		Number of threads to requeue to futex2
 *
 * \retval EOK				Success
 * \retval EINVAL			If the first futex address refer to a NULL pointer
 * \retval EINVAL			If the second futex address refer to a NULL pointer,
 *							but "count2" is greater than zero
 * \retval EINVAL			If the futex addresses are not aligned to 4 bytes
 * \retval EINVAL			If "futex" and "futex2" refer to the same address
 * \retval EFAULT			Invalid address or page fault accessing futexes
 * \retval EAGAIN			If the first futex does not have the expected value
 *
 * \see sys_futex_wait()
 * \see sys_futex_wake()
 */
err_t sys_futex_requeue(
	const int *futex,
	unsigned int count,
	int compare,
	const int *futex2,
	unsigned int count2);

/** Retrieve current FPU state of calling thread
 *
 * This function returns the assigned CPU of the calling thread.
 *
 * \return FPU state (one of FPU_STATE_OFF, FPU_STATE_AUTO, or FPU_STATE_ON)
 *
 * \see sys_fpu_state_set()
 */
unsigned int sys_fpu_state_get(void);

/** Change FPU state of calling thread
 *
 * This function changes the FPU state of the calling thread.
 * The FPU state is one of the following value:
 * - FPU_STATE_OFF: 	disable the FPU
 * - FPU_STATE_AUTO:	automatically enable the FPU on demand
 * - FPU_STATE_ON:		enable the FPU
 * Any current FPU registers are preserved when changing the FPU state.
 * In FPU_STATE_OFF and FPU_STATE_AUTO state, the FPU is disabled
 * for the calling to shorten context switch times.
 *
 * \param [in] fpu_state	FPU state
 *
 * \retval EOK				Success
 * \retval EINVAL			If the FPU state is invalid
 *
 * \see sys_fpu_state_get()
 */
err_t sys_fpu_state_set(unsigned int fpu_state);

/** Wait for event
 *
 * If no notification event is pending in the given event wait channel,
 * this function blocks the calling thread in the scheduler
 * until the given timeout in nanoseconds since boot expires,
 * or an event is send.
 * Otherwise the function returns immediately
 * If the timeout is in the past, the thread will at least yield.
 * The flags parameter is currently unused and must be set to 0.
 *
 * \param [in] event_wait_id	Event channel ID
 * \param [in] flags		Flags
 * \param [in] timeout		Expiry time in nanoseconds, or TIMEOUT_INFINITE
 *
 * \retval EOK				Success
 * \retval ELIMIT			If the given event wait channel ID is out of bounds
 * \retval EINVAL			If a non-zero flags parameter is set
 * \retval EAGAIN			If another thread is already waiting
 * \retval ETIMEOUT			If the timeout expired before the thread
 *							was woken up by a partition notification event
 *
 * \see sys_event_send()
 */
err_t sys_event_wait(unsigned int event_wait_id, unsigned int flags, sys_timeout_t timeout);

/** Send event
 *
 * Send a notification event to the given event send channel.
 * If a thread is currently waiting on the event,
 * it will be woken up. Otherwise the event is marked as pending in the channel.
 *
 * \param [in] event_send_id	Event channel ID
 *
 * \retval EOK				Success
 * \retval ELIMIT			If the given event send channel ID is out of bounds
 *
 * \see sys_event_wait()
 */
err_t sys_event_send(unsigned int event_send_id);

#endif
