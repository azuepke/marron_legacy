/* SPDX-License-Identifier: MIT */
/*
 * marron/error.h
 *
 * Error codes.
 *
 * azuepke, 2013-05-09: initial
 * azuepke, 2017-10-02: imported and adapted
 * azuepke, 2018-01-04: cleaned up for API documentation
 * azuepke, 2018-12-21: standalone version
 */

#ifndef __ERROR_H__
#define __ERROR_H__

/* error codes */
#define EOK			0	/**< no error */
#define ENOSYS		1	/**< function not implemented */
#define EINVAL		2	/**< invalid argument */
#define ELIMIT		3	/**< value outside configured limits */
#define EFAULT		4	/**< bad address */
#define ETIMEOUT	5	/**< timer expired / timeout in the past */
#define EAGAIN		6	/**< try again */
#define EBUSY		7	/**< resource busy */
#define ESTATE		8	/**< resource in wrong state */
#define ETYPE		9	/**< resource has wrong type */
#define EPERM		10	/**< operation not permitted */
#define ECANCEL		11	/**< a blocking operation was cancelled */

#endif
