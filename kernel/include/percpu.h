/* SPDX-License-Identifier: MIT */
/*
 * percpu.h
 *
 * Per-CPU data.
 *
 * azuepke, 2018-03-05: moved current*() getters from kernel.h
 */

#ifndef __PERCPU_H__
#define __PERCPU_H__

#include <percpu_types.h>
#include <arch_percpu.h>
#include <bsp.h>
#include <sched.h>
#include <thread.h>
#include <part.h>

/* "per-CPU" API */
/** update current thread and current partition in per-CPU data */
static inline void percpu_thread_switch(struct thread *thr)
{
	arch_percpu()->current_thread = thr;
}

/* "current" API */
/** get current CPU via BSP */
static inline unsigned int current_cpu_id_safe(void)
{
#ifdef SMP
	return bsp_cpu_id();
#else
	return 0;
#endif
}

/** get current CPU via per-CPU area */
static inline unsigned int current_cpu_id(void)
{
	// also possible, but too slow
	//return current_cpu_id_safe();
#ifdef SMP
	return arch_percpu()->cpu_id;
#else
	return 0;
#endif
}

/** get current thread */
static inline struct thread *current_thread(void)
{
	// also possible, but too slow
	//return sched_per_cpu(current_cpu_id())->current;
	return arch_percpu()->current_thread;
}

/** get current partition config */
static inline const struct part_cfg *current_part_cfg(void)
{
	return current_thread()->part_cfg;
}

#endif
