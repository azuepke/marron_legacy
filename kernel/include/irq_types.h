/* SPDX-License-Identifier: MIT */
/*
 * irq_types.h
 *
 * IRQ object data types.
 *
 * azuepke, 2018-01-13: initial
 */

#ifndef __IRQ_TYPES_H__
#define __IRQ_TYPES_H__

#include <marron/types.h>
#include <stdint.h>

/* forward declarations */
struct part_cfg;
struct thread;

/** IRQ configuration data */
struct irq_cfg {
	/** global IRQ ID */
	unsigned int irq_id;
};

/** IRQ object */
struct irq {
	/** pointer to associated partition */
	const struct part_cfg *part_cfg;
	/** waiting thread */
	struct thread *waiter;
	/** cached IRQ ID (const after init) */
	unsigned int irq_id;
	/** IRQ is enabled */
	bool_t enabled;
	/** statistic counter */
	unsigned int count;
};

/* generated data */
extern const uint8_t irq_num;
extern const struct irq_cfg irq_cfg[];
extern struct irq irq_dyn[];

extern const uint16_t irq_table_num;
extern struct irq * const irq_table[];

#endif
