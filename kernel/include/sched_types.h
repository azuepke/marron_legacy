/* SPDX-License-Identifier: MIT */
/*
 * sched_types.h
 *
 * Scheduler data types
 *
 * azuepke, 2017-12-31: initial
 * azuepke, 2018-01-13: move types to dedicated header
 */

#ifndef __SCHED_TYPES_H__
#define __SCHED_TYPES_H__

#include <marron/types.h>
#include <list.h>
#include <compiler.h>
#include <arch.h>

/* forward declarations */
struct thread;

/*
 * How the scheduler works:
 * - we have an array of linked lists for each priority
 * - we keep threads in FIFO order in these per-priority lists
 * - the currently running thread is not kept on the ready queue
 *   (we assume frequent priority changes)
 * - the idle thread has a scheduling priority of -1
 * - we do not keep the idle thread in the ready queue as well
 * To find the next thread to schedule, we use a priority tracking bitmap
 * for each priority level. If the bit is set in the tracking bitmap,
 * we know that the priority's ready queue list is not empty.
 * We also cache the highest priority level which is not empty in "highest_prio"
 */

/** per-CPU scheduling data */
struct sched {
	/** currently scheduled thread */
	struct thread *current;
	/** idle thread to schedule if the ready queues are empty (const after init) */
	struct thread *idle;

	/** next highest priority, set to -1 if ready queues are empty */
	int next_highest_prio;

	/** assigned CPU (const after init) */
	uint8_t cpu;
	/** pending rescheduling */
	uint8_t reschedule;
	uint8_t padding2;
	uint8_t padding3;

	/** the ready queues */
	list_t readyqh[NUM_PRIOS];
	/** bitmask of active priority levels */
	ulong_t tracking_bitmap[NUM_PRIOS / sizeof(ulong_t)];

	/** timeout queue */
	list_t timeoutqh;
} __aligned(ARCH_ALIGN);

/* generated data */
extern const uint8_t cpu_num;
extern const uint8_t sched_num;
extern struct sched sched_dyn[];

#endif
