/* SPDX-License-Identifier: MIT */
/*
 * sched.h
 *
 * Scheduler
 *
 * azuepke, 2017-12-31: initial
 * azuepke, 2018-01-13: move types to dedicated header
 */

#ifndef __SCHED_H__
#define __SCHED_H__

#include <sched_types.h>
#include <assert.h>
#include <arch.h>

/* API */
/** get scheduler data for a given CPU */
static inline struct sched *sched_per_cpu(unsigned int cpu_id)
{
	assert(cpu_id < sched_num);
	return &sched_dyn[cpu_id];
}

/** initialize per-CPU scheduling data, assuming only the idle thread is active */
void sched_init(struct sched *sched, struct thread *idle, unsigned int cpu);

/** find and remove highest thread on ready queue */
void sched_readyq_next(struct sched *sched);

/** preempt the current thread */
void sched_preempt(struct sched *sched);

/** check if we need to preempt the current thread */
void sched_check_preempt(struct sched *sched);

/** current thread waits, with state, timeout and error code already set */
void sched_wait(struct sched *sched, struct thread *thr);

/** remove a thread from the ready queue */
void sched_remove(struct sched *sched, struct thread *thr);

/** release the timeout of a blocked thread */
void sched_timeout_release(struct sched *sched, struct thread *thr);

/** wake a thread and schedule */
void sched_wakeup(struct sched *sched, struct thread *thr);

/** expire all currently pending timeouts and wake waiting threads */
void sched_timeout_expire(sys_time_t now, struct sched *sched);

/** yield current thread */
void sched_yield(struct sched *sched, struct thread *thr);

/** migrate current thread to a new CPU */
void sched_migrate(struct sched *sched, struct thread *thr);

#endif
