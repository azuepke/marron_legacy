/* SPDX-License-Identifier: MIT */
/*
 * adspace_types.h
 *
 * Adspace configuration data.
 *
 * azuepke, 2018-03-05: initial
 */

#ifndef __ADSPACE_TYPES_H__
#define __ADSPACE_TYPES_H__

#include <arch_adspace_types.h>

/* generated data */
extern const struct arch_adspace_cfg adspace_cfg[];

#endif
