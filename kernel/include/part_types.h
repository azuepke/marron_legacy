/* SPDX-License-Identifier: MIT */
/*
 * part_types.h
 *
 * Partition data types.
 *
 * azuepke, 2017-11-01: initial
 * azuepke, 2018-01-13: move types to dedicated header
 */

#ifndef __PART_TYPES_H__
#define __PART_TYPES_H__

#include <marron/types.h>
#include <list.h>

/* partition permissions */
#define PART_PERM_SHUTDOWN		0x1	/* sys_bsp_shutdown() allowed */
#define PART_PERM_PART_OTHER	0x2	/* sys_part_*_other() allowed */

/* forward declarations */
struct thread;
struct part;
struct memrq_cfg;
struct irq_cfg;
struct irq;
struct event_wait;
struct event_send_cfg;


/** Partition configuration data */
struct part_cfg {
	/** pointer to runtime data */
	struct part *part;

	/** Partition name */
	const char *name;

	/** Available CPUs */
	uint32_t cpu_mask;
	/** Partition ID */
	uint8_t id;
	/** The partition's initial state (PART_STATE_xxx) */
	uint8_t initial_state;
	/** Permissios (PART_PERM_xxx) */
	uint8_t perm;
	/** Maximum priority */
	uint8_t max_prio;

	/** Number of memory requirements */
	uint8_t memrq_num;
	/** Number of threads */
	uint8_t thread_num;
	/** Number of assigned IRQ configs */
	uint8_t irq_num;

	/** Number of assigned event_wait objects */
	uint8_t event_wait_num;
	/** Number of assigned event_send configs */
	uint8_t event_send_num;
	/** First assigned CPU */
	uint8_t first_cpu;
	uint8_t padding;

	/** main entry point */
	addr_t entry;
	/** base address of main stack */
	addr_t stack_base;
	/** size of main stack */
	size_t stack_size;

	/** Array of memory requirements */
	const struct memrq_cfg *memrq_cfg;
	/** Array of thread objects */
	struct thread *thread;
	/** Array of IRQ config data */
	const struct irq_cfg *irq_cfg;
	/** Array of event_wait objects */
	struct event_wait *event_wait;
	/** Array of event_send configs */
	const struct event_send_cfg *event_send_cfg;
};

/** Partition object */
struct part {
	/** pointer to configuration data */
	const struct part_cfg *part_cfg;

	/** Partition state */
	uint8_t state;

	/* Process specific attributes (signal and exception handling) */
	/** User registered handlers for signals and exceptions */
	addr_t sig_handler[NUM_SIGS];
	/** Mask of blocked signals while executing a handler */
	uint32_t sig_mask[NUM_SIGS];

	/** Futex wait queue head -- list of threads blocked on a futex */
	list_t futex_waitqh;
};

/* generated data */
extern const uint8_t part_num;
extern struct part part_dyn[];
extern const struct part_cfg part_cfg[];

#endif
