/* SPDX-License-Identifier: MIT */
/*
 * percpu_types.h
 *
 * Per-CPU data types.
 *
 * azuepke, 2018-03-05: initial
 */

#ifndef __PERCPU_TYPES_H__
#define __PERCPU_TYPES_H__

#include <arch_percpu_types.h>

/* forward declarations */
struct sched;
struct thread;
struct percpu;
struct part_cfg;

/** per-CPU configuration data */
struct percpu_cfg {
	/** pointer to per-CPU data */
	struct percpu *percpu;
	/** pointer to per-CPU idle thread stack top */
	void *idle_stack_top;
};

/** per-CPU object */
struct percpu {
	/** architecture specific CPU-private data */
	struct arch_percpu arch;

	/** pointer to current thread */
	struct thread *current_thread;

	/** current CPU ID */
	unsigned int cpu_id;
};

/* generated data */
extern const struct percpu_cfg percpu_cfg[];
/* NOTE: the per-CPU dyn variables are not kept in an array,
 * but in CPU-specific linker sections
 */

#endif
