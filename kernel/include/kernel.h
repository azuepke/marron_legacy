/* SPDX-License-Identifier: MIT */
/*
 * kernel.h
 *
 * Kernel common functions.
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2017-10-02: imported and adapted
 * azuepke, 2018-03-09: more documentation
 */

#ifndef __KERNEL_H__
#define __KERNEL_H__

#include <marron/types.h>
#include <stdint.h>

/* buildid.c */
/** Kernel build ID string
 *
 * The build ID string is printed by the kernel at boot time.
 */
extern const char kernel_buildid[];

/* main.c */
/** Kernel C entry point
 *
 * The architecture layer calls the kernel entry point on each CPU.
 */
void kernel_main(void);

/** Idle loop entry point
 *
 * The kernel idle threads comprises a logical thread, but executes in kernel
 * space. The idle thread has the lowest possible priority in the system.
 */
void kernel_idle(void);

/** Scheduling function of the kernel
 *
 * This function schedules to another thread, if necessary,
 * and returns the next register context to return to.
 * The architecture layer calls this function on at kernel exit
 * from system calls, interrupts, and exceptions.
 */
struct regs *kernel_schedule(void);

/** Kernel timer callback
 *
 * The BSP calls this functions with the current system time when a timer
 * interrupt happens. The kernel reprograms the next timer expiry, if needed.
 * Also, the timer interrupt should never be masked during interrupt handling.
 *
 * This function must only be called during interrupt handling.
 */
void kernel_timer(uint64_t time);

/** Kernel IRQ callback
 *
 * When an interrupt happens, the BSP calls this function to notify the kernel
 * to wake waiting threads. The parameter "irq_id" refers to the interrupt ID.
 * The kernel should then in turn wake waiting threads, for example.
 * When the BSP calls kernel_irq(), the interrupt source must be masked.
 * The kernel will unmask the interrupt source again when the woken thread
 * starts waiting for the next interrupt.
 *
 * This function must only be called during interrupt handling.
 */
void kernel_irq(unsigned int irq_id);

#ifdef SMP
/** Kernel SMP rescheduling callback
 *
 * The BSP calls this function when the current CPU received
 * an IPI (inter-processor interrupt) requesting scheduling on a remote CPU.
 * Also, the IPI interrupt should never be masked during interrupt handling.
 *
 * This function must only be called during interrupt handling,
 * and this function is only necessary for multiprocessor systems.
 */
void kernel_ipi(void);
#endif

/** Kernel exception entry point
 *
 * The architecture layer calls this function to notify the kernel about
 * a synchronous exception in the current thread.
 * The parameter "info" refers to exception information.
 */
void kernel_exception(struct regs *regs, unsigned long info);

/** print error header */
void kernel_print_current(void);

#endif
