/* SPDX-License-Identifier: MIT */
/*
 * part.h
 *
 * Partition data types.
 *
 * azuepke, 2017-11-01: initial
 * azuepke, 2018-01-13: move types to dedicated header
 */

#ifndef __PART_H__
#define __PART_H__

#include <part_types.h>

/* API */
/** initialize all partition objects at boot time */
void part_init_all(void);

/** start a partition and its threads */
void part_start(const struct part_cfg *cfg);

/** stop a partition and its threads */
void part_stop(const struct part_cfg *cfg);

/** restart partition */
void part_restart(const struct part_cfg *cfg);

/** raise a partition error (always to currently running partition) */
void part_error(struct part *part, unsigned int sig);

#endif
