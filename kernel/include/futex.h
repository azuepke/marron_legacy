/* SPDX-License-Identifier: MIT */
/*
 * futex.h
 *
 * Futex implementation
 *
 * azuepke, 2018-03-19: initial
 * azuepke, 2018-04-19: simplified implementation
 */

#ifndef __FUTEX_H__
#define __FUTEX_H__

#include <marron/types.h>
#include <stdint.h>

/* forward declaration */
struct thread;

/* API */
/** Block current thread on user space locking object */
err_t futex_wait(
	addr_t futex_addr,
	int compare,
	sys_timeout_t timeout);

/** Callback for cleanup during wakeup (timeout, thread deletion) */
void futex_wait_cancel(
	struct thread *thr);

/** Wake threads waiting on user space locking object */
err_t futex_wake(
	addr_t futex_addr,
	unsigned int count);

/** Wake and/or requeue threads waiting on user space locking object */
err_t futex_requeue(
	addr_t futex_addr,
	unsigned int count,
	int compare,
	addr_t futex2_addr,
	unsigned int count2);

#endif
