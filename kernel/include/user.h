/* SPDX-License-Identifier: MIT */
/*
 * user.h
 *
 * User space accessors
 *
 * azuepke, 2018-03-22: initial
 */

#ifndef __USER_H__
#define __USER_H__

#include <marron/types.h>
#include <stdint.h>

/** copy data from current user space to kernel space */
err_t user_copy_in(void *to_kernel, const void *from_user, size_t size);
/** copy data from kernel space to current user space */
err_t user_copy_out(void *to_user, const void *from_kernel, size_t size);

#endif
