/* SPDX-License-Identifier: MIT */
/*
 * user.c
 *
 * User space accessors
 *
 * azuepke, 2018-03-22: initial
 */

#include <marron/error.h>
#include <user.h>
#include <part.h>
#include <memrq_types.h>
#include <percpu.h>
#include <string.h>


/** check an address range in the current user space if the requested access is OK */
static bool_t part_check_addr_range(
	addr_t addr,
	size_t size,
	unsigned int access)
{
	const struct part_cfg *cfg;
	const struct memrq_cfg *m;
	unsigned int i;

	assert(size > 0);
	assert((access & (MEMRQ_READ | MEMRQ_WRITE | MEMRQ_EXEC)) != 0);

	if (addr + size < addr) {
		/* address space overflow */
		return false;
	}

	cfg = current_part_cfg();

	for (i = 0; i < cfg->memrq_num; i++) {
		m = &cfg->memrq_cfg[i];
		if ((addr >= m->addr) &&
		    (addr + size <= m->addr + m->size) &&
		    ((access & m->access) == access)) {
			return true;
		}
	}

	return false;
}

/** copy data from current user space to kernel space */
err_t user_copy_in(void *to_kernel, const void *from_user, size_t size)
{
	assert(to_kernel != NULL);
	assert(size > 0);

	if (!part_check_addr_range((addr_t)from_user, size, MEMRQ_READ)) {
		return EFAULT;
	}

	memcpy(to_kernel, from_user, size);
	return EOK;
}

/** copy data from kernel space to current user space */
err_t user_copy_out(void *to_user, const void *from_kernel, size_t size)
{
	assert(from_kernel != NULL);
	assert(size > 0);

	if (!part_check_addr_range((addr_t)to_user, size, MEMRQ_WRITE)) {
		return EFAULT;
	}

	memcpy(to_user, from_kernel, size);
	return EOK;
}
