/* SPDX-License-Identifier: MIT */
/*
 * part.c
 *
 * Partitioning
 *
 * azuepke, 2018-01-02: initial
 */

#include <part.h>
#include <thread.h>
#include <compiler.h>
#include <assert.h>
#include <irq.h>
#include <kernel.h>
#include <stdio.h>
#include <percpu.h>
#include <smp.h>


/** initialize all partition objects at boot time */
__init void part_init_all(void)
{
	const struct part_cfg *cfg;
	struct thread *thr;
	struct part *part;

	for (unsigned int i = 0; i < part_num; i++) {
		cfg = &part_cfg[i];
		part = &part_dyn[i];

		part->part_cfg = cfg;
		part->state = PART_STATE_IDLE;

		/* empty futex queue */
		list_head_init(&part->futex_waitqh);

		/* clear all signal handlers */
		for (unsigned int sig = 0; sig < NUM_SIGS; sig++) {
			part->sig_handler[sig] = 0;
			part->sig_mask[sig] = 0;
		}

		/* assign IRQs to partition */
		for (unsigned int j = 0; j < cfg->irq_num; j++) {
			irq_assign_part(irq_by_id(cfg->irq_cfg[j].irq_id), cfg);
		}

		/* bind all threads to the partition */
		for (unsigned int j = 0; j < cfg->thread_num; j++) {
			thr = &cfg->thread[j];

			thread_bind_part(thr, j, cfg);
		}
	}
}

/** start a partition and its threads */
static void part_start_internal(void *arg)
{
	const struct part_cfg *cfg;
	struct part *part = arg;

	assert(part != NULL);

	if (part->state == PART_STATE_RUNNING) {
		return;
	}

	assert(part->state == PART_STATE_IDLE);
	part->state = PART_STATE_RUNNING;

	/* clear all signal handlers */
	for (unsigned int sig = 0; sig < NUM_SIGS; sig++) {
		part->sig_handler[sig] = 0;
		part->sig_mask[sig] = 0;
	}

	/* start first thread at the partition entry point */
	cfg = part->part_cfg;
	assert(cfg != NULL);
	thread_start(&cfg->thread[0],
	             cfg->entry, 0, cfg->stack_base + cfg->stack_size, 0,
	             cfg->max_prio,
	             cfg->first_cpu);
}

/** start a partition and its threads */
void part_start(const struct part_cfg *cfg)
{
	struct part *part;

	assert(cfg != NULL);
	assert(cfg->id != 0);
	part = cfg->part;
	assert(part != NULL);

#ifdef SMP
	if (cfg->first_cpu != current_cpu_id()) {
		smp_call(cfg->first_cpu, part_start_internal, part);
		return;
	}
#endif

	part_start_internal(part);
}

/** stop a partition and its threads (internal function) */
static void part_stop_internal(void *arg)
{
	const struct part_cfg *cfg;
	struct thread *thr;
	struct part *part;

	part = arg;

	assert(part != NULL);
	assert(part->part_cfg->id != 0);

	if (part->state == PART_STATE_IDLE) {
		return;
	}

	assert(part->state == PART_STATE_RUNNING);
	part->state = PART_STATE_IDLE;

	cfg = part->part_cfg;

	/* kill all interrupts */
	for (unsigned int i = 0; i < cfg->irq_num; i++) {
		irq_shutdown(irq_by_id(cfg->irq_cfg->irq_id));
	}

	/* kill all threads */
	for (unsigned int i = 0; i < cfg->thread_num; i++) {
		thr = &cfg->thread[i];

		thread_kill(thr);
	}
}

/** stop a partition and its threads */
void part_stop(const struct part_cfg *cfg)
{
	struct part *part;

	assert(cfg != NULL);
	assert(cfg->id != 0);
	part = cfg->part;
	assert(part != NULL);

#ifdef SMP
	if (cfg->first_cpu != current_cpu_id()) {
		smp_call(cfg->first_cpu, part_stop_internal, part);
		return;
	}
#endif

	part_stop_internal(part);
}

/** restart partition (internal function) */
static void part_restart_internal(void *arg)
{
	struct part *part = arg;

	assert(part != NULL);

	/* restart partition */
	if (part->state == PART_STATE_RUNNING) {
		part_stop_internal(part);
	}
	assert(part->state == PART_STATE_IDLE);
	part_start_internal(part);
	assert(part->state == PART_STATE_RUNNING);
}

/** restart partition */
void part_restart(const struct part_cfg *cfg)
{
	struct part *part;

	assert(cfg != NULL);
	assert(cfg->id != 0);
	part = cfg->part;
	assert(part != NULL);

#ifdef SMP
	if (cfg->first_cpu != current_cpu_id()) {
		smp_call(cfg->first_cpu, part_restart_internal, part);
		return;
	}
#endif

	part_restart_internal(part);
}

/** raise a partition error (always to currently running partition) */
void part_error(struct part *part, unsigned int sig)
{
	assert(part != NULL);
	assert((sig != 0) && (sig < NUM_SIGS));

	// FIXME: debug output to help debugging
	kernel_print_current();
	printf("partition error %d, partition stopped\n", sig);

	part_stop_internal(part);
}
