/* SPDX-License-Identifier: MIT */
/*
 * panic.c
 *
 * Kernel panic.
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2017-10-02: imported and adapted
 */

#include <panic.h>
#include <assert.h>
#include <bsp.h>
#include <stdio.h>
#include <arch.h>
#include <percpu.h>

static bool_t panic_in_progress = false;

static __cold void panic_pre(void)
{
	if (panic_in_progress) {
		bsp_halt(BOARD_STOP);
		for (;;) {
			;
		}
	}

	panic_in_progress = true;

	printf("\nKernel panic on CPU %d: ", current_cpu_id_safe());
}

static __cold __noreturn void panic_post(void)
{
#ifdef AUTOBUILD
	printf("AUTOBUILD: halt on panic or assert in kernel\n");
	bsp_halt(BOARD_POWEROFF);
#endif

	bsp_halt(BOARD_PANIC);
	for (;;) {
		;
	}
}

/** kernel panic */
void panic(const char* format, ...)
{
	va_list args;

	panic_pre();

	va_start(args, format);
	vprintf(format, args);
	va_end(args);

	panic_post();
}

/** panic + register dump */
void panic_regs(struct regs *regs, const char* format, ...)
{
	va_list args;

	panic_pre();

	va_start(args, format);
	vprintf(format, args);
	va_end(args);

	arch_dump_registers(regs);

	panic_post();
}

#ifndef NDEBUG
/** kernel assertion */
void __assert_fail(const char *cond, const char *file, int line, const char *func)
{
	printf("%s:%d: %s: assertion '%s' failed.\n", file, line, func, cond);

	panic_post();
}
#endif
