/* SPDX-License-Identifier: MIT */
/*
 * strlen.c
 *
 * simple byte-wise strlen
 *
 * azuepke, 2013-03-22
 */

#include <string.h>

size_t strlen(const char *s)
{
	size_t i;

	for (i = 0; s[i] != '\0'; i++) {
		/* nothing */
	}

	return i;
}
