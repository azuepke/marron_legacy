/* SPDX-License-Identifier: MIT */
/*
 * thread.c
 *
 * Threading
 *
 * azuepke, 2018-01-02: initial
 */

#include <thread.h>
#include <part.h>
#include <sched.h>
#include <percpu.h>
#include <adspace_types.h>
#include <sig.h>
#include <irq.h>
#include <futex.h>
#include <kernel.h>
#include <event.h>


/** initialize all thread objects at boot time to be blocked in STATE_DEAD */
__init void thread_init_all(void)
{
	for (unsigned int i = 0; i < thread_num; i++) {
		struct thread *thr = &thread_dyn[i];

		thr->regs = &thread_regs[i];

		list_node_init(&thr->readyql, thr);
		thr->timeout = TIMEOUT_INFINITE;

		thr->suspend_wakeup = 0;
		thr->sig_mask_blocked = 0;
		thr->sig_mask_pending = 0;
		thr->fpu_state = FPU_STATE_AUTO;

		thr->irq = NULL;
		thr->futex_addr = 0;
		list_node_init(&thr->futex_waitql, thr);
		thr->event_wait = NULL;

		thr->prio = 0;
		thr->state = THREAD_STATE_DEAD;
	}
}

/** bild thread object to a partition at boot time */
__init void thread_bind_part(
	struct thread *thr,
	unsigned int id,
	const struct part_cfg *this_part_cfg)
{
	assert(thr != NULL);
	assert(this_part_cfg != NULL);

	thr->part_cfg = this_part_cfg;
	thr->sched = sched_per_cpu(this_part_cfg->first_cpu);
	thr->id = id;
}

/* perform context switch */
static struct regs *switch_threads(
	struct thread *old_thr,
	struct thread *new_thr)
{
	assert(old_thr != NULL);
	assert(new_thr != NULL);
	assert(new_thr != old_thr);
	assert(new_thr->state == THREAD_STATE_CURRENT);

	// switch additional registers
	arch_reg_save(old_thr->regs);

	if (old_thr->fpu_state == FPU_STATE_ON) {
		arch_fpu_save(old_thr->regs);
		arch_fpu_disable();
	}

	arch_reg_switch();
	percpu_thread_switch(new_thr);

	/* switch address space when partition changes */
	if (new_thr->part_cfg != old_thr->part_cfg) {
		arch_adspace_switch(&adspace_cfg[new_thr->part_cfg->id]);
	}

	arch_reg_restore(new_thr->regs);

	if (new_thr->fpu_state == FPU_STATE_ON) {
		arch_fpu_enable();
		arch_fpu_restore(new_thr->regs);
	}

	/* return to thread, send signals if any */
	return sig_send_if_pending(new_thr);
}

/* perform context switch on kernel exit if necessary
 *
 * NOTE: we keep the real switch in a dedicated function,
 * this helps to the compiler to optimize better
 */
struct regs *kernel_schedule(void)
{
	struct thread *current;
	struct thread *next;
	struct sched *sched;

	current = current_thread();
	sched = current->sched;

	if (sched->reschedule == 0) {
		/* shortcut: immediately return to thread, send signals if any */
		return sig_send_if_pending(current);
	}

	sched_preempt(sched);
	sched_readyq_next(sched);
	next = sched->current;

	if (next == current) {
		/* shortcut: immediately return to thread, send signals if any */
		return sig_send_if_pending(current);
	}

	return switch_threads(current, next);
}

/** start a thread */
void thread_start(
	struct thread *thr,
	ulong_t entry,
	ulong_t arg,
	ulong_t stack,
	ulong_t tls,
	int prio,
	unsigned int cpu_id)
{
	assert(thr != NULL);
	assert(thr->state == THREAD_STATE_DEAD);

	/* set prio + CPU + registers to given values */
	thr->prio = prio;
	thr->sched = sched_per_cpu(cpu_id);
	arch_reg_init(thr->regs, entry, stack, arg, tls);

	/* reset thread state */
	thr->suspend_wakeup = 0;
	thr->sig_mask_blocked = 0;
	thr->sig_mask_pending = 0;
	/* initialize FPU registers */
	thr->fpu_state = FPU_STATE_AUTO;
	arch_fpu_init(thr->regs);

	thread_wakeup(thr);
}

/** kill a thread in any state */
void thread_kill(struct thread *thr)
{
	struct sched *sched;

	assert(thr != NULL);

	switch (thr->state) {
	case THREAD_STATE_CURRENT:
		/* CURRENT -> DEAD */
		assert(thr == current_thread());
		thr->state = THREAD_STATE_DEAD;
		thr->timeout = TIMEOUT_INFINITE;

		/* The current thread dies:
		 * Perform a fake switch to the idle thread first
		 * to save our registers, then change the state to dead,
		 * so we can be restarted properly afterwards.
		*/
		sched = thr->sched;
		assert(sched != NULL);
		sched->current = sched->idle;
		sched->idle->state = THREAD_STATE_CURRENT;
		switch_threads(thr, sched->idle);
		break;

	case THREAD_STATE_READY:
		/* READY -> DEAD */
		sched_remove(thr->sched, thr);
		break;

	case THREAD_STATE_WAIT_SLEEP:
		/* WAIT_SLEEP -> DEAD */
		/* clear any pending timeout */
		sched_timeout_release(thr->sched, thr);
		thr->state = THREAD_STATE_DEAD;
		break;

	case THREAD_STATE_WAIT_SUSPEND:
		/* WAIT_SUSPEND -> DEAD */
		/* no timeout here, just change state */
		assert(thr->timeout == TIMEOUT_INFINITE);
		thr->state = THREAD_STATE_DEAD;
		break;

	case THREAD_STATE_WAIT_IRQ:
		/* WAIT_IRQ -> DEAD */
		/* clear any pending timeout, then release and mask IRQ */
		sched_timeout_release(thr->sched, thr);
		irq_wait_cancel(thr);
		thr->state = THREAD_STATE_DEAD;
		break;

	case THREAD_STATE_WAIT_FUTEX:
		/* WAIT_FUTEX -> DEAD */
		/* clear any pending timeout, then remove from futex queue */
		sched_timeout_release(thr->sched, thr);
		futex_wait_cancel(thr);
		thr->state = THREAD_STATE_DEAD;
		break;

	case THREAD_STATE_WAIT_EVENT:
		/* WAIT_EVENT -> DEAD */
		/* clear any pending timeout, then remove from event object*/
		sched_timeout_release(thr->sched, thr);
		event_wait_cancel(thr);
		thr->state = THREAD_STATE_DEAD;
		break;

	default:
		assert(thr->state == THREAD_STATE_DEAD);
		/* already DEAD */
		break;
	}

	assert(thr->state == THREAD_STATE_DEAD);
}

/** let current thread wait in wait state with timeout */
void thread_wait(struct thread *thr, unsigned int state, sys_time_t timeout)
{
	assert(thr != NULL);
	assert(thr == current_thread());
	assert(state > THREAD_STATE_READY);

	thr->state = state;
	thr->timeout = timeout;

	sched_wait(thr->sched, thr);
}

/** wake a thread and make it READY again */
void thread_wakeup(struct thread *thr)
{
	assert(thr != NULL);
	assert(thr->state > THREAD_STATE_READY);

	sched_wakeup(thr->sched, thr);
}

/** release the timeout of a blocked thread */
void thread_timeout_release(struct thread *thr)
{
	assert(thr != NULL);
	assert(thr->state > THREAD_STATE_READY);

	sched_timeout_release(thr->sched, thr);
}

/** do cleanup of special waiting states after timeout or thread deletion */
void thread_wait_cancel(struct thread *thr)
{
	assert(thr != NULL);

	assert(thr->state > THREAD_STATE_READY);

	if (thr->state == THREAD_STATE_WAIT_IRQ) {
		irq_wait_cancel(thr);
	} else if (thr->state == THREAD_STATE_WAIT_FUTEX) {
		futex_wait_cancel(thr);
	} else if (thr->state == THREAD_STATE_WAIT_EVENT) {
		event_wait_cancel(thr);
	}
}

/** yield current thread */
void thread_yield(struct thread *thr)
{
	assert(thr == current_thread());

	sched_yield(thr->sched, thr);
}

/** change priority of current thread */
int thread_prio_change(struct thread *thr, int new_prio)
{
	int old_prio;

	assert(thr == current_thread());
	assert(new_prio >= 0);
	assert(new_prio < NUM_PRIOS);

	old_prio = thr->prio;
	thr->prio = new_prio;

	sched_check_preempt(thr->sched);

	return old_prio;
}

/** change CPU of current thread */
void thread_cpu_migrate(struct thread *thr, unsigned int new_cpu)
{
	struct sched *sched;

	assert(thr == current_thread());
	assert(new_cpu < cpu_num);

	/* We must save all our registers before migration,
	 * otherwise we could we woken up too early on the new CPU.
	 * So we perform a fake switch to the idle thread first
	 * to save our registers, then we migrate,
	 * and finally reschedule on our CPU.
	 */
	sched = thr->sched;
	sched->current = sched->idle;
	sched->idle->state = THREAD_STATE_CURRENT;
	switch_threads(thr, sched->idle);

	sched->reschedule = 1;

	sched_migrate(sched_per_cpu(new_cpu), thr);
}

/** suspend current thread */
err_t thread_suspend(struct thread *thr)
{
	assert(thr == current_thread());

	if (thr->suspend_wakeup != 0) {
		/* clear pending suspension wakeup request */
		thr->suspend_wakeup = 0;
		return EAGAIN;
	} else {
		/* suspend thread */
		thread_wait(thr, THREAD_STATE_WAIT_SUSPEND, TIMEOUT_INFINITE);
		return EOK;
	}
}

/** resume suspended thread */
err_t thread_resume(struct thread *thr)
{
	assert(thr != NULL);

	if (thr->state == THREAD_STATE_DEAD) {
		/* thread is dead */
		return ESTATE;
	}

	if (thr->state != THREAD_STATE_WAIT_SUSPEND) {
		/* set pending suspension wakeup request */
		thr->suspend_wakeup = 1;
	} else {
		/* thread is suspended */
		thread_wakeup(thr);
	}
	return EOK;
}
