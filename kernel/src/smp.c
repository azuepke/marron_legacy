/* SPDX-License-Identifier: MIT */
/*
 * smp.c
 *
 * SMP remote calls.
 *
 * azuepke, 2018-04-10: initial
 */

#include <smp.h>
#include <kernel.h>
#include <assert.h>
#include <sched_types.h>
#include <percpu.h>
#include <arch_smp.h>


#ifdef SMP

/** Extract counter from upper 16 bits of a request */
#define SMP_CALL_COUNTER(id)	((id) >> 16)
/** Extract CPU ID from lower 16 bits of a request */
#define SMP_CALL_CPU(id)		((id) & 0x0000ffff)
/** Compose a request ID with counter in upper 16 bit and CPU ID in lower 16 bit */
#define SMP_CALL(cnt, cpu)		((cnt) << 16 | (cpu))

/** initialize SMP remote calls */
__init void smp_init(void)
{
	for (unsigned int i = 0; i < cpu_num; i++) {
		struct smp_call *smp_call = &smp_call_dyn[i];

		smp_call->handler = NULL;
		smp_call->arg = NULL;
		smp_call->request = SMP_CALL(0, 0);

		/* initially, request #0 is handled on all CPUs */
		for (unsigned int j = 0; j < SMP_MAX_CPUS; j++) {
			smp_call->handled_id[j] = SMP_CALL(0, 0);
		}
	}

	/* place a write barrier after all requests have been written */
	arch_smp_write_barrier();
}

/** submit an SMP remote call */
static uint32_t smp_call_submit(unsigned int cpu, smp_call_handler_t handler, void *arg)
{
	struct smp_call *smp_call_self;
	uint32_t request;

	assert(handler != NULL);

	/* prepare an SMP outgoing call */
	smp_call_self = &smp_call_dyn[current_cpu_id()];
	smp_call_self->handler = handler;
	smp_call_self->arg = arg;

	request = smp_call_self->request;
	request = SMP_CALL(SMP_CALL_COUNTER(request) + 1, cpu);

	/* submit */
	arch_smp_write_barrier();
	smp_call_self->request = request;
	arch_smp_write_barrier();

	return request;
}

/** wait for an SMP remote call to complete */
static bool_t smp_call_poll_complete(uint32_t request)
{
	struct smp_call *smp_call_other;
	unsigned int target_cpu;
	unsigned int our_cpu;

	target_cpu = SMP_CALL_CPU(request);
	assert(target_cpu < cpu_num);

	smp_call_other = &smp_call_dyn[target_cpu];

	our_cpu = current_cpu_id();
	assert(our_cpu != target_cpu);

	/* the target CPU sets handled_id for our CPU to the current request */
	arch_smp_read_barrier();

	return smp_call_other->handled_id[our_cpu] == request;
}

/** check for incoming SMP remote calls to the current CPU and execute them */
static void smp_call_poll_pending(void)
{
	struct smp_call *smp_call_other;
	struct smp_call *smp_call_self;
	unsigned int our_cpu;
	uint32_t request;

	our_cpu = current_cpu_id();
	smp_call_self = &smp_call_dyn[our_cpu];

	for (unsigned int cpu = 0; cpu < cpu_num; cpu++) {
		smp_call_other = &smp_call_dyn[cpu];
		assert(smp_call_other != NULL);

		request = smp_call_other->request;
		if ((SMP_CALL_CPU(request) == our_cpu) &&
		    (smp_call_self->handled_id[cpu] != request)) {

			/* call handler */
			arch_smp_read_barrier();
			assert(smp_call_other->handler != NULL);
			smp_call_other->handler(smp_call_other->arg);

			/* complete */
			arch_smp_write_barrier();
			smp_call_self->handled_id[cpu] = request;
			arch_smp_write_barrier();
		}
	}
}

/** SMP remote call of "handler" with argument "arg" on CPU "cpu" */
void smp_call(unsigned int cpu, smp_call_handler_t handler, void *arg)
{
	uint32_t request;

	assert(cpu < cpu_num);
	assert(cpu != current_cpu_id());
	assert(handler != NULL);

	/* submit an SMP outgoing call */
	request = smp_call_submit(cpu, handler, arg);

	/* trigger IPI */
	bsp_cpu_reschedule(cpu);

	/* wait for completion */
	while (!smp_call_poll_complete(request)) {
		/* we must also check for incoming requests to prevent deadlocks */
		smp_call_poll_pending();
		/* hyperthreading or QEMU: give other CPUs time to run */
		arch_smp_pause();
	}
}

/** Handle incoming SMP remote calls */
void smp_interrupt(void)
{
	smp_call_poll_pending();
}

#endif
