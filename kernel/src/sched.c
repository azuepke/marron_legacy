/* SPDX-License-Identifier: MIT */
/*
 * sched.c
 *
 * Scheduler
 *
 * azuepke, 2017-12-31: initial
 */

#include <sched.h>
#include <thread.h>
#include <bsp.h>
#include <bit.h>


/** get thread object from ready queue node */
#define THR_FROM_READYQ(t) list_entry((t), struct thread, readyql)

/** get thread object from timeout queue node */
#define THR_FROM_TIMEOUTQ(t) list_entry((t), struct thread, timeoutql)

/** bits per word in tracking bitmap */
#define TRACKING_BITMAP_BITS_PER_WORD (8*sizeof(ulong_t))

/** initialize per-CPU scheduling data, assuming only the idle thread is active */
__init void sched_init(struct sched *sched, struct thread *idle, unsigned int cpu)
{
	unsigned int i;

	assert(sched != NULL);
	assert(idle != NULL);
	assert(idle->state == THREAD_STATE_CURRENT);
	assert(idle->prio == -1);

	sched->current = idle;
	sched->idle = idle;
	sched->cpu = cpu;
	sched->reschedule = 0;

	sched->next_highest_prio = -1;

	for (i = 0; i < countof(sched->readyqh); i++) {
		list_head_init(&sched->readyqh[i]);
	}
	for (i = 0; i < countof(sched->tracking_bitmap); i++) {
		sched->tracking_bitmap[i] = 0;
	}

	list_head_init(&sched->timeoutqh);

	bsp_timer_set_expiry(TIMEOUT_INFINITE, cpu);
}

/** reprogram BSP timer when the timeout queue changes */
static void sched_reprogram_timer_expiry(struct sched *sched)
{
	struct thread *thr;
	list_t *node;

	/* reprogram timer to first timeout on the timeout queue */
	node = list_first(&sched->timeoutqh);
	if (node != NULL) {
		thr = THR_FROM_TIMEOUTQ(node);
		assert(thr->state > THREAD_STATE_READY);
		assert(thr->timeout != TIMEOUT_INFINITE);

		bsp_timer_set_expiry(thr->timeout, sched->cpu);
	}
}

/** update priority tracking bitmap after a thread was added */
static void sched_tracking_bitmap_add(struct sched *sched, int prio)
{
	int word;
	int bit;

	assert(sched != NULL);
	assert(prio >= 0);
	assert(prio < NUM_PRIOS);

	word = prio / TRACKING_BITMAP_BITS_PER_WORD;
	bit = prio % TRACKING_BITMAP_BITS_PER_WORD;
	sched->tracking_bitmap[word] |= (1UL << bit);

	if (prio > sched->next_highest_prio) {
		sched->next_highest_prio = prio;
		/* change of the next_highest priority:
		 * trigger rescheduling when we leave the kernel,
		 * this also enforces an update the shadow value in user space
		 */
		sched->reschedule = 1;
	}
}

/** update priority tracking bitmap after a thread was removed */
static void sched_tracking_bitmap_remove(struct sched *sched, int prio)
{
	int word;
	int bit;

	assert(sched != NULL);
	assert(prio >= 0);
	assert(prio < NUM_PRIOS);

	word = prio / TRACKING_BITMAP_BITS_PER_WORD;
	bit = prio % TRACKING_BITMAP_BITS_PER_WORD;
	sched->tracking_bitmap[word] &= ~(1UL << bit);

	if (prio == sched->next_highest_prio) {
		/* need to find the next-highest priority */

		while ((word >= 0) && (sched->tracking_bitmap[word] == 0)) {
			word--;
		}
		if (word < 0) {
			sched->next_highest_prio = -1;
			return;
		}
		assert(sched->tracking_bitmap[word] != 0);
		bit = __bit_ffsl(sched->tracking_bitmap[word]);
		sched->next_highest_prio = word * TRACKING_BITMAP_BITS_PER_WORD + bit;
	}
}

/** enqueue a thread at the head of its ready queue */
static void sched_readyq_enqueue_head(struct sched *sched, struct thread *thr)
{
	int prio;

	assert(sched != NULL);
	assert(thr != NULL);
	assert(thr->state == THREAD_STATE_READY);

	if (thr == sched->idle) {
		return;
	}

	prio = thr->prio;
	assert(prio >= 0);
	assert(prio < NUM_PRIOS);

	list_add_first(&sched->readyqh[prio], &thr->readyql);
	sched_tracking_bitmap_add(sched, prio);
}

/** enqueue a thread at the tail of its ready queue */
static void sched_readyq_enqueue_tail(struct sched *sched, struct thread *thr)
{
	int prio;

	assert(sched != NULL);
	assert(thr != NULL);
	assert(thr->state == THREAD_STATE_READY);

	if (thr == sched->idle) {
		return;
	}

	prio = thr->prio;
	assert(prio >= 0);
	assert(prio < NUM_PRIOS);

	list_add_last(&sched->readyqh[prio], &thr->readyql);
	sched_tracking_bitmap_add(sched, prio);
}

/** remove a thread from its ready queue */
static void sched_readyq_remove(struct sched *sched, struct thread *thr)
{
	int prio;

	assert(sched != NULL);
	assert(thr != NULL);
	assert(thr->state > THREAD_STATE_READY);
	assert(thr != sched->idle);

	prio = thr->prio;
	assert(prio >= 0);
	assert(prio < NUM_PRIOS);
	assert(prio <= sched->next_highest_prio);

	list_del(&thr->readyql);
	if (list_is_empty(&sched->readyqh[prio])) {
		sched_tracking_bitmap_remove(sched, prio);
	}
}

/** find and remove highest thread on ready queue */
void sched_readyq_next(struct sched *sched)
{
	struct thread *thr;
	list_t *node;
	int prio;

	assert(sched != NULL);
	assert(sched->current != THREAD_STATE_CURRENT);

	prio = sched->next_highest_prio;
	if (prio < 0) {
		assert(prio == -1);
		thr = sched->idle;
		goto set_next;
	}

	node = __list_remove_first(&sched->readyqh[prio]);
	assert(node != NULL);
	thr = THR_FROM_READYQ(node);

	if (list_is_empty(&sched->readyqh[prio])) {
		sched_tracking_bitmap_remove(sched, prio);
	}

set_next:
	assert(thr->state == THREAD_STATE_READY);
	thr->state = THREAD_STATE_CURRENT;
	sched->current = thr;
	sched->reschedule = 0;
}


/** preempt the current thread */
void sched_preempt(struct sched *sched)
{
	struct thread *thr;

	assert(sched != NULL);

	thr = sched->current;
	if (thr->state == THREAD_STATE_CURRENT) {
		thr->state = THREAD_STATE_READY;

		sched_readyq_enqueue_head(sched, thr);
	}
}

/** check if we need to preempt the current thread */
void sched_check_preempt(struct sched *sched)
{
	assert(sched != NULL);

	if (sched->current->prio < sched->next_highest_prio) {
		sched->reschedule = 1;
	}
}

/** current thread waits, with state, timeout and error code already set */
void sched_wait(struct sched *sched, struct thread *thr)
{
	/* The current thread is not kept on the ready queue,
	 * so we just need to enqueue it on the timeout queue.
	 * We assume that both new state and timeout value are already set.
	 */
	assert(sched != NULL);
	assert(thr != NULL);
	assert(thr != sched->idle);
	assert(thr->state > THREAD_STATE_READY);

	if (thr->timeout != TIMEOUT_INFINITE) {
		/* Thread has a timeout: enqueue in sorted timeout queue */
		list_add_sorted(&sched->timeoutqh, &thr->timeoutql,
		                THR_FROM_TIMEOUTQ(__ITER__)->timeout > thr->timeout);

		/* reprogram timer to first thread's timeout */
		sched_reprogram_timer_expiry(sched);
	}

	/* enforce scheduling */
	sched->reschedule = 1;
}

/** remove a thread from the ready queue
 * NOTE: thread_kill() calls this for threads in THREAD_STATE_READY state
 */
void sched_remove(struct sched *sched, struct thread *thr)
{
	assert(sched != NULL);
	assert(thr != NULL);
	assert(thr != sched->idle);

	assert(thr->state == THREAD_STATE_READY);
	thr->state = THREAD_STATE_DEAD;

	sched_readyq_remove(sched, thr);
}

/** release the timeout of a blocked thread
 * NOTE: thread_kill() also calls this for waiting threads
 */
void sched_timeout_release(struct sched *sched, struct thread *thr)
{
	assert(sched != NULL);
	assert(thr != NULL);
	assert(thr->state > THREAD_STATE_READY);

	if (thr->timeout != TIMEOUT_INFINITE) {
		thr->timeout = TIMEOUT_INFINITE;
		list_del(&thr->timeoutql);

		/* reprogram timer to first thread's timeout */
		sched_reprogram_timer_expiry(sched);
	}
}

/** wake a thread and schedule */
void sched_wakeup(struct sched *sched, struct thread *thr)
{
	assert(sched != NULL);
	assert(thr != NULL);
	assert(thr->state > THREAD_STATE_READY);

	sched_timeout_release(sched, thr);

	thr->state = THREAD_STATE_READY;
	sched_readyq_enqueue_tail(sched, thr);
}

/** expire all currently pending timeouts and wake waiting threads */
/* NOTE: the code is an optimized version of sched_wakeup():
 * we may wake more than one thread, but reprogram the timer only once
 * and also check for preemption only once
 */
void sched_timeout_expire(sys_time_t now, struct sched *sched)
{
	struct thread *thr;
	list_t *node;
	bool_t woken;

	/* walk sorted timeout queue and wake all threads with expired timeout */
	woken = false;
	while ((node = list_first(&sched->timeoutqh))) {
		thr = THR_FROM_TIMEOUTQ(node);
		assert(thr->state > THREAD_STATE_READY);
		assert(thr->timeout != TIMEOUT_INFINITE);

		if (thr->timeout > now) {
			if (woken) {
				/* we expired at least one thread, reprgram next timer expiry */
				bsp_timer_set_expiry(thr->timeout, sched->cpu);
			}
			break;
		}

		/* do cleanup of special waiting states */
		thread_wait_cancel(thr);

		/* wake up thread */
		thr->timeout = TIMEOUT_INFINITE;
		list_del(&thr->timeoutql);
		thr->state = THREAD_STATE_READY;

		sched_readyq_enqueue_tail(sched, thr);

		woken = true;
	}
}

/** yield current thread */
void sched_yield(struct sched *sched, struct thread *thr)
{
	assert(sched != NULL);
	assert(thr != NULL);
	assert(thr->state == THREAD_STATE_CURRENT);

	thr->state = THREAD_STATE_READY;
	sched->reschedule = 1;
	sched_readyq_enqueue_tail(sched, thr);
}

/** migrate current thread to a new CPU (and yield there) */
void sched_migrate(struct sched *sched, struct thread *thr)
{
	assert(sched != NULL);
	assert(thr != NULL);
	assert(thr->state == THREAD_STATE_CURRENT);
	//assert(thr->cpu == sched->cpu);

	thr->state = THREAD_STATE_READY;
	sched_readyq_enqueue_tail(sched, thr);

#if 0 //#ifdef SMP -- migration cannot happen, the thread just yields
	/* notify target CPU */
	bsp_cpu_reschedule(sched->cpu);
#endif
}
