/* SPDX-License-Identifier: MIT */
/*
 * event.c
 *
 * Event implementation
 *
 * azuepke, 2018-04-10: initial
 */

#include <marron/error.h>
#include <event.h>
#include <thread.h>
#include <assert.h>
#include <percpu.h>
#include <smp.h>


/** initialize all event objects at boot time */
void event_init_all(void)
{
	for (unsigned int i = 0; i < event_wait_num; i++) {
		struct event_wait *ew = &event_wait_dyn[i];

		ew->waiter = NULL;
		ew->pending = false;
	}
}

err_t event_wait(struct event_wait *event_wait, sys_timeout_t timeout)
{
	struct thread *thr;

	assert(event_wait != NULL);

	if (event_wait->pending != false) {
		/* event immediately delivered */
		event_wait->pending = false;
		return EOK;
	}

	if (event_wait->waiter != NULL) {
		/* someone else is already waiting */
		return EAGAIN;
	}

	/* register the current thread as waiter on the event */
	thr = current_thread();
	thr->event_wait = event_wait;
	event_wait->waiter = thr;

	thread_wait(thr, THREAD_STATE_WAIT_EVENT, timeout);

	/* set error code to ETIMEOUT in case the timeout triggers */
	return ETIMEOUT;
}

/** Callback for cleanup during wakeup (timeout, thread deletion) */
void event_wait_cancel(struct thread *thr)
{
	struct event_wait *event_wait;

	assert(thr != NULL);
	assert(thr->state == THREAD_STATE_WAIT_EVENT);
	event_wait = thr->event_wait;
	assert(event_wait != NULL);
	thr->event_wait = NULL;

	/* unregister thread from event object */
	assert(event_wait->waiter == thr);
	event_wait->waiter = NULL;
}

/** Wake a thread waiting on event object, if any (internal routine) */
static void event_send_internal(void *arg)
{
	struct event_wait *event_wait = arg;
	struct thread *thr;

	assert(event_wait != NULL);

	thr = event_wait->waiter;
	if (thr != NULL) {
		/* wake waiting waiter */
		event_wait->waiter = NULL;
		assert(thr->event_wait == event_wait);
		thr->event_wait = NULL;
		assert(thr->sched->cpu == current_cpu_id());

		/* wake waiting thread with EOK */
		REG_OUT0(thr->regs) = EOK;
		thread_wakeup(thr);
	} else {
		/* no waiter, just mark event as pending */
		event_wait->pending = true;
	}
}

/** Wake a thread waiting on event object, if any */
void event_send(const struct event_send_cfg *event_cfg)
{
	struct event_wait *event_wait;

	assert(event_cfg != NULL);
	event_wait = event_cfg->event_wait;
	assert(event_wait != NULL);

#ifdef SMP
	if (event_cfg->target_cpu != current_cpu_id()) {
		smp_call(event_cfg->target_cpu, event_send_internal, event_wait);
		return;
	}
#endif

	/* send event to same CPU */
	event_send_internal(event_wait);
}
