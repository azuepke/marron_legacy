/* SPDX-License-Identifier: MIT */
/*
 * futex.c
 *
 * Futex implementation
 *
 * azuepke, 2018-03-19: initial
 * azuepke, 2018-04-19: simplified implementation
 */

#include <marron/error.h>
#include <futex.h>
#include <thread.h>
#include <percpu.h>
#include <user.h>
#include <assert.h>


/** get thread object from futex_waitql node */
#define THR_FROM_WAITQ(t) list_entry((t), struct thread, futex_waitql)


/** Block current thread on user space locking object */
err_t futex_wait(
	addr_t futex_addr,
	int compare,
	sys_timeout_t timeout)
{
	struct thread *thr;
	struct part *part;
	int futex_value;
	err_t err;

	/* compare futex value */
	err = user_copy_in(&futex_value, (void*)futex_addr, sizeof(futex_value));
	if (err != EOK) {
		return err;
	}
	if (futex_value != compare) {
		return EAGAIN;
	}

	/* enqueue the current thread on the futex wait queue in priority order */
	thr = current_thread();
	thr->futex_addr = futex_addr;

	part = thr->part_cfg->part;
	list_add_sorted(&part->futex_waitqh, &thr->futex_waitql,
	                THR_FROM_WAITQ(__ITER__)->prio < thr->prio);

	thread_wait(thr, THREAD_STATE_WAIT_FUTEX, timeout);

	/* set error code to ETIMEOUT in case the timeout triggers */
	return ETIMEOUT;
}

/** Callback for cleanup during wakeup (timeout, thread deletion)
 * NOTE: this is called when the timeout expires or the thread is deleted
 */
void futex_wait_cancel(
	struct thread *thr)
{
	assert(thr != NULL);
	assert(thr->state == THREAD_STATE_WAIT_FUTEX);

	/* remove thread from wait queue */
	list_del(&thr->futex_waitql);
}

/** Wake threads waiting on user space locking object */
err_t futex_wake(
	addr_t futex_addr,
	unsigned int count)
{
	struct thread *thr;
	struct part *part;
	list_t *node, *tmp;

	part = current_part_cfg()->part;

	if (count > 0) {
		list_for_each_safe(&part->futex_waitqh, node, tmp) {
			thr = THR_FROM_WAITQ(node);
			assert(thr != NULL);
			assert(thr->state == THREAD_STATE_WAIT_FUTEX);

			if (thr->futex_addr == futex_addr) {
				/* remove thread from wait queue */
				list_del(&thr->futex_waitql);

				/* wake waiting thread with EOK */
				REG_OUT0(thr->regs) = EOK;
				thread_wakeup(thr);

				count--;
				if (count == 0) {
					break;
				}
			}
		}
	}

	return EOK;
}

/** Wake and/or requeue threads waiting on user space locking object */
err_t futex_requeue(
	addr_t futex_addr,
	unsigned int count,
	int compare,
	addr_t futex2_addr,
	unsigned int count2)
{
	struct thread *thr;
	struct part *part;
	int futex_value;
	list_t *node, *tmp;
	list_t tmp_head;
	err_t err;

	/* compare futex value */
	err = user_copy_in(&futex_value, (void*)futex_addr, sizeof(futex_value));
	if (err != EOK) {
		return err;
	}
	if (futex_value != compare) {
		return EAGAIN;
	}

	part = current_part_cfg()->part;

	/* wake up count threads */
	if (count > 0) {
		list_for_each_safe(&part->futex_waitqh, node, tmp) {
			thr = THR_FROM_WAITQ(node);
			assert(thr != NULL);
			assert(thr->state == THREAD_STATE_WAIT_FUTEX);

			if (thr->futex_addr == futex_addr) {
				/* remove thread from wait queue */
				list_del(&thr->futex_waitql);

				/* wake waiting thread with EOK */
				REG_OUT0(thr->regs) = EOK;
				thread_wakeup(thr);

				count--;
				if (count == 0) {
					break;
				}
			}
		}
	}

	/* requeue count2 threads */
	if (count2 > 0) {
		/* walk wait queue again and remember threads to requeue in tmp_head */
		list_head_init(&tmp_head);

		list_for_each_safe(&part->futex_waitqh, node, tmp) {
			thr = THR_FROM_WAITQ(node);
			assert(thr != NULL);
			assert(thr->state == THREAD_STATE_WAIT_FUTEX);

			if (thr->futex_addr == futex_addr) {
				/* move thread from wait queue to tmp queue */
				list_del(&thr->futex_waitql);
				list_add_last(&tmp_head, &thr->futex_waitql);

				count2--;
				if (count2 == 0) {
					break;
				}
			}
		}

		list_for_each_remove_first(&tmp_head, node) {
			thr = THR_FROM_WAITQ(node);
			assert(thr != NULL);
			assert(thr->state == THREAD_STATE_WAIT_FUTEX);

			/* release any timeout, but let thread continue to wait */
			thread_timeout_release(thr);

			thr->futex_addr = futex2_addr;

			list_add_sorted(&part->futex_waitqh, &thr->futex_waitql,
			                THR_FROM_WAITQ(__ITER__)->prio < thr->prio);
		}
	}

	return EOK;
}
