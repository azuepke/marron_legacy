/* SPDX-License-Identifier: MIT */
/*
 * sig.c
 *
 * Signal and exception handling
 *
 * azuepke, 2018-03-16: initial
 */

#include <marron/regs.h>
#include <marron/error.h>
#include <sig.h>
#include <kernel.h>
#include <assert.h>
#include <stddef.h>
#include <percpu.h>
#include <arch_context.h>
#include <bit.h>
#include <user.h>


/** register a signal handler */
void sig_register(struct part *part, unsigned int sig, addr_t handler, uint32_t sig_mask)
{
	assert(part != NULL);
	assert(sig < NUM_SIGS);

	part->sig_handler[sig] = handler;
	part->sig_mask[sig] = sig_mask;
}

/** send a signal or exception to the current thread */
static void sig_send_now(
	struct thread *thr,
	unsigned int sig)
{
	struct regs *regs;
	struct part *part;
	addr_t user_frame;
	uint32_t sig_mask;
	addr_t handler;
	err_t err;

	assert(thr != NULL);
	assert(thr == current_thread());
	assert((sig != 0) && (sig < NUM_SIGS));

	part = current_part_cfg()->part;
	handler = part->sig_handler[sig];

	if (handler == 0) {
		/* no registered handler */
		goto escalate;
	}

	sig_mask = current_thread()->sig_mask_blocked;

	if (sig_mask & SIG_TO_MASK(sig)) {
		/* exception is currently blocked */
		goto escalate;
	}

	/* update exception info and save FPU registers */
	regs = thr->regs;
	if (thr->fpu_state == FPU_STATE_ON) {
		regs->info |= EX_FPU_ENABLED;
		arch_fpu_save(regs);
	}

	/* setup register frame */
	user_frame = arch_signal_alloc_frame(regs);

	arch_signal_prepare(regs);

	err = user_copy_out((void*)user_frame, regs, sizeof(*regs));
	if (err != EOK) {
		/* setting up the signal failed */
		goto escalate;
	}

	arch_signal_init(regs, handler, user_frame, sig_mask, sig);

	if (thr->fpu_state == FPU_STATE_ON) {
		/* disable FPU for signal handler */
		thr->fpu_state = FPU_STATE_AUTO;
		arch_fpu_init(regs);
		arch_fpu_disable();
	}

	/* block current signal while executing in handler */
	sig_mask |= SIG_TO_MASK(sig);
	/* additionally block signals registered next to the handler */
	sig_mask |= part->sig_mask[sig];
	thr->sig_mask_blocked = sig_mask;
	return;

escalate:
	/* escalate error to partition error */
	part_error(part, sig);
}


/** immediately raise an exception to the current thread
 * NOTE: the architecture layer and sys_abort() calls this,
 * and exceptions are never set to "pending"
 */
void sig_exception(
	struct thread *thr,
	unsigned int sig)
{
	assert(thr == current_thread());
	assert((sig != 0) && (sig < NUM_SIGS));
	assert(SIG_TO_MASK(sig) & SIG_MASK_EXCEPTION);

	sig_send_now(thr, sig);
}

/** if the thread has pending signals, send them now
 * NOTE: the scheduler calls this function at kernel exit
 */
struct regs *sig_send_if_pending(
	struct thread *thr)
{
	uint32_t blocked_mask;
	uint32_t pending_mask;
	uint32_t sig_mask;
	unsigned int sig;

	assert(thr == current_thread());

	/* check for pending signals */
	blocked_mask = thr->sig_mask_blocked;
	pending_mask = thr->sig_mask_pending;

	sig_mask = pending_mask & ~blocked_mask;
	if (sig_mask == 0) {
		return thr->regs;
	}

	/*
	 * We send all signals which are pending, but not masked (yet).
	 * The loop terminates when all these signals have been delivered
	 * to user space, which means at most NUM_SIGS iterations.
	 * Note that the user can specify additional signals which become masked
	 * while a signal handler is active.
	 */
	do {
		/* pending signal, the ones with lowest bits have higher priority */
		sig = __bit_fls(sig_mask);
		assert(!(SIG_TO_MASK(sig) & SIG_MASK_EXCEPTION));

		/* clear the pending signal */
		thr->sig_mask_pending &= ~SIG_TO_MASK(sig);

		/* clear exception information when sending a software signal */
		thr->regs->info = EX_TYPE_NONE;

		/* send this signal */
		sig_send_now(thr, sig);

		/* check for other pending signals */
		blocked_mask = thr->sig_mask_blocked;
		pending_mask = thr->sig_mask_pending;

		sig_mask = pending_mask & ~blocked_mask;
	} while (sig_mask != 0);

	return thr->regs;
}

/** return from a signal handler */
void sig_return(
	struct thread *thr,
	addr_t user_frame,
	uint32_t sig_mask)
{
	struct regs *regs;
	err_t err;

	assert(thr == current_thread());

	regs = thr->regs;
	err = user_copy_in(regs, (void*)user_frame, sizeof(*regs));

	/* fixup registers in any case */
	arch_signal_fixup(regs);

	/* restore FPU registers */
	if (thr->fpu_state != FPU_STATE_OFF) {
		/* FPU must be ON or AUTO */
		if (regs->info & EX_FPU_ENABLED) {
			thr->fpu_state = FPU_STATE_ON;
			arch_fpu_enable();
			arch_fpu_restore(regs);
		} else {
			thr->fpu_state = FPU_STATE_AUTO;
			arch_fpu_disable();
		}
	}

	if (err != EOK) {
		/* restore failed, escalate error to partition error */
		part_error(current_part_cfg()->part, SIG_PAGEFAULT);
		return;
	}

	/* restore signal mask */
	thr->sig_mask_blocked = sig_mask;
}

/** change the current thread's signal mask */
uint32_t sig_mask(
	struct thread *thr,
	uint32_t clear_mask,
	uint32_t set_mask)
{
	uint32_t previous_mask;
	uint32_t sig_mask;

	assert(thr == current_thread());

	previous_mask = thr->sig_mask_blocked;

	sig_mask = thr->sig_mask_blocked;
	sig_mask &= ~clear_mask;
	sig_mask |= set_mask;
	thr->sig_mask_blocked = sig_mask;

	return previous_mask;
}

/** send an asynchronous signal to a thread */
void sig_send(struct thread *thr, unsigned int sig)
{
	assert(thr != NULL);
	assert((sig != 0) && (sig < NUM_SIGS));

	thr->sig_mask_pending |= SIG_TO_MASK(sig);

#if 0 //#ifdef SMP -- the target thread is always in the current partition
	if ((thr != current_thread()) && (thr->state == THREAD_STATE_CURRENT)) {
		/* target thread is current on other CPU, send IPI */
		bsp_cpu_reschedule(thr->sched->cpu);
	}
#endif
}
