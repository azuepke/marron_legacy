# SPDX-License-Identifier: MIT
#
# Top level Makefile
#
# azuepke, 2013-04-06: split from Kernel/Makefile
# azuepke, 2013-08-08: now in 64-bit
# azuepke, 2014-08-07: moved libs to top level Makefile
# azuepke, 2017-10-03: imported and adapted

TOP := .

include $(TOP)/rules.mk

ifeq ("$(ARCH)", "arm")
# prefer local builds to system ones
ifeq ("$(BSP)", "qemu-aarch64")
	QCMD = $(shell which ~/.qemu/bin/qemu-system-aarch64 || \
	               echo qemu-system-aarch64)
	ELF_ARCH = aarch64
else
	QCMD = $(shell which ~/.qemu/bin/qemu-system-arm || \
	               echo qemu-system-arm)
	ELF_ARCH = arm
endif
	# QEMU standard arguments
	ifeq ("$(BSP)", "qemu-arm")
		# Versatile Express Cortex A15 with four UARTs
		QARGS = -M vexpress-a15 -m 64 -nographic -no-reboot -net none -semihosting \
			-serial mon:stdio \
			-serial telnet:localhost:1235,server,nowait \
			-serial telnet:localhost:1236,server,nowait \
			-serial telnet:localhost:1237,server,nowait
		ifeq ("$(SMP)", "yes")
			QARGS += -smp 2
		endif
		ELF_LOADADDR = 0x80008000
	endif
	ifeq ("$(BSP)", "am335x")
		ELF_LOADADDR = 0x80008000
	endif
	ifeq ("$(BSP)", "am57xx")
		ELF_LOADADDR = 0x80008000
	endif
	ifeq ("$(BSP)", "imx6")
		# Sabrelite on second UART
		QARGS = -M sabrelite -m 64 -nographic -no-reboot -net none -semihosting \
			-serial telnet:localhost:1235,server,nowait \
			-serial mon:stdio \
			-serial telnet:localhost:1236,server,nowait \
			-serial telnet:localhost:1237,server,nowait
		ifeq ("$(SMP)", "yes")
			QARGS += -smp 4
		endif
		ELF_LOADADDR = 0x10008000
	endif
	ifeq ("$(BSP)", "bcm2836")
		QARGS = -M raspi2 -m 1024 -nographic -no-reboot -net none -semihosting \
			-serial mon:stdio -smp 4
		ELF_LOADADDR = 0x00008000
	endif
	ifeq ("$(BSP)", "bcm2711")
		ELF_LOADADDR = 0x00008000
	endif
endif
ifneq ("$(AUTOBUILD)", "true")
	XTERM=xterm -geom 80x45 -hold -e
endif
ALL = bootfile.elf

# check mkimage is install on system
MKIMAGE:=$(notdir $(shell which mkimage))
ifeq ("$(MKIMAGE)", "mkimage")
# Add boofile.img to all
	ALL += bootfile.img
endif


#APPS CONFIG and LIBS is spetify by BUILDENVs
DUMMY_APPS = $(addprefix dummy_,$(APPS))
FINAL_APPS = $(addprefix final_,$(APPS))
subdirs = $(LIBS) kernel
alldirs = $(subdirs) bsp/$(BSP) $(APPS)

all: $(ALL)

run: all
	$(XTERM) $(QCMD) $(QARGS) -kernel bootfile.elf -s

debug: all
	# Debug session with QEMU
	$(XTERM) $(QCMD) $(QARGS) -kernel bootfile.elf -s -S

bootfile.elf: bootfile.bin tools
	$(Q)./mkelf $< $(ELF_ARCH) $(ELF_LOADADDR) $@

# binary
bootfile.bin: final_bsp
	@echo "  IMAGE $@"
	$(Q)$(TOP)/scripts/gen_image.py -hw $(TOP)/hardware_$(BSP).xml -c $(TOP)/$(CONFIG) $@
# compress bin file
bootfile.bin.gz: bootfile.bin
	cp $^ tmp_$^
	gzip -f -9 tmp_$^
	mv tmp_$@ $@
# create u-boot img
bootfile.img: bootfile.bin.gz
	$(MKIMAGE) -A arm -O linux -T kernel -a $(ELF_LOADADDR) -e $(ELF_LOADADDR) -n Marron -d '$(basename $@).bin.gz' '$(basename $@).img'

# kernel config
dummy_config:
	@echo "  GEN   $@"
	$(Q)$(TOP)/scripts/gen_config.py -hw $(TOP)/hardware_$(BSP).xml -c $(TOP)/$(CONFIG) kernel/dummy_config.c
	$(MAKE) -C kernel dummy_config.o

final_config: $(FINAL_APPS)
	@echo "  GEN   $@"
	$(Q)$(TOP)/scripts/gen_config.py -hw $(TOP)/hardware_$(BSP).xml -c $(TOP)/$(CONFIG) --final kernel/final_config.c
	$(MAKE) -C kernel final_config.o

# generate headers
dummy_headers:
	$(Q)$(TOP)/scripts/gen_header.py -hw $(TOP)/hardware_$(BSP).xml -c $(TOP)/$(CONFIG) --dummy --config --all

final_headers: dummy_bsp $(DUMMY_APPS)
	$(Q)$(TOP)/scripts/gen_header.py -hw $(TOP)/hardware_$(BSP).xml -c $(TOP)/$(CONFIG) --final --all

# bsp
dummy_bsp: kernel dummy_config dummy_headers
	$(MAKE) -C bsp/$(BSP) dummy

final_bsp: kernel final_config final_headers
	$(MAKE) -C bsp/$(BSP) final

$(subdirs):
	$(MAKE) -C $@ all

dummy_%: dummy_headers $(LIBS)
	$(MAKE) -C $(patsubst dummy_%,%,$@) dummy

final_%: final_headers $(LIBS)
	$(MAKE) -C $(patsubst final_%,%,$@) final

# tools
tools: mkelf

HOST_CFLAGS := -W -Wall -Wextra -Werror

mkelf: tools/mkelf/mkelf.c
	@echo "  CCHOST $<"
	$(Q)$(HOSTCC) $(HOST_CFLAGS) -o $@ $<

# cleanup
thisclean:
	$(Q)rm -f bootfile.bin bootfile.bin.gz bootfile.img $(ALL)
	$(Q)rm -rf scripts/__pycache__

# build loop;)
clean-dirs = $(addprefix _clean_, $(alldirs))
distclean-dirs = $(addprefix _distclean_, $(alldirs))

$(clean-dirs):
	-make -C $(patsubst _clean_%,%,$@) clean

$(distclean-dirs):
	-make -C $(patsubst _distclean_%,%,$@) distclean

clean: thisclean $(clean-dirs)

distclean: thisclean $(distclean-dirs)
	$(Q)rm -f mkelf

zap: distclean
	# no extra steps right now

print-%  : ; @echo $* = $($*)

.PHONY: all clean distclean $(subdirs) $(clean-dirs) $(distclean-dirs)
