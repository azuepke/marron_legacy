/* SPDX-License-Identifier: MIT */
/*
 * mpcore.h
 *
 * ARM Cortex A9/A15 MPCore specific IRQ handling
 *
 * azuepke, 2013-09-11: initial ARM port
 * azuepke, 2013-11-19: refactored from board.h
 * azuepke, 2013-11-20: split into IRQ and timer code
 * azuepke, 2020-11-23: cleanup
 */

#ifndef __MPCORE_H__
#define __MPCORE_H__

/* special IRQ IDs */
#define IRQ_ID_STOP			0
#define IRQ_ID_RESCHED		1

#if defined(ARM_CORTEX_A9) || defined(ARM_CORTEX_A5)
#define IRQ_ID_GTIMER		27	/* global timer */
#define IRQ_ID_LEGACY_FIQ	28
#define IRQ_ID_PTIMER		29	/* private timer */
#define IRQ_ID_WATCHDOG		30	/* watchdog timer */
#define IRQ_ID_LEGACY_IRQ	31
#else /* A15/A5/GIC-400 or later */
#define IRQ_ID_VIRT			25	/* virtual maintenance */
#define IRQ_ID_HTIMER		26	/* hypervisor timer */
#define IRQ_ID_VTIMER		27	/* virtual timer */
#define IRQ_ID_LEGACY_FIQ	28
#define IRQ_ID_PTIMER		29	/* physical timer */
#define IRQ_ID_NS_TIMER		30	/* non-secure physical timer */
#define IRQ_ID_LEGACY_IRQ	31
#endif

#define IRQ_ID_FIRST_SPI	32

void mpcore_send_stop(void);
void mpcore_irq_init(void);
void mpcore_gic_enable(void);
unsigned int mpcore_init_smp(void);

/* real number of interrupts */
extern unsigned int mpcore_num_irqs;

#endif
