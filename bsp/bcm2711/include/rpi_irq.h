/* SPDX-License-Identifier: MIT */
/*
 * rpi_irq.h
 *
 * Raspberry Pi BCM2711 IRQ handling
 *
 * azuepke, 2020-11-05: initial
 */

#ifndef __RPI_IRQ_H__
#define __RPI_IRQ_H__

/* secondary interrupt controller (BCM2835) */
#define IRQ_ID_GPU_HALT0	68
#define IRQ_ID_GPU_HALT1	69
#define IRQ_ID_ACCESS_ERROR_TYPE_MINUS1		70	/* address error */
#define IRQ_ID_ACCESS_ERROR_TYPE_MINUS0		71	/* AXI error */

void bcm_irq_init(void);

#endif
