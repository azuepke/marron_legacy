/* SPDX-License-Identifier: MIT */
/*
 * 8250_uart.c
 *
 * 16550-like UART driver
 *
 * azuepke, 2013-03-24: initial
 * azuepke, 2020-11-05: BSP for Raspberry Pi 4
 */

#include <bsp.h>
#include <arm_io.h>
#include <board.h>

/*
 * 8250/16550 UARTs on Raspberry Pi:
 *
 * UART1: 0xfe215040
 */
/* FIXME: use a board specific value here! */
#define PORT	0xfe215040
#define CLOCK	250*1000*1000

/* register accessors */
static inline uint32_t rd(unsigned long reg)
{
	return readl((volatile void *)(PORT + reg*4));
}

static inline void wr(unsigned long reg, uint32_t val)
{
	writel((volatile void *)(PORT + reg*4), val);
}

err_t bsp_putc(int c)
{
	/* poll until transmitter is empty */
	while (!(rd(5) & 0x20)) {
		return EBUSY;
	}

	wr(0, c);
	return EOK;
}

__init void serial_init(unsigned int baudrate)
{
	(void)baudrate;
#if 0
	unsigned int div;

	/* standard sequence for NS16550 UART */

	/* disable all interrupts */
	wr(1, 0);

	/* enable DLAB, set baudrate */
	div = (CLOCK + (baudrate * 16) / 2) / (baudrate * 16);
	wr(3, 0x80);
	wr(0, div & 0xff);
	wr(1, div >> 8);

	/* 8n1 mode */
	wr(3, 0x03);
	/* enable FIFO, 14 byte threshold */
	wr(2, 0xc7);

	/* clear pending status bits */
	rd(5);
#endif
}
