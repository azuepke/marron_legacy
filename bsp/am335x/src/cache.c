/* SPDX-License-Identifier: MIT */
/*
 * cache.c
 *
 * ARM cache handling
 *
 * azuepke, 2013-09-14: initial
 */

#include <marron/error.h>
#include <kernel.h>
#include <assert.h>
#include <arm_insn.h>
#include <bsp.h>
#include <arm_private.h>
#include <bsp.h>

#ifdef ARM_V7
/** clean and invalidate all dcache */
static void armv7_flush_dcache_all(void)
{
	/*
	 * based on ARM example code
	 * ARM ARM DDI 0406A, "Example code for cache maintenance operations"
	 * clobbers r0 .. r7, r9 .. r10
	 */
	__asm__ volatile (
		"	dmb								\n"
		"	mrc		p15, 1, r0, c0, c0, 1	\n"	// Read CLIDR
		"	ands	r3, r0, #0x7000000		\n"
		"	mov		r3, r3, lsr #23			\n"	// Cache level value (naturally aligned)
		"	beq		5f						\n"
		"	mov		r10, #0					\n"

		"1:									\n"
		"	add		r2, r10, r10, lsr #1	\n"	// Work out 3xcachelevel
		"	mov		r1, r0, lsr r2			\n"	// bottom 3 bits are the Cache type for this level
		"	and		r1, r1, #7				\n"	// get those 3 bits alone
		"	cmp		r1, #2					\n"
		"	blt		4f						\n"	// no cache or only instruction cache at this level
		"	mcr		p15, 2, r10, c0, c0, 0	\n"	// write the Cache Size selection register
		"	isb								\n"	// ISB to sync the change to the CacheSizeID reg
		"	mrc		p15, 1, r1, c0, c0, 0	\n"	// reads current Cache Size ID register
		"	and		r2, r1, #0x7			\n"	// extract the line length field
		"	add		r2, r2, #4				\n"	// add 4 for the line length offset (log2 16 bytes)
		"	ldr		r4, =0x3ff				\n"
		"	ands	r4, r4, r1, lsr #3		\n"	// r4 is the max number on the way size (right aligned)
		"	clz		r5, r4					\n"	// r5 is the bit position of the way size increment
		"	ldr		r7, =0x7fff				\n"
		"	ands	r7, r7, r1, lsr #13		\n"	// r7 is the max number of the index size (right aligned)

		"2:									\n"
		"	mov		r9, r4					\n"	// r9 working copy of the max way size (right aligned)

		"3:									\n"
		"	orr		r6, r10, r9, lsl r5		\n"	// factor in the way number and cache number into r6
		"	orr		r6, r6, r7, lsl r2		\n"	// factor in the index number
		"	mcr		p15, 0, r6, c7, c14, 2	\n"	// clean and invalidate by set/way
		"	subs	r9, r9, #1				\n"	// decrement the way number
		"	bge		3b						\n"
		"	subs	r7, r7, #1				\n"	// decrement the index
		"	bge		2b						\n"

		"4:									\n"
		"	add		r10, r10, #2			\n"	// increment the cache number
		"	cmp		r3, r10					\n"
		"	bgt		1b						\n"

		"5:									\n"
		"	mov		r10, #0					\n"	// select cache level 0
		"	mcr		p15, 2, r10, c0, c0, 0	\n"	// write the Cache Size selection register
		"	isb								\n"
		: : : "r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7",
		      "r9", "r10", "cc", "memory");
}
#else
/** ARMv6: flush data cache */
static inline void armv6_flush_dcache_all(void)
{
	__asm__ volatile ("mcr p15, 0, %0, c7, c14, 0" : : "r"(0) : "memory");
}
#endif

/** invalidate icache and branch prediction */
static inline void inval_icache_all(void)
{
#ifdef SMP
	/* inner shareable */
	__asm__ volatile ("mcr p15, 0, %0, c7, c1, 0" : : "r"(0) : "memory");
#else
	__asm__ volatile ("mcr p15, 0, %0, c7, c5, 0" : : "r"(0) : "memory");
#endif
}

/** ARM cache handling */
unsigned int arm_cache_op(cache_op_t mode, addr_t addr, size_t size)
{
	unsigned int err;
	addr_t end;

	assert(board.arch.cache_line_size > 0);
	end = _ALIGN_UP(addr + size, board.arch.cache_line_size);

	switch (mode) {
	case FLUSH_DCACHE_ALL:
		/* flushes current CPU's dcache to PoC */
#ifdef ARM_V7
		armv7_flush_dcache_all();
#else
		armv6_flush_dcache_all();
#endif
		err = EOK;
		break;

	case INVAL_ICACHE_ALL:
		/* invalidates current CPU's icache only */
		inval_icache_all();
		err = EOK;
		break;

	case INVAL_ICACHE_RANGE:
		/* clean dcache + inval icache (to PoU each) */

		/*
		 * The code below is in fact:
		 *		while (addr < end) {
		 *			MAGIC_CACHE_OP(addr);
		 *			addr += board.arch.cache_line_size;
		 *		}
		 *		err = EOK;
		 *	But robust against exceptions (return EFAULT then).
		 */
		__asm__ volatile(
			"	b		3f						\n"
			"1:	mcr		p15, 0, %0, c7, c10, 1	\n"
			"       dsb                                     \n"
			"       isb                                     \n"
#ifdef SMP
			/* inner shareable */
			/* FIXME Test on IMX6 */
			/*"2:	mcr		p15, 0, %0, c7, c1, 1	\n"*/
			"2:	mcr		p15, 0, %0, c7, c5, 1	\n"
			"       dsb                                     \n"
			"       isb                                     \n"
#else
			"2:	mcr		p15, 0, %0, c7, c5, 1	\n"
			"       dsb                                     \n"
			"       isb                                     \n"
#endif
			"	add		%0, %2					\n"

			"3:	cmp		%0, %1					\n"
			"	bcc		1b						\n"
			"	mov		%0, %4					\n"
			"4:									\n"
			".pushsection .text.exception,\"ax\"\n"
			"5:	mov		%0, %5					\n"
			"	b		4b						\n"
			".popsection						\n"
			EX_TABLE(1b, 5b)
			EX_TABLE(2b, 5b)
			: "=r"(err)
			: "r"(end), "r"(board.arch.cache_line_size), "0"(addr), "i"(EOK), "i"(EFAULT));
		break;

	case FLUSH_DCACHE_RANGE:
		/* flush dcache to PoC */
		__asm__ volatile(
			"	b		3f						\n"
			"1:	mcr		p15, 0, %0, c7, c14, 1	\n"
			"	add		%0, %2					\n"

			"3:	cmp		%0, %1					\n"
			"	bcc		1b						\n"
			"	mov		%0, %4					\n"
			"4:									\n"
			".pushsection .text.exception,\"ax\"\n"
			"5:	mov		%0, %5					\n"
			"	b		4b						\n"
			".popsection						\n"
			EX_TABLE(1b, 5b)
			: "=r"(err)
			: "r"(end), "r"(board.arch.cache_line_size), "0"(addr), "i"(EOK), "i"(EFAULT));
		break;

	case CLEAN_DCACHE_RANGE:
	default:
		/* clean dcache to PoC */
		__asm__ volatile(
			"	b		3f						\n"
			"1:	mcr		p15, 0, %0, c7, c10, 1	\n"
			"	add		%0, %2					\n"

			"3:	cmp		%0, %1					\n"
			"	bcc		1b						\n"
			"	mov		%0, %4					\n"
			"4:									\n"
			".pushsection .text.exception,\"ax\"\n"
			"5:	mov		%0, %5					\n"
			"	b		4b						\n"
			".popsection						\n"
			EX_TABLE(1b, 5b)
			: "=r"(err)
			: "r"(end), "r"(board.arch.cache_line_size), "0"(addr), "i"(EOK), "i"(EFAULT));
		break;
	}

#ifdef ARM_V7
	arm_dsb();
	arm_isb();
#endif
	return err;
}
