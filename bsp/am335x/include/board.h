/* SPDX-License-Identifier: MIT */
/*
 * board.h
 *
 * Board specific setting for Beaglebone Black with ARM Cortex A8.
 *
 * azuepke, 2013-09-11: initial ARM port
 * azuepke, 2013-11-19: refactored board.h
 * azuepke, 2013-12-23: Beaglebone Black port
 * azuepke, 2020-01-31: adapted to Marron
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#ifndef __ASSEMBLER__

/* start.S */
void __bsp_halt(void);

/* ti_uart.c */
void serial_init(unsigned int baud);

/* dmtimer.c */
void dmtimer_init(unsigned int freq);

/* intc.c */
void irq_init(void);

#endif

/* specific memory layout of the BBB board */
#define BOARD_RAM_PHYS		0x80000000
#define BOARD_RAM_SIZE		0x20000000	/* 512 MB */
#define BOARD_RAM_VIRT		0x80000000

/* 1st IO region, peripherals, e.g. serial, timer */
#define BOARD_IO1_PHYS		0x44e00000
#define BOARD_IO1_SIZE		0x00100000	/* 1 MB */
#define BOARD_IO1_VIRT		0x44e00000

/* 2nd IO region, L4 peripherals, DMTIMER2, UART1 */
#define BOARD_IO2_PHYS		0x48000000
#define BOARD_IO2_SIZE		0x00100000	/* 1 MB */
#define BOARD_IO2_VIRT		0x48000000

/* 3rd IO region, interrupt controller */
#define BOARD_IO3_PHYS		0x48200000
#define BOARD_IO3_SIZE		0x00100000	/* 1 MB */
#define BOARD_IO3_VIRT		0x48200000

/* region 0xfff00000 must be left out for the mapping of the vector pages! */

/* like Linux, we use a load offset of 32K relative to an 1M aligned region */
#define LOAD_ADDR	0x80008000	/* physical kernel load address */

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

/* SoC specific addresses */
/* UART0 */
#define UART_BASE			(BOARD_IO1_VIRT + 0x9000)
#define UART_CLOCK			48000000	/* 48 MHz */

/* DMTIMER0 */
#define DMTIMER_BASE		(BOARD_IO1_VIRT + 0x5000)
#define DMTIMER_IRQ			66
#define DMTIMER_CLOCK		32768	/* 32 kHz */
//FIXME: better use DMTimer2? -- 0x48040000, IRQ 68, 25 MHz(?) clock

/* INTC */
#define INTC_BASE			(BOARD_IO3_VIRT + 0x0000)

#endif
