/* SPDX-License-Identifier: MIT */
/*
 * rpi_irq.h
 *
 * Raspberry Pi BCM2836 IRQ handling
 *
 * azuepke, 2020-08-05: initial
 */

#ifndef __RPI_IRQ_H__
#define __RPI_IRQ_H__

/* primary interrupt controller (BCM2836) */
#define IRQ_ID_PTIMER		0	/* physical timer */
#define IRQ_ID_NS_TIMER		1	/* non-secure physical timer */
#define IRQ_ID_HTIMER		2	/* hypervisor timer */
#define IRQ_ID_VTIMER		3	/* virtual timer */
#define IRQ_ID_MBOX0		4
#define IRQ_ID_MBOX1		5
#define IRQ_ID_MBOX2		6
#define IRQ_ID_MBOX3		7
#define IRQ_ID_GPU			8	/* secondary interrupt controller */
#define IRQ_ID_PMU			9

/* secondary interrupt controller (BCM2835) */
#define IRQ_ID_GPU_HALT0	36
#define IRQ_ID_GPU_HALT1	37
#define IRQ_ID_ACCESS_ERROR_TYPE_MINUS1		38
#define IRQ_ID_ACCESS_ERROR_TYPE_MINUS0		39
#define IRQ_ID_PL011		51	/* GPU IRQ 57 remapped */

void bsp_send_stop(void);
void bsp_irq_init(void);

#endif
