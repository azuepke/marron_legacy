/* SPDX-License-Identifier: MIT */
/*
 * board.h
 *
 * Board specific setting for Raspberry Pi 2 Model B V1.1 with Cortex A7.
 *
 * azuepke, 2020-08-05: Raspberry Pi port
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#ifndef __ASSEMBLER__

/* start.S */
void __bsp_halt(void);

/* pl011_uart.c */
void serial_init(unsigned int baud);

#endif

/* specific memory layout of the Raspberry Pi 2 board */
#define BOARD_RAM_PHYS		0x00000000
#define BOARD_RAM_SIZE		0x40000000	/* RPi 2 has 1 GB RAM */
#define BOARD_RAM_VIRT		0x00000000

/* like Linux, we use a load offset of 32K relative to an 1M aligned region */
#define LOAD_ADDR	0x00008000	/* physical kernel load address */

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

/** BCM specific addresses and frequencies */
#define CORE_FREQ	900000000	/* 900 MHz */
#define BCM_FREQ	396000000	/* 250 MHz */

#define CPU_COUNTER_FREQ	19200000	/* 19.2 MHz */

#define GPU_IRQ_REGS	0x3f00b200	/* GPU interrupt controller */
#define ARM_IRQ_REGS	0x40000000	/* ARM interrupt router/controller */
#define WDOG_REGS		0x3f100000	/* Watchdog */

/** enforce QEMU SMP startup workaround */
#define QEMU_SMP_STARTUP 1

#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

#endif
