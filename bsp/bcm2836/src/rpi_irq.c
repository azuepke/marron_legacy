/* SPDX-License-Identifier: MIT */
/*
 * rpi_irq.c
 *
 * Raspberry Pi BCM2836 IRQ handling
 *
 * azuepke, 2020-08-05: initial
 */

#include <kernel.h>
#include <panic.h>
#include <bsp.h>
#include <assert.h>
#include <arm_io.h>
#include <arm_insn.h>
#include <board.h>
#include <stdio.h>
#include <rpi_irq.h>
#include <current.h>
#include <bit.h>
#include <cpu_timer.h>

////////////////////////////////////////////////////////////////////////////////

/* GPU interrupt controller registers at 0x7e00b000 (mapped at 0x3f00b200) */
#define GPU_IRQ_PENDING0 0x00
#define GPU_IRQ_PENDING1 0x04
#define GPU_IRQ_PENDING2 0x08
#define GPU_FIQ_CONTROL  0x0c
#define GPU_IRQ_ENABLE1  0x10
#define GPU_IRQ_ENABLE2  0x14
#define GPU_IRQ_ENABLE0  0x18
#define GPU_IRQ_DISABLE1 0x1c
#define GPU_IRQ_DISABLE2 0x20
#define GPU_IRQ_DISABLE0 0x24

/* register accessors */
static inline uint32_t gpu_irq_rd(unsigned long reg)
{
	return readl((volatile void *)(GPU_IRQ_REGS + reg));
}

static inline void gpu_irq_wr(unsigned long reg, uint32_t val)
{
	writel((volatile void *)(GPU_IRQ_REGS + reg), val);
}

////////////////////////////////////////////////////////////////////////////////

/* ARM interrupt router registers at 0x40000000 */
#define ARM_IRQ_CORE_TIMER_CONTROL			0x00
#define ARM_IRQ_CORE_TIMER_PRESCALER		0x08
#define ARM_GPU_IRQ_ROUTING					0x0c
#define ARM_PMU_IRQ_ROUTING_SET				0x10
#define ARM_PMU_IRQ_ROUTING_CLR				0x14
#define ARM_IRQ_CORE_TIMER_LO				0x1c
#define ARM_IRQ_CORE_TIMER_HI				0x20
#define ARM_IRQ_LOCAL_TIMER_IRQ_ROUTING		0x24
#define ARM_IRQ_AXI_OUTSTANDING_COUNTERS	0x2c
#define ARM_IRQ_AXI_OUTSTANDING_IRQ			0x30
#define ARM_IRQ_LOCAL_TIMER_CONTROL			0x34
#define ARM_IRQ_LOCAL_TIMER_CLR				0x38

#define ARM_IRQ_CORE_TIMER_IRQS(core)		(0x40 + 4*(core))
#define ARM_IRQ_CORE_MBOX_IRQS(core)		(0x50 + 4*(core))
#define ARM_IRQ_CORE_IRQ_SOURCE(core)		(0x60 + 4*(core))
#define ARM_IRQ_CORE_FIQ_SOURCE(core)		(0x70 + 4*(core))

#define ARM_IRQ_MBOX_SET(core, mbox)		(0x80 + 16*(core) + 4*(mbox))
#define ARM_IRQ_MBOX_GET(core, mbox)		(0xc0 + 16*(core) + 4*(mbox))
#define ARM_IRQ_MBOX_CLR(core, mbox)		(0xc0 + 16*(core) + 4*(mbox))

/* register accessors */
static inline uint32_t arm_irq_rd(unsigned long reg)
{
	return readl((volatile void *)(ARM_IRQ_REGS + reg));
}

static inline void arm_irq_wr(unsigned long reg, uint32_t val)
{
	writel((volatile void *)(ARM_IRQ_REGS + reg), val);
}

////////////////////////////////////////////////////////////////////////////////

/* we support 128 possible IRQ vectors */
#define NUM_IRQS 256

// Number of IRQs exported by the BSP
unsigned int bsp_irq_num = NUM_IRQS;

// Number of CPUs exported by the BSP
unsigned int bsp_cpu_num = 4;

static bsp_irq_handler_t irq_handlers[NUM_IRQS];

#ifdef SMP
// CPUs online
volatile ulong_t bsp_cpu_online_mask = 0;
// CPUs started scheduling
volatile int bsp_cpu_go = 0;
#endif

/* default interrupt handlers */

/* kernel private IPIs */
static void mbox0_irq_handler(unsigned int irq __unused)
{
	unsigned int cpu = __current_cpu_id();
	uint32_t bits;

	bits = arm_irq_rd(ARM_IRQ_MBOX_GET(cpu, 0));

	/* bit 0: reschedule */
	if ((bits & 0x1) != 0) {
		arm_irq_wr(ARM_IRQ_MBOX_CLR(cpu, 0), 0x1);
	}

	/* bit 1: halt */
	if ((bits & 0x2) != 0) {
		arm_irq_wr(ARM_IRQ_MBOX_CLR(cpu, 0), 0x2);
		__bsp_halt();
	}

#ifdef SMP
	kernel_ipi();
#endif
}

/* interrupts from the GPU (second BCM2835-style interrupt controller) */
static void gpu_irq_handler(unsigned int _irq __unused)
{
	uint32_t bits;
	uint32_t irq;

	bits = gpu_irq_rd(GPU_IRQ_PENDING0);
	if ((bits & ~0x300) != 0) {
		bits &= ~0x300;
		irq = 32 + __bit_fls(bits);
		goto found;
	}

	if ((bits & 0x100) != 0) {
		bits = gpu_irq_rd(GPU_IRQ_PENDING1);
		/* clear interrupts already present in PENDING0 */
		bits &= ~((1<<7) | (1<<9) | (1<<10) | (1<<18) | (1<<19));
		assert(bits != 0);
		irq = 64 + __bit_fls(bits);
		goto found;
	}

	if ((bits & 0x200) != 0) {
		bits = gpu_irq_rd(GPU_IRQ_PENDING2);
		/* clear interrupts already present in PENDING0 */
		bits &= ~((1<<21) | (1<<22) | (1<<23) | (1<<24) | (1<<25) | (1<<30));
		assert(bits != 0);
		irq = 96 + __bit_fls(bits);
		goto found;
	}

	/* spurious interrupt */
	return;

found:
	if (irq == IRQ_ID_ACCESS_ERROR_TYPE_MINUS1) {
		printf("### illegal access type -1\n");
		__bsp_halt();
	}
	if (irq == IRQ_ID_ACCESS_ERROR_TYPE_MINUS0) {
		printf("### illegal access type -0\n");
		__bsp_halt();
	}

	irq_handlers[irq](irq);
}


#ifdef SMP

/** release the hounds */
__init void bsp_cpu_start_secondary(void)
{
	unsigned int cpu;

	assert(__current_cpu_id() == 0);
	assert(bsp_cpu_online_mask == 0x0);

	/* we flush the caches before doing anything, as the first instructions
	 * of a newly started processor runs in uncached memory mode.
	 */
	for (cpu = 1; cpu < bsp_cpu_num; cpu++) {
#ifdef QEMU_SMP_STARTUP
		/* enable pass-through in the early boot code (needed on QEMU):
		 * we flush the caches before doing anything, as the first instructions
		 * of a newly started processor runs in uncached memory mode.
		 */
		__boot_release = cpu;
		arm_dmb();
		arm_clean_dcache(&__boot_release);
#endif

		/* write start address to mbox #3 of a specific CPU */
		arm_irq_wr(ARM_IRQ_MBOX_SET(cpu, 3), LOAD_ADDR);
		arm_dsb();
		arm_sev();

		/* wait until CPU is up */
		while (!(bsp_cpu_online_mask & (1u << cpu))) {
			arm_wfe();
		}
	}
}

/** callback to signal successful booting of a processor */
__init void bsp_cpu_up(unsigned int cpu __unused)
{
	assert(cpu == __current_cpu_id());

	/* clear startup mailbox */
	arm_irq_wr(ARM_IRQ_MBOX_CLR(cpu, 3), 0xffffffff);

	/* notify CPU #0 in bsp_cpu_start_secondary() */
	bsp_cpu_online_mask |= (1u << cpu);

	/* let all CPUs spin on "bsp_cpu_go" to have the timers synchronized */
	if (cpu == 0) {
		bsp_cpu_go = 1;
		arm_dmb();
		arm_sev();
	} else {
		arm_dmb();
		arm_sev();
		/* wait until we see the "go" signal */
		while (bsp_cpu_go == 0) {
			arm_wfe();
		}
	}

	/* start timer here on SMP */
	cpu_timer_start();
}

/** determine current CPU via MPIDR */
unsigned int bsp_cpu_id(void)
{
	return __current_cpu_id();
}

/** send stop request to other cores */
void __cold bsp_send_stop(void)
{
	/* send HALT request (bit 1) to all CPUs */
	arm_irq_wr(ARM_IRQ_MBOX_SET(0, 0), 0x2);
	arm_irq_wr(ARM_IRQ_MBOX_SET(1, 0), 0x2);
	arm_irq_wr(ARM_IRQ_MBOX_SET(2, 0), 0x2);
	arm_irq_wr(ARM_IRQ_MBOX_SET(3, 0), 0x2);
}

/** send rescheduling request to another processor */
void bsp_cpu_reschedule(unsigned int cpu)
{
	assert(cpu < 4);

	/* send RESCHED request (bit 0) to target CPU */
	arm_irq_wr(ARM_IRQ_MBOX_SET(cpu, 0), 0x1);
}
#endif

/** initialize the MPCore distributed interrupt controller */
__init void bsp_irq_init(void)
{
	unsigned int irq;

	/* core timer runs on external 19.2 MHz clock (ABP does not work) */
	arm_irq_wr(ARM_IRQ_CORE_TIMER_CONTROL, 0);

	/* reset core clock (this resets the prescaler) */
	arm_irq_wr(ARM_IRQ_CORE_TIMER_LO, 0);
	arm_irq_wr(ARM_IRQ_CORE_TIMER_HI, 0);

	/* set core timer prescaler for divider ratio of 1 */
	arm_irq_wr(ARM_IRQ_CORE_TIMER_PRESCALER, 0x80000000);

	/* route IRQ and FIQ of GPU to core #0 */
	arm_irq_wr(ARM_GPU_IRQ_ROUTING, (0 << 0) | (0 << 2));

	/* route each core's PMU interrupts to IRQs */
	arm_irq_wr(ARM_PMU_IRQ_ROUTING_CLR, 0xff);
	arm_irq_wr(ARM_PMU_IRQ_ROUTING_SET, 0x0f);

	/* route each core's four timer interrupt to IRQs */
	arm_irq_wr(ARM_IRQ_CORE_TIMER_IRQS(0), 0x0f);
	arm_irq_wr(ARM_IRQ_CORE_TIMER_IRQS(1), 0x0f);
	arm_irq_wr(ARM_IRQ_CORE_TIMER_IRQS(2), 0x0f);
	arm_irq_wr(ARM_IRQ_CORE_TIMER_IRQS(3), 0x0f);

	/* clear all mailboxes */
	arm_irq_wr(ARM_IRQ_MBOX_CLR(0, 0), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(0, 1), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(0, 2), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(0, 3), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(1, 0), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(1, 1), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(1, 2), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(1, 3), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(2, 0), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(2, 1), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(2, 2), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(2, 3), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(3, 0), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(3, 1), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(3, 2), 0xffffffff);
	arm_irq_wr(ARM_IRQ_MBOX_CLR(3, 3), 0xffffffff);

	/* route each core's four mailbox interrupt to IRQs */
	arm_irq_wr(ARM_IRQ_CORE_MBOX_IRQS(0), 0x0f);
	arm_irq_wr(ARM_IRQ_CORE_MBOX_IRQS(1), 0x0f);
	arm_irq_wr(ARM_IRQ_CORE_MBOX_IRQS(2), 0x0f);
	arm_irq_wr(ARM_IRQ_CORE_MBOX_IRQS(3), 0x0f);

	/* disable AXI interrupts */
	arm_irq_wr(ARM_IRQ_AXI_OUTSTANDING_IRQ, 0);

	/* disable local timer (interrupts routes to core #0) */
	arm_irq_wr(ARM_IRQ_LOCAL_TIMER_CONTROL, 0);
	arm_irq_wr(ARM_IRQ_LOCAL_TIMER_IRQ_ROUTING, 0x0);

	/* disable GPU FIQ interrupt */
	gpu_irq_wr(GPU_FIQ_CONTROL, 0x0);

	/* disable all GPU IRQs */
	gpu_irq_wr(GPU_IRQ_DISABLE0, 0x0);
	gpu_irq_wr(GPU_IRQ_DISABLE1, 0x0);
	gpu_irq_wr(GPU_IRQ_DISABLE2, 0x0);

	/* enable GPU IRQs for access errors */
	// FIXME: do not enable
	//gpu_irq_wr(GPU_IRQ_ENABLE0, (1u << 7) | (1u << 6));

	/* setup default interrupt handler */
	for (irq = 0; irq < NUM_IRQS; irq++) {
		irq_handlers[irq] = kernel_irq;
	}

	/* register special interrupt handlers */
	irq_handlers[IRQ_ID_MBOX0] = mbox0_irq_handler;
	irq_handlers[IRQ_ID_GPU] = gpu_irq_handler;
}

/** check if the user may attach to an interrupt:
 * returns:
 *  1 -> OK
 *  0 -> not available or not suitable for sharing
 * -1 -> out of range
 */
// Configure interrupt source
// this call returns zero if the interrupt source was configured to the
// requested mode, or non-zero if not.
// the mode "IRQ_AUTO" always succeeds if the interrupt source is available.
// the call also fails if the interrupt is not available
err_t bsp_irq_config(unsigned int irq __unused, irq_mode_t mode __unused)
{
	assert(irq < NUM_IRQS);

	return EBUSY;	/* not available */
}

void bsp_irq_register(unsigned int irq, bsp_irq_handler_t handler)
{
	assert(irq < NUM_IRQS);

	irq_handlers[irq] = handler;
}

/** mask IRQ in distributor */
void bsp_irq_disable(unsigned int irq)
{
	unsigned int bit = irq & 0x1f;

	assert(irq < NUM_IRQS);

	if (irq < 32) {
		/* ARM_IRQ interrupts are hardcoded */
		return;
	}

	/* GPU interrupts */
	if ((irq >= 32) && (irq < 32 + 8)) {
		gpu_irq_wr(GPU_IRQ_DISABLE0, 1u << bit);
	} else if ((irq >= 64) && (irq < 64 + 32)) {
		gpu_irq_wr(GPU_IRQ_DISABLE1, 1u << bit);
	} else if ((irq >= 96) && (irq < 96 + 32)) {
		gpu_irq_wr(GPU_IRQ_DISABLE2, 1u << bit);
	}
}

/** unmask IRQ in distributor */
void bsp_irq_enable(unsigned int irq, unsigned int cpu_id __unused)
{
	unsigned int bit = irq & 0x1f;

	assert(irq < NUM_IRQS);

	if (irq < 32) {
		/* ARM_IRQ interrupts are hardcoded */
		return;
	}

	/* GPU interrupts */
	if ((irq >= 32) && (irq < 32 + 8)) {
		gpu_irq_wr(GPU_IRQ_ENABLE0, 1u << bit);
	} else if ((irq >= 64) && (irq < 64 + 32)) {
		gpu_irq_wr(GPU_IRQ_ENABLE1, 1u << bit);
	} else if ((irq >= 96) && (irq < 96 + 32)) {
		gpu_irq_wr(GPU_IRQ_ENABLE2, 1u << bit);
	}
}

/** dispatch IRQ: mask and ack, call handler */
// Dispatch interrupt (called from architecture layer)
// Note that "vector" has an architecture specific meaning
void bsp_irq_dispatch(ulong_t vector __unused)
{
	unsigned int cpu = __current_cpu_id();
	uint32_t pending;
	uint32_t irq;

	pending = arm_irq_rd(ARM_IRQ_CORE_IRQ_SOURCE(cpu));
	if (pending != 0) {
		irq = __bit_fls(pending);
		irq_handlers[irq](irq);
	}
}
