/* SPDX-License-Identifier: MIT */
/*
 * board.c
 *
 * Board initialization for Raspberry Pi 2 Model B V1.1 (BCM2836)
 *
 * azuepke, 2020-08-05: cloned from QEMU BSP
 */

#include <kernel.h>
#include <bsp.h>
#include <stdio.h>
#include <board.h>
#include <rpi_irq.h>
#include <assert.h>
#include <cpu_timer.h>
#include <arm_insn.h>
#include <arm_io.h>
#include <current.h>


/** BSP name string */
const char bsp_name[] = "bcm2836";

// initialize BSP
__init void bsp_init(void)
{
	unsigned int cpu_id;

#ifdef SMP
	cpu_id = __current_cpu_id();
#else
	cpu_id = 0;
#endif

	/* do low-level init only on first CPU */
	if (cpu_id == 0) {
		serial_init(115200);
		printf("Starting up ...\n");

		bsp_irq_init();
		cpu_timer_init(100);	/* HZ */
#ifndef SMP
		/* start timer here on UP */
		cpu_timer_start();
#endif
	}
#ifdef SMP
	else {
		/* empty */
	}
#endif
}

// idle the CPU
void bsp_idle(void)
{
	__asm__ volatile ("dsb; wfi" : : : "memory");
}

// Cache management
err_t bsp_cache(
	cache_op_t op,
	addr_t start,
	size_t size,
	addr_t alias)
{
	(void)op;
	(void)start;
	(void)size;
	(void)alias;
	return ENOSYS;
}


/* watchdog registers */
#define WDOG_RSTC	0x1c
#define WDOG_RSTS	0x20
#define WDOG_WDOG	0x24

/* register accessors */
static inline uint32_t wdog_rd(unsigned long reg)
{
	return readl((volatile void *)(WDOG_REGS + reg));
}

static inline void wdog_wr(unsigned long reg, uint32_t val)
{
	writel((volatile void *)(WDOG_REGS + reg), val);
}

static __cold void bsp_reset(void)
{
	uint32_t rstc;

	rstc = wdog_rd(WDOG_RSTC);
	rstc &= ~0x30;
	rstc |= 0x20;

	wdog_wr(WDOG_WDOG, 0x5a00000d);
	wdog_wr(WDOG_RSTC, 0x5a000000 | rstc);
}

// halt the board (shutdown of the system)
__cold void bsp_halt(
	halt_mode_t mode __unused)
{
#ifdef SMP
	if (mode != BOARD_STOP) {
		bsp_send_stop();
	}
#endif

	if (mode == BOARD_RESET) {
		bsp_reset();
	}

	__bsp_halt();
	unreachable();
}
