/* SPDX-License-Identifier: MIT */
/*
 * board.h
 *
 * Board specific setting for NXP IMX6 Cortex A9.
 *
 * awerner, 2020-01-30: imported from am57xx and ported to imx6
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#ifndef __ASSEMBLER__

/* start.S */
void __bsp_halt(void);

/* pl011_uart.c */
void serial_init(unsigned int baud);

#endif

/* specific memory layout of the QEMU Cortex A15 board */
#define BOARD_RAM_PHYS		0x10000000
#define BOARD_RAM_SIZE		0x20000000	/* 512MB TODO Begaboard and Phytec Board has 2 GB RAM */
#define BOARD_RAM_VIRT		0x10000000

/* like Linux, we use a load offset of 32K relative to an 1M aligned region */
#define LOAD_ADDR	0x10008000	/* physical kernel load address */

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

/** MPCore specific addresses */
#define GIC_DIST_BASE			0x00a01000
#define GIC_PERCPU_BASE			0x00a00100
#define MPCORE_SCU_BASE			0x00a00000
#define MPCORE_GTIMER_BASE		0x00a00200
#define MPCORE_PTIMER_BASE		0x00a00600

#define MPCORE_TIMER_CLOCK		396000000	/* 396 MHz */

#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

#endif
