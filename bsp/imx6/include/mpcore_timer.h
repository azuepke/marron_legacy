/* SPDX-License-Identifier: MIT */
/*
 * mpcore_timer.h
 *
 * ARM Cortex A9 MPCore specific IRQ handling
 *
 * azuepke, 2013-09-11: initial ARM port
 * azuepke, 2013-11-19: refactored from board.h
 * azuepke, 2013-11-20: split into IRQ and timer code
 */

#ifndef __MPCORE_TIMER_H__
#define __MPCORE_TIMER_H__

void mpcore_timer_init(unsigned int freq);
void mpcore_timer_start(void);

#endif
