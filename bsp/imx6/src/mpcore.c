/* SPDX-License-Identifier: MIT */
/*
 * mpcore.c
 *
 * ARM Cortex A9/A15 MPCore specific IRQ handling
 *
 * azuepke, 2013-09-18: initial
 * azuepke, 2013-11-20: split into IRQ and timer code
 * azuepke, 2020-11-23: cleanup
 */

#include <kernel.h>
#include <panic.h>
#include <bsp.h>
#include <assert.h>
#include <arm_io.h>
#include <arm_insn.h>
#include <board.h>
#include <stdio.h>
#include <mpcore.h>

/* we support 256 possible IRQ vectors (even if GIC supports up to 1024) */
#define NUM_IRQS 256

/* real number of interrupts */
unsigned int mpcore_num_irqs;

// Number of IRQs exported by the BSP
unsigned int bsp_irq_num = NUM_IRQS;

// Number of CPUs exported by the BSP
unsigned int bsp_cpu_num;

static bsp_irq_handler_t irq_handlers[NUM_IRQS];

/* default interrupt handlers */

static void resched_irq_handler(unsigned int irq __unused)
{
#ifdef SMP
	kernel_ipi();
#endif
}

static __cold void stop_irq_handler(unsigned int irq __unused)
{
	__bsp_halt();
}


/*
 * MPCore GIC and distributor registers.
 *
 * Based on the following TRMs (Technical Reference Manual) by ARM:
 *  DDI0416B -- PrimeCell Generic Interrupt Controller (PL390), Revision: r0p0
 *  IHI0048A -- ARM Generic Interrupt Controller, Architecture version 1.0
 *  DDI0388I -- Cortex-A9, Revision: r4p1
 *  DDI0407I -- Cortex-A9 MPCore, Revision: r4p1
 *  DDI0471B -- GIC-400, Revision: r0p1
 *
 * Interrupt types:
 *  SGI: software generated interrupts, 0..15
 *  PPI: private peripheral interrupt, 16..31
 *  SPI: shared peripheral interrupt, 32..1023 (max, in multiples of 32)
 */


/* registers in per-CPU area, relative to MPCORE_BASE + 0x0100 (Cortex-A9)
 *                                     or MPCORE_BASE + 0x2000 (all others) */
#define GIC_CTRL		0x000
#define GIC_PRIO		0x004
#define GIC_BINPOINT	0x008
#define GIC_ACK			0x00c
#define GIC_EOI			0x010
#define GIC_PENDING		0x018

#define VALID_IRQ_MASK	0x3ff

/* registers in the distributor area, relative to MPCORE_BASE + 0x1000 */
#define DIST_CTRL		0x000	/* global control register */
#define DIST_TYPE		0x004	/* read-only ID register */

#define DIST_ENABLE		0x100	/* write 1 to enable an interrupt */
#define DIST_DISABLE	0x180	/* write 1 to disable an interrupt */

#define DIST_PENDING	0x280	/* write 1 to clear an interrupt */

#define DIST_PRIO		0x400	/* per interrupt priority, byte accessible */
#define DIST_TARGET		0x800	/* per interrupt target, byte accessible */

#define DIST_IRQ_CONFIG	0xc00	/* per interrupt config */
	/* 2 bits per IRQ:
	 *  0x  level
	 *  1x  edge
	 *  x0  N:N model: all processors handle the interrupt
	 *  x1  1:N model: only one processor handles the interrupt
	 */

#define DIST_IPI		0xf00	/* send IPI register */
	#define IPI_ALL_BUT_SELF	0x01000000
	#define IPI_SELF_ONLY		0x02000000
	/* bits 23..16: target CPUs */
	/* bits 3..0: interrupt ID */


/* MPCore specific register accessors */

/* access to per-CPU specific registers */
static inline uint32_t gic_read32(unsigned long reg)
{
	return readl((volatile void *)(GIC_PERCPU_BASE + reg));
}

static inline void gic_write32(unsigned long reg, uint32_t val)
{
	writel((volatile void *)(GIC_PERCPU_BASE + reg), val);
}

/* access to distributor registers */
static inline uint32_t dist_read32(unsigned long reg)
{
	return readl((volatile void *)(GIC_DIST_BASE + reg));
}

static inline void dist_write32(unsigned long reg, uint32_t val)
{
	writel((volatile void *)(GIC_DIST_BASE + reg), val);
}

static inline uint8_t dist_read8(unsigned long reg)
{
	return readb((volatile void *)(GIC_DIST_BASE + reg));
}

static inline void dist_write8(unsigned long reg, uint8_t val)
{
	writeb((volatile void *)(GIC_DIST_BASE + reg), val);
}



#ifdef SMP
/** determine current CPU via APIC ID */
unsigned int bsp_cpu_id(void)
{
	return arm_get_mpidr() & 0xf;
}

/** send stop request to other cores */
void __cold mpcore_send_stop(void)
{
	dist_write32(DIST_IPI, IPI_ALL_BUT_SELF | IRQ_ID_STOP);
}

/** send rescheduling request to another processor */
void bsp_cpu_reschedule(unsigned int cpu)
{
	dist_write32(DIST_IPI, 0x10000u << cpu | IRQ_ID_RESCHED);
}

/** detect the number of processors and initialize SMP stuff */
__init unsigned int mpcore_init_smp(void)
{
	unsigned int val;
	unsigned int num_cpus;

	val = dist_read32(DIST_TYPE);
	num_cpus = ((val >> 5) & 0x7) + 1;
	printf("GIC supports %u CPUs\n", num_cpus);

	bsp_cpu_num = num_cpus;

	return num_cpus;
}
#endif

/** initialize the MPCore distributed interrupt controller */
__init void mpcore_irq_init(void)
{
	unsigned int irq;
	unsigned int val;
	unsigned int i;

	val = dist_read32(DIST_TYPE);
	mpcore_num_irqs = ((val & 0x1f) + 1) * 32;
	printf("GIC supports %u IRQs\n", mpcore_num_irqs);

	/* mask all interrupts and clear pending bits */
	for (i = 0; i < mpcore_num_irqs / 32; i++) {
		dist_write32(DIST_DISABLE + i * 4, 0xffffffff);
		dist_write32(DIST_PENDING + i * 4, 0xffffffff);
	}

	/* set all interrupts to max prio */
	for (i = 0; i < mpcore_num_irqs; i++) {
		/* highest possible priority */
		dist_write8(DIST_PRIO + i, 0x00);
		/* default to first CPU */
		dist_write8(DIST_TARGET + i, 0x01);
	}

	/* FIXME: need to load a valid edge/level configuration for each interrupt! */
	/* The reset value for the different interrupts depends on the CPU type.
	 *
	 * SGI interrupts always use 0xaaaaaaaa (edge-triggered)
	 *
	 * PPI interrupts depend on the CPU type:
	 *
	 * - Cortex-A9/A5:
	 *   IRQ	Interrupt	Name						Type
	 *    27	PPI0		global timer				rising-edge
	 *    28	PPI1		legacy nFIQ					active-LOW
	 *    29	PPI2		private timer				rising-edge
	 *    30	PPI3		watchdog					rising-edge
	 *    31	PPI4		legacy nIRQ					active-LOW
	 *
	 * - Cortex-A15/A7/GIC-400:
	 *   IRQ	Interrupt	Name						Type
	 *    25	PPI6		virtual maintenance			active-HIGH
	 *    26	PPI5		hypervisor timer			active-LOW
	 *    27	PPI4		virtual timer				active-LOW
	 *    28	PPI0		legacy nFIQ					active-LOW
	 *    29	PPI1		secure physical timer		active-LOW
	 *    30	PPI2		non-secure physical timer	active-LOW
	 *    31	PPI3		nIRQ						active-LOW
	 *
	 * Note that the PPI assignment is different!
	 *
	 * SGI interrupts always use 0x55555555 (active-HIGH)
	 */
	dist_write32(DIST_IRQ_CONFIG + 0, 0xaaaaaaaa);
#if defined(ARM_CORTEX_A9) || defined(ARM_CORTEX_A5)
	dist_write32(DIST_IRQ_CONFIG + 4, 0x7dc00000);
#else
	dist_write32(DIST_IRQ_CONFIG + 4, 0x55540000);
#endif
	for (i = 2; i < mpcore_num_irqs / 16; i++) {
		dist_write32(DIST_IRQ_CONFIG + i * 4, 0x55555555);
	}

	/* set bit 0 to globally enable the GIC */
	val = dist_read32(DIST_CTRL);
	val |= 1;
	dist_write32(DIST_CTRL, val);

	/* setup default interrupt handler */
	for (irq = 0; irq < NUM_IRQS; irq++) {
		irq_handlers[irq] = kernel_irq;
	}

	/* register special interrupt handlers */
	irq_handlers[IRQ_ID_STOP] = stop_irq_handler;
	irq_handlers[IRQ_ID_RESCHED] = resched_irq_handler;
}

/** enable the CPU specific GIC parts */
__init void mpcore_gic_enable(void)
{
	unsigned int val;

	/* mask and clear all private interrupts */
	dist_write32(DIST_DISABLE, 0xffffffff);
	dist_write32(DIST_PENDING, 0xffffffff);

	/* prio 255 let all interrupts pass through, no preemption */
	gic_write32(GIC_PRIO, 0xff);
	gic_write32(GIC_BINPOINT, 0x7);

	/* set bit 0 to enable the GIC CPU interface */
	val = gic_read32(GIC_CTRL);
	val |= 1;
	gic_write32(GIC_CTRL, val);
}


/** check if the user may attach to an interrupt:
 * returns:
 *  1 -> OK
 *  0 -> not available or not suitable for sharing
 * -1 -> out of range
 */
// Configure interrupt source
// this call returns zero if the interrupt source was configured to the
// requested mode, or non-zero if not.
// the mode "IRQ_AUTO" always succeeds if the interrupt source is available.
// the call also fails if the interrupt is not available
err_t bsp_irq_config(unsigned int irq, irq_mode_t mode __unused)
{
	assert(irq < NUM_IRQS);

	if (irq < IRQ_ID_FIRST_SPI)	/* private or special */
		return EBUSY;
	if (irq >= mpcore_num_irqs)	/* not available */
		return EBUSY;

	return EOK;
}

/** register a handler with an interrupt */
void bsp_irq_register(unsigned int irq, bsp_irq_handler_t handler)
{
	assert(irq < mpcore_num_irqs);

	irq_handlers[irq] = handler;
}

/** mask IRQ in distributor */
void bsp_irq_disable(unsigned int irq)
{
	unsigned int word, bit;

	assert(irq < mpcore_num_irqs);

	word = irq / 32;
	bit = irq % 32;
	dist_write32(DIST_DISABLE + 4 * word, 1u << bit);
}

/** unmask IRQ in distributor */
void bsp_irq_enable(unsigned int irq, unsigned int cpu_id __unused)
{
	unsigned int word, bit;

	assert(irq < mpcore_num_irqs);

#ifdef SMP
	/* set target processor */
	dist_write8(DIST_TARGET, 1u << cpu_id);
#endif

	word = irq / 32;
	bit = irq % 32;
	dist_write32(DIST_ENABLE + 4 * word, 1u << bit);
}

/** dispatch IRQ: mask and ack, call handler */
// Dispatch interrupt (called from architecture layer)
// Note that "vector" has an architecture specific meaning
void bsp_irq_dispatch(ulong_t vector __unused)
{
	unsigned int val;
	unsigned int irq;

	do {
		val = gic_read32(GIC_ACK);

		irq = val & VALID_IRQ_MASK;
		if (irq < 32) {
			/* per CPU interrupts, never masked, directly call handler */
			irq_handlers[irq](irq);
		} else if (irq == VALID_IRQ_MASK) {
			/* spurious! */
			break;
		} else {
			/* device interrupt: first mask, then call handler */
			assert(irq < mpcore_num_irqs);
			bsp_irq_disable(irq);

			/* call handler */
			irq_handlers[irq](irq);
		}

		gic_write32(GIC_EOI, val);

		/* check for further pending interrupts */
		val = gic_read32(GIC_PENDING);
	} while ((val & VALID_IRQ_MASK) != VALID_IRQ_MASK);
}
