/* SPDX-License-Identifier: MIT */
/*
 * board.c
 *
 * Board initialization for NXP IMX6 Cortex A9.
 *
 * azuepke, 2013-11-19: initial cloned
 * awerner, 2020-01-30: copy form qemu and port to imx6
 */

#include <kernel.h>
#include <bsp.h>
#include <stdio.h>
#include <board.h>
#include <mpcore.h>
#include <assert.h>
#include <mpcore_timer.h>
#include <arm_insn.h>


/** BSP name string */
const char bsp_name[] = "imx6";
void _start(void);

#ifdef SMP
// CPUs online
volatile ulong_t bsp_cpu_online_mask = 0;
// CPUs started scheduling
volatile int bsp_cpu_go = 0;

/** release the hounds */
__init void bsp_cpu_start_secondary(void)
{
	unsigned int cpu;

	/* Sytem reset controller @ 0x020d8000, see page 5069 in i.MX6 manual */
	volatile uint32_t *src_scr = (volatile uint32_t *)0x020d8000;
	volatile uint32_t *src_gpr = (volatile uint32_t *)0x020d8020;

	assert(bsp_cpu_id() == 0);
	assert(bsp_cpu_online_mask == 0x0);

	for (cpu = 1; cpu < bsp_cpu_num; cpu++) {
		/* set SMP entry point */
		src_gpr[cpu * 2] = (unsigned long)_start;
		/* bits 22..24: enable core1..3 */
		/* bits 13..16: reset core0..3 */
		*src_scr |= (1 << (21 + cpu)) | (1 << (13 + cpu));
		arm_dmb();

		/* wait until CPU is up */
		while (!(bsp_cpu_online_mask & (1u << cpu))) {
			arm_wfe();
		}
	}
}

/** callback to signal successful booting of a processor */
__init void bsp_cpu_up(unsigned int cpu __unused)
{
	assert(cpu == bsp_cpu_id());
	bsp_cpu_online_mask |= (1u << cpu);

	/* let all CPUs spin on "bsp_cpu_go" to have the timers synchronized */
	if (cpu == 0) {
		bsp_cpu_go = 1;
		arm_dmb();
		arm_sev();
	} else {
		arm_dmb();
		arm_sev();
		/* wait until we see the "go" signal */
		while (bsp_cpu_go == 0) {
			arm_wfe();
		}
	}

	/* start timer here on SMP */
	mpcore_timer_start();
}
#endif

// initialize BSP
__init void bsp_init(void)
{
	unsigned int cpu_id;

#ifdef SMP
	cpu_id = bsp_cpu_id();
#else
	cpu_id = 0;
#endif

	/* do low-level init only on first CPU */
	if (cpu_id == 0) {
		serial_init(38400);
		printf("Starting up ...\n");

#ifdef SMP
		mpcore_init_smp();
#endif

		mpcore_irq_init();
		mpcore_gic_enable();
		mpcore_timer_init(100);	/* HZ */
#ifndef SMP
		/* start timer here on UP */
		mpcore_timer_start();
#endif
	}
#ifdef SMP
	else {
		printf("secondary CPU %d came up\n", bsp_cpu_id());
		mpcore_gic_enable();
	}
#endif
}

// idle the CPU
void bsp_idle(void)
{
	/* use WFE instead of WFI to keep the cycle counter running */
	__asm__ volatile ("dsb; wfe" : : : "memory");
}

// Cache management
err_t bsp_cache(
	cache_op_t op,
	addr_t start,
	size_t size,
	addr_t alias)
{
	(void)op;
	(void)start;
	(void)size;
	(void)alias;
	return ENOSYS;
}

// halt the board (shutdown of the system)
void bsp_halt(
	halt_mode_t mode __unused)
{
#ifdef SMP
	if (mode != BOARD_STOP) {
		mpcore_send_stop();
	}
#endif

	__bsp_halt();
	unreachable();
}
