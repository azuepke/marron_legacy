/* SPDX-License-Identifier: MIT */
/*
 * kernel.ld.in
 *
 * Kernel linker script
 *
 * azuepke, 2013-03-22: initial
 * azuepke, 2013-04-06: use CPP for includes
 * azuepke, 2013-08-06: added 64-bit support
 * azuepke, 2017-07-17: adapted to mini-OS
 */

#ifdef FINAL_RUN
#include "final.ld.h"
#else
#include "dummy.ld.h"
#endif

/* FIXME: hardcoded stack size */
#define KERNEL_STACK_SIZE	2048
#define FIQ_STACK_SIZE		1024

OUTPUT_FORMAT("elf32-littlearm")
OUTPUT_ARCH("arm")
ENTRY(_start)

SECTIONS
{
	.text (0x10008000) : {
		__text_start = .;
		*(.text.start)
		*(.text.init)
		*(.text.cold)

		__text_hot_start = .;
		*(.text)
		__text_exception_start = .;
		*(.text.exception)
		__text_hot_end = .;
		*(.text.*)
	}

	.rodata : {
		*(.rodata)
		*(.rodata.*)
		*(.rodata1)
		*(.sdata2)
		*(.sbss2)

		/* exception table */
		. = ALIGN(16);
		__ex_table_start = .;
		*(__ex_table)
		__ex_table_end = .;

		/* place page tables in .rodata, or the linker places
		 * empty page tables into .bss
		 */
		*(.pagetables.l1)
		*(.pagetables.l2)
		*(.pagetables.l2_dummy)

		__text_end = .;
	}

	.data (0x10100000) : {
		__data_start = .;
		*(.data)
		*(.data.*)
		*(.data1)
		*(.sdata)
		__data_end = .;
	}
	.bss ALIGN(16) : {
		__bss_start = .;
		*(.sbss)
		*(.scommon)
		*(.bss)
		*(.bss.*)
		*(COMMON)

		/* kernel stacks and per-CPU data areas */
#if CFG_THREADS >= 1
		/* kernel stack, aligned to 16 bytes */
		. = ALIGN(16);
		kern_stack_cpu0 = .;
		. += KERNEL_STACK_SIZE;
		kern_stack_cpu0_top = .;

		/* per-CPU area, kept after stack */
		*(.percpu.cpu0)

		/* FIQ stack, aligned to 16 bytes */
		. = ALIGN(16);
		fiq_stack_cpu0 = .;
		. += FIQ_STACK_SIZE;
		fiq_stack_cpu0_top = .;
#endif
#if CFG_THREADS >= 2
		/* kernel stack, aligned to 16 bytes */
		. = ALIGN(16);
		kern_stack_cpu1 = .;
		. += KERNEL_STACK_SIZE;
		kern_stack_cpu1_top = .;

		/* per-CPU area, kept after stack */
		*(.percpu.cpu1)

		/* FIQ stack, aligned to 16 bytes */
		. = ALIGN(16);
		fiq_stack_cpu1 = .;
		. += FIQ_STACK_SIZE;
		fiq_stack_cpu1_top = .;
#endif
#if CFG_THREADS >= 3
		/* kernel stack, aligned to 16 bytes */
		. = ALIGN(16);
		kern_stack_cpu2 = .;
		. += KERNEL_STACK_SIZE;
		kern_stack_cpu2_top = .;

		/* per-CPU area, kept after stack */
		*(.percpu.cpu2)

		/* FIQ stack, aligned to 16 bytes */
		. = ALIGN(16);
		fiq_stack_cpu2 = .;
		. += FIQ_STACK_SIZE;
		fiq_stack_cpu2_top = .;
#endif
#if CFG_THREADS >= 4
		/* kernel stack, aligned to 16 bytes */
		. = ALIGN(16);
		kern_stack_cpu3 = .;
		. += KERNEL_STACK_SIZE;
		kern_stack_cpu3_top = .;

		/* per-CPU area, kept after stack */
		*(.percpu.cpu3)

		/* FIQ stack, aligned to 16 bytes */
		. = ALIGN(16);
		fiq_stack_cpu3 = .;
		. += FIQ_STACK_SIZE;
		fiq_stack_cpu3_top = .;
#endif

		__bss_end = .;
	}

	/DISCARD/ : {
		*(.eh_frame)
		*(.ARM.exidx)
	}
}
