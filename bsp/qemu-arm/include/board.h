/* SPDX-License-Identifier: MIT */
/*
 * board.h
 *
 * Board specific setting for QEMU Versatile Express Cortex A15.
 *
 * azuepke, 2013-11-19: initial
 * azuepke, 2017-07-17: adapted to mini-OS
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#ifndef __ASSEMBLER__

/* start.S */
void __bsp_halt(void);

/* pl011_uart.c */
void serial_init(unsigned int baud);

#endif

/* specific memory layout of the QEMU Cortex A15 board */
#define BOARD_RAM_PHYS		0x80000000
#define BOARD_RAM_SIZE		0x04000000	/* 64 MB */
#define BOARD_RAM_VIRT		0x80000000

/* 1st IO region, peripherals, e.g. serial, timer */
#define BOARD_IO1_PHYS		0x1c000000
#define BOARD_IO1_SIZE		0x00200000	/* 2 MB */
#define BOARD_IO1_VIRT		0x1c000000

#if 0	/* not used yet */
/* 2nd IO region, daughterboard peripherals */
#define BOARD_IO2_PHYS		0x1c100000
#define BOARD_IO2_SIZE		0x00100000	/* 1 MB */
#define BOARD_IO2_VIRT		0x1c100000
#endif

/* 3rd IO region, MPCore region */
#define BOARD_IO3_PHYS		0x2c000000
#define BOARD_IO3_SIZE		0x00200000	/* 2 MB */
#define BOARD_IO3_VIRT		0x2c000000

/* region 0xfff00000 must be left out for the mapping of the vector pages! */

/* like Linux, we use a load offset of 32K relative to an 1M aligned region */
#define LOAD_ADDR	0x80008000	/* physical kernel load address */

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

/** MPCore specific addresses */
#define GIC_DIST_BASE			BOARD_IO3_VIRT + 0x1000
#define GIC_PERCPU_BASE			BOARD_IO3_VIRT + 0x2000

#define SP804_TIMER_BASE		BOARD_IO1_VIRT + 0x110000
#define SP804_TIMER_CLOCK		1000000	/* 1 MHz */
#define SP804_TIMER_IRQ			34

/** enforce QEMU SMP startup workaround */
#define QEMU_SMP_STARTUP 1

#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

#endif
