/* SPDX-License-Identifier: MIT */
/*
 * board.c
 *
 * Board initialization for QEMU Versatile Express Cortex A15.
 *
 * azuepke, 2013-11-19: initial cloned
 */

#include <kernel.h>
#include <bsp.h>
#include <stdio.h>
#include <board.h>
#include <mpcore.h>
#include <current.h>
#include <assert.h>
#include <cpu_timer.h>
#include <arm_insn.h>


/** BSP name string */
const char bsp_name[] = "qemu-arm";

#ifdef SMP
// CPUs online
volatile ulong_t bsp_cpu_online_mask = 0;
// CPUs started scheduling
volatile int bsp_cpu_go = 0;

/** release the hounds */
__init void bsp_cpu_start_secondary(void)
{
	unsigned int cpu;

	assert(__current_cpu_id() == 0);
	assert(bsp_cpu_online_mask == 0x0);

	/* we flush the caches before doing anything, as the first instructions
	 * of a newly started processor runs in uncached memory mode.
	 */
	for (cpu = 1; cpu < bsp_cpu_num; cpu++) {
		__boot_release = cpu;
		arm_dmb();
		arm_clean_dcache(&__boot_release);
		arm_sev();

		/* wait until CPU is up */
		while (!(bsp_cpu_online_mask & (1u << cpu))) {
			arm_wfe();
		}
	}
}

/** callback to signal successful booting of a processor */
__init void bsp_cpu_up(unsigned int cpu __unused)
{
	assert(cpu == __current_cpu_id());
	bsp_cpu_online_mask |= (1u << cpu);

	/* let all CPUs spin on "bsp_cpu_go" to have the timers synchronized */
	if (cpu == 0) {
		bsp_cpu_go = 1;
		arm_dmb();
		arm_sev();
	} else {
		arm_dmb();
		arm_sev();
		/* wait until we see the "go" signal */
		while (bsp_cpu_go == 0) {
			arm_wfe();
		}
	}

	/* start timer here on SMP */
	cpu_timer_start();
}
#endif

// initialize BSP
__init void bsp_init(void)
{
	unsigned int cpu_id;

	cpu_id = __current_cpu_id();

	/* do low-level init only on first CPU */
	if (cpu_id == 0) {
		serial_init(38400);
		printf("Starting up ...\n");

#ifdef SMP
		mpcore_init_smp();
#endif

		mpcore_irq_init();
		mpcore_gic_enable();
		cpu_timer_init(100);	/* HZ */
#ifndef SMP
		/* start timer here on UP */
		cpu_timer_start();
#endif
	}
#ifdef SMP
	else {
		mpcore_gic_enable();
	}
#endif
}

// idle the CPU
void bsp_idle(void)
{
	__asm__ volatile ("dsb; wfi" : : : "memory");
}

// Cache management
err_t bsp_cache(
	cache_op_t op,
	addr_t start,
	size_t size,
	addr_t alias)
{
	(void)op;
	(void)start;
	(void)size;
	(void)alias;
	return ENOSYS;
}

// halt the board (shutdown of the system)
void bsp_halt(
	halt_mode_t mode __unused)
{
#ifdef SMP
	if (mode != BOARD_STOP) {
		mpcore_send_stop();
	}
#endif

	__bsp_halt();
	unreachable();
}

