/* SPDX-License-Identifier: MIT */
/*
 * cpu_timer.c
 *
 * ARM generic per-CPU virtual timer (for Cortex-A15 or newer)
 *
 * azuepke, 2015-07-24: initial
 * azuepke, 2018-03-05: imported
 * azuepke, 2020-11-22: cleanup
 */

#include <kernel.h>
#include <assert.h>
#include <bsp.h>
#include <mpcore.h>
#include <compiler.h>
#include <cpu_timer.h>
#include <current.h>
#include <arch.h>
#include <stdio.h>


#define CTL_ENABLE			0x001	/* timer enabled */
#define CTL_IMASK			0x002	/* interrupt masked */
#define CTL_ISTATUS			0x004	/* interrupt pending */

/** Set counter commpare */
static inline void arm_set_cnt_compare(unsigned long long val)
{
	__asm__ volatile ("mcrr p15, 3, %Q0, %R0 , c14" : : "r" (val) : "memory");
}

/** Get counter control */
static inline unsigned long arm_get_cnt_control(void)
{
	unsigned long val;
	/* CNTV_CTL */
	__asm__ volatile ("mrc p15, 0, %0, c14, c3, 1" : "=r"(val));
	return val;
}

/** Set counter control */
static inline void arm_set_cnt_control(unsigned long val)
{
	/* CNTV_CTL */
	__asm__ volatile ("mcr p15, 0, %0, c14, c3, 1" : : "r"(val) : "memory");
}

/** Get counter frequency (user accessible) */
static inline unsigned long arm_get_cnt_freq(void)
{
	/* some SOCs not support get Counter Frequency! */
	unsigned long val;
	/* CNTFRQ */
	__asm__ volatile ("mrc p15, 0, %0, c14, c0, 0" : "=r"(val));
	return val;
}

/** Get virtual counter value (user accessible) */
static inline unsigned long long arm_get_cnt_val(void)
{
	unsigned long long val;
	__asm__ volatile ("isb; mrrc p15, 1, %Q0, %R0, c14" : "=r" (val));
	return val;
}

/** Per CPU timer state */
struct per_cpu_timer {
	/* FIXME: the jiffies will overflow in 49 days at 1000 HZ! */
	uint64_t next_expiry;
	uint32_t jiffies;
	/* we copy the data over to each per-CPU data (less cachelines to access) */
	uint32_t reload;
	uint32_t clock_ns;
} __aligned(ARCH_ALIGN);

static struct per_cpu_timer timer_state[4];

// Timer resolution in nanoseconds
uint64_t bsp_timer_resolution;

uint64_t bsp_timer_get_time(void)
{
	unsigned int cpu_id = __current_cpu_id();
	struct per_cpu_timer *t = &timer_state[cpu_id];

	return (uint64_t)t->jiffies * t->clock_ns;
}

// set next timer expiry (oneshot)
void bsp_timer_set_expiry(uint64_t expiry __unused, unsigned int cpu_id __unused)
{
	/* ignored in periodic mode */
}

static void timer_handler(unsigned int irq __unused)
{
	unsigned int cpu_id = __current_cpu_id();
	struct per_cpu_timer *t = &timer_state[cpu_id];

	t->next_expiry += t->reload;
	arm_set_cnt_compare(t->next_expiry);

	t->jiffies++;

	/* notify kernel to expiry timeouts */
	kernel_timer((uint64_t)t->jiffies * t->clock_ns);
}

/** timer implementation -- uses ARM per core virtual timer */
__init void cpu_timer_init(unsigned int freq)
{
	uint32_t reload;
	uint32_t clock_ns;

#ifdef CPU_COUNTER_FREQ
	reload = CPU_COUNTER_FREQ / freq;
#else
	reload = arm_get_cnt_freq() / freq;
#endif
	clock_ns = 1000000000 / freq;
	bsp_timer_resolution = clock_ns;

	for (unsigned int i = 0; i < countof(timer_state); i++) {
		struct per_cpu_timer *t = &timer_state[i];
		t->reload = reload;
		t->clock_ns = clock_ns;
	}

	/* install handler */
	bsp_irq_register(400 + IRQ_ID_VTIMER, timer_handler);
}


/** timer implementation -- uses ARM per core virtual timer */
__init void cpu_timer_start(void)
{
	unsigned int cpu_id = __current_cpu_id();
	struct per_cpu_timer *t = &timer_state[cpu_id];

	/* unmask interrupt in GIC */
	bsp_irq_enable(400 + IRQ_ID_VTIMER, cpu_id);

	t->next_expiry = arm_get_cnt_val() + t->reload;
	arm_set_cnt_compare(t->next_expiry);

	/* enable timer interrupt, unmasked */
	arm_set_cnt_control(CTL_ENABLE);
}
