/* SPDX-License-Identifier: MIT */
/*
 * board.c
 *
 * Board initialization for QEMU Versatile Express Cortex A15.
 *
 * azuepke, 2013-11-19: initial cloned
 */

#include <kernel.h>
#include <bsp.h>
#include <stdio.h>
#include <board.h>
#include <mpcore.h>
#include <assert.h>
#include <cpu_timer.h>
#include <arm_insn.h>
#include <master_timer.h>


/** BSP name string */
const char bsp_name[] = "am57xx";

#ifdef SMP
// CPUs online
volatile ulong_t bsp_cpu_online_mask = 0;
// CPUs started scheduling
volatile int bsp_cpu_go = 0;

/** release the hounds */
__init void bsp_cpu_start_secondary(void)
{
	volatile uint32_t *PM_CPU1_PWRSTCTRL = (volatile uint32_t *) 0x48243800;
	volatile uint32_t *RM_CPU1_CPU1_RSTCTRL = (volatile uint32_t *) 0x48243810;
	volatile uint32_t *WKG_CONTROL_1 = (volatile uint32_t *) 0x48281400;
	volatile uint32_t *AUX_CORE_BOOT = (volatile uint32_t *) 0x48281800;
	uint32_t pwr;
	printf("Start CPU1\n");

	assert(bsp_cpu_id() == 0);
	assert(bsp_cpu_online_mask == 0x0);

	/**
	 * Check CPU is Power UP
	 */
	pwr = *PM_CPU1_PWRSTCTRL;
	if ((pwr & (1 << 7))) {
		printf("Disable forced off state\n");
		/* disable forced in OFF state */
		pwr &= ~(1 << 7);
		*PM_CPU1_PWRSTCTRL = pwr;
		arm_dmb();
	}
	if ((pwr & 0x3) != 0x3) {
		printf("Power and Reset CPU1\n");
		/* Set CPU to ON State */
		pwr |= 0x3;
		*PM_CPU1_PWRSTCTRL = pwr;
	}
	/* reset CPU1 */
	*RM_CPU1_CPU1_RSTCTRL |= 0x1;
	arm_dmb();
	/* relase the reset of CPU1 */
	*RM_CPU1_CPU1_RSTCTRL &= ~0x1;
	arm_dmb();

	AUX_CORE_BOOT[1] = (uint32_t) 0x80008000;
	AUX_CORE_BOOT[0] = 0x00000020;
	arm_dmb();
	printf("Wakeup CPU1\n");
	*WKG_CONTROL_1 |= (1 << 9);
	arm_dmb();
	arm_sev();

	/* wait until CPU1 is up */
	while (!(bsp_cpu_online_mask & (1u << 1))) {
		arm_wfe();
	}
	printf("CPU 1 is online\n");
}

/** callback to signal successful booting of a processor */
__init void bsp_cpu_up(unsigned int cpu __unused)
{
	assert(cpu == bsp_cpu_id());
	bsp_cpu_online_mask |= (1u << cpu);

	/* let all CPUs spin on "bsp_cpu_go" to have the timers synchronized */
	if (cpu == 0) {
		bsp_cpu_go = 1;
		arm_dmb();
		arm_sev();
	} else {
		arm_dmb();
		arm_sev();
		/* wait until we see the "go" signal */
		while (bsp_cpu_go == 0) {
			arm_wfe();
		}
	}

	/* start timer here on SMP */
	cpu_timer_start();
}
#endif

// initialize BSP
__init void bsp_init(void)
{
	unsigned int cpu_id;

#ifdef SMP
	cpu_id = bsp_cpu_id();
#else
	cpu_id = 0;
#endif

	/* do low-level init only on first CPU */
	if (cpu_id == 0) {
		serial_init(115200);
		printf("Starting up ...\n");
		master_timer_init();

#ifdef SMP
		mpcore_init_smp();
#endif

		mpcore_irq_init();
		mpcore_gic_enable();
		cpu_timer_init(100);	/* HZ */
#ifndef SMP
		/* start timer here on UP */
		cpu_timer_start();
#endif
	}
#ifdef SMP
	else {
		mpcore_gic_enable();
	}
#endif
}

// idle the CPU
void bsp_idle(void)
{
	__asm__ volatile ("dsb; wfi" : : : "memory");
}

// Cache management
unsigned int bsp_cache(
	cache_op_t op,
	addr_t start,
	size_t size,
	addr_t alias)
{
	(void)op;
	(void)start;
	(void)size;
	(void)alias;
	return ENOSYS;
}

// halt the board (shutdown of the system)
void bsp_halt(
	halt_mode_t mode __unused)
{
#ifdef SMP
	if (mode != BOARD_STOP) {
		mpcore_send_stop();
	}
#endif

	__bsp_halt();
	unreachable();
}

