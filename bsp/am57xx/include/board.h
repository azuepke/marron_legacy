/* SPDX-License-Identifier: MIT */
/*
 * board.h
 *
 * Board specific setting for QEMU Versatile Express Cortex A15.
 *
 * azuepke, 2013-11-19: initial
 * azuepke, 2017-07-17: adapted to mini-OS
 * awerner, 2018-03-16: AM57xx port (from QEMU)
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#ifndef __ASSEMBLER__

/* start.S */
void __bsp_halt(void);

/* pl011_uart.c */
void serial_init(unsigned int baud);

#endif

/* specific memory layout of the QEMU Cortex A15 board */
#define BOARD_RAM_PHYS		0x80000000
#define BOARD_RAM_SIZE		0x20000000	/* 512MB TODO Begaboard and Phytec Board has 2 GB RAM */
#define BOARD_RAM_VIRT		0x80000000

/* 1st IO region, peripherals, e.g. serial, timer */
#define BOARD_IO1_PHYS		0x44000000
#define BOARD_IO1_SIZE		0x10000000	/* 256 MB */
#define BOARD_IO1_VIRT		0x44000000

#if 0	/* not used yet */
/* 2nd IO region, daughterboard peripherals */
#define BOARD_IO2_PHYS		0x1c100000
#define BOARD_IO2_SIZE		0x00100000	/* 1 MB */
#define BOARD_IO2_VIRT		0x1c100000
#endif

/* 3rd IO region, MPCore region */
#define BOARD_IO3_PHYS		0x48200000
#define BOARD_IO3_SIZE		0x00200000	/* 2 MB */
#define BOARD_IO3_VIRT		0x48200000

/* region 0xfff00000 must be left out for the mapping of the vector pages! */

/* like Linux, we use a load offset of 32K relative to an 1M aligned region */
#define LOAD_ADDR	0x80008000	/* physical kernel load address */

#define BOARD_PHYS_TO_KERNEL(x)	((x) + BOARD_RAM_VIRT - BOARD_RAM_PHYS)
#define BOARD_KERNEL_TO_PHYS(x)	((x) - BOARD_RAM_VIRT + BOARD_RAM_PHYS)

/** MPCore specific addresses */
#define GIC_DIST_BASE			BOARD_IO3_VIRT + 0x11000
#define GIC_PERCPU_BASE			BOARD_IO3_VIRT + 0x12000

#define UART_BASE                       0x48020000 /* uart3 */
#if 0
#define UART_BASE                       0x4806A000 /* uart1 */
#define UART_BASE                       0x4806C000 /* uart2 */
#define UART_BASE                       0x48020000 /* uart3 */
#define UART_BASE                       0x4806E000 /* uart4 */
#define UART_BASE                       0x48066000 /* uart5 */
#define UART_BASE                       0x48068000 /* uart6 */
#define UART_BASE                       0x48420000 /* uart7 */
#define UART_BASE                       0x48422000 /* uart8 */
#define UART_BASE                       0x48424000 /* uart9 */
#define UART_BASE                       0x4AE2B000 /* uart10 */
#endif

#define UART_CLOCK			48000000	/* 48 MHz */

#ifndef __ASSEMBLER__
extern unsigned int __boot_release;
#endif

#endif
