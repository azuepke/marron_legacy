/* SPDX-License-Identifier: MIT */
/*
 * current.h
 *
 * Determine current CPU ID (via MPIDR on ARM)
 *
 * azuepke, 2020-08-05: initial
 */

#ifndef __CURRENT_H__
#define __CURRENT_H__

#include <arm_insn.h>

/** Get current CPU ID */

#ifdef SMP
#define __current_cpu_id() (int)(arm_get_mpidr() & 0xf)
#else
#define __current_cpu_id() 0
#endif

#endif
