#ifndef __IRQ_CROSSBAR__H__
#define __IRQ_CROSSBAR__H__
#include <stdint.h>

uint32_t am57xx_map_interrupt(uint32_t irqnr);
void am57xx_irq_crossbar_init(void);
uint32_t am57xx_crossbar_to_gic(unsigned int irqnr);
uint32_t am57xx_gic_to_crossbar(unsigned int irqnr);
#endif
