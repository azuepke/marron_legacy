#!/usr/bin/env python3
# SPDX-License-Identifier: MIT
#
# gen_core.py -- Generator common code
#
# azuepke, 2017-09-22: initial
# azuepke, 2017-10-06: header generator
# azuepke, 2017-10-19: forked from header generator
# azuepke, 2017-10-20: first working version
# azuepke, 2017-10-23: new data model
# azuepke, 2017-10-26: factored out common code library and merged generators
# azuepke, 2017-11-01: first working version of the config.c generator
# azuepke, 2018-01-02: include scheduling data
# azuepke, 2018-01-13: generate interrupt data
# azuepke, 2018-03-05: expand shell strings
# azuepke, 2018-03-05: generate page tables for ARM v6
# azuepke, 2018-04-10: event channels
# azuepke, 2018-04-19: simplified futex implementation
# azuepke, 2020-01-31: handle Cortex-A8 correctly
#
#
# What does this library do?
#
# - scan hardware.xml and iterate all memory pools and ios
# - scan config.xml, iterate all SHMs, partitions and the kernel
#
# We can generate multiple files:
# - linker_h contain all link-time addresses, either resolved or unresolved
#   (either as dummy_h or final_h)
# - config_h contains all static partition data which do not change anymore
# - config_c contains the kernel configuration (emitted only for kernel)
# - memrqs_xml contains all memory requirements


from __future__ import print_function
import xml.etree.ElementTree as ET
import subprocess
import time
import sys
import os
from string import Template

# global variables
verbose = False
nm_tool = "nm"
progname = "gen_core.py"


# list of all <mem> entries + hashed by name
mem_num = 0
mem_all = []
mem_by_name = {}

# hash of all <io> entries by name + hashed by name
io_num = 0
io_all = []
io_by_name = {}

# list of all <shm> entries + hashed by name
shm_num = 0
shm_all = []
shm_by_name = {}

# list of all <kernel> and <partition> entries + hashed by name
part_num = 0
part_all = []
part_by_name = {}

# hash of all <irq> entries + hashed by IRQ_ID
irq_num = 0
irq_all = []
irq_by_id = {}

# list of all <event_wait> and <event_send> entries + hashed by name
event_wait_num = 0
event_wait_all = []
event_send_num = 0
event_send_all = []

# global IDs of various resources
memrq_num = 0
io_map_num = 0
shm_map_num = 0
thread_num = 0
sched_num = 1
cpu_num = 1
hw_irq_num = 0
idle_stack_size = 0
arm_cortex = ''
is_arm_cortex_a8 = False


# --- helper functions ---

# replace shell strings like $HOME in string
def shell_expand(s):
    if s is None:
        return None
    return Template(s).substitute(os.environ)

# set program name
def set_progname(name):
    global progname
    progname = name

# set verbose
def set_verbose(v):
    global verbose
    verbose = v

# set nm tool
def set_nm_tool(nm):
    global nm_tool
    nm_tool = nm

# join arguments to string
# Phyton 3.4 cannot handle *args in die() otherwise
def jj(*args):
    if len(args) == 0:
        return ''
    j = ''.join(*args)
    if j == '':
        return ''
    return j + ': '

# print to stderr and abort program execution
def die(*args, **kwargs):
    print("%s: error:" % progname, *args, file=sys.stderr, **kwargs)
    sys.exit(1)


# print to stdio if verbose
def vprint(*args):
    if verbose:
        print(*args)


# print line to file
def emit(fd, *args):
    print(*args, file=fd)


# read mandatory XML string attribute
def get_str(xml, attr_name, *args):
    attr = shell_expand(xml.get(attr_name))
    if attr is None:
        die(jj(*args) + '<%s>: attribute "%s" missing' % (xml.tag, attr_name))
    if len(attr) == 0:
        die(jj(*args) + '<%s>: attribute "%s" is empty ' % (xml.tag, attr_name))
    return attr


# read optional XML string attribute, returns default value if missing
def get_str_opt(xml, attr_name, default, *args):
    attr = shell_expand(xml.get(attr_name))
    if attr is None:
        return default
    return attr


# read mandatory XML number attribute
def get_num(xml, attr_name, *args):
    attr = get_str(xml, attr_name, *args)
    try:
        return int(attr, 0)
    except:
        die(jj(*args) + '<%s>: attribute "%s" value "%s" is not a number' % (xml.tag, attr_name, attr))


# read optional XML number attribute, returns default value if missing
def get_num_opt(xml, attr_name, default, *args):
    attr = shell_expand(xml.get(attr_name))
    if attr is None:
        return default
    if len(attr) == 0:
        die(jj(*args) + '<%s>: attribute "%s" is empty ' % (xml.tag, attr_name))
    try:
        return int(attr, 0)
    except:
        die(jj(*args) + '<%s>: attribute "%s" value "%s" is not a number' % (xml.tag, attr_name, attr))


# read mandatoryXML boolean attribute
def get_bool(xml, attr_name, *args):
    attr = get_str(xml, attr_name, *args)
    if attr == "1":
        return True
    elif attr == "0":
        return False
    else:
        die(jj(*args) + '<%s>: attribute "%s" value "%s" is not a bool ("0" or "1")' % (xml.tag, attr_name, attr))


# read optional XML boolean attribute, returns default value if missing
def get_bool_opt(xml, attr_name, default, *args):
    attr = shell_expand(xml.get(attr_name))
    if attr is None:
        return default
    elif attr == "1":
        return True
    elif attr == "0":
        return False
    else:
        die(jj(*args) + '<%s>: attribute "%s" value "%s" is not a bool ("0" or "1")' % (xml.tag, attr_name, attr))


# get symbols from ELF file
# nm_tool refers to the toolchain's name mangler, e.g. arm-linux-gnueabihf-nm
# returns dict['name'] -> addr
def get_syms(elf_file, sym_sizes, nm_tool = 'nm'):
    syms = {}

    try:
        cmd = [nm_tool, '--no-sort', '--portability', '--extern-only', elf_file]
        output = subprocess.check_output(cmd)
    except subprocess.CalledProcessError as e:
        die('error reading symbols from "%s"' % elf_file)

    for line in output.decode().split('\n'):
        # nm output is formatted as follows:
        #   ['main', 'T', '80012345', '00000123\n']
        #    name    type address     size
        # note that the size is optional, e.g. for linker defined symbols
        # also, undefined symbols have no start address
        l = line.strip().split(' ')
        if len(l) <= 2:
            continue
        name = l[0]
        addr = 0
        size = 0
        try:
            if len(l) >= 3:
                addr = int(l[2], 16)
            if len(l) >= 4:
                size = int(l[3], 16)
        except:
            die('invalid format of nm output')

        syms[name] = addr
        sym_sizes[name] = size

    return syms


# --- hardware.xml ---

# <mem> or <io> entry
class Pool:
    # constructor
    def __init__(self, id, is_pool):
        self.id = id
        self.is_pool = is_pool

        self.name = ''
        self.start = 0
        self.size = 0
        self.current = 0        # current allocation
        self.align = 0x1000     # fits to MMU
        self.r = True
        self.w = False
        self.x = False
        self.cache = 0

    # parse XML <mem> and <io>
    def parse(self, xml):
        self.name = get_str(xml, 'name')
        if self.name in mem_by_name or self.name in io_by_name:
            die('mem or io named "%s" already exist' % self.name)

        self.start = get_num(xml, 'start')
        self.size = get_num(xml, 'size')
        self.align = get_num_opt(xml, 'align', self.align)
        self.r = get_bool_opt(xml, 'read', self.r)
        self.w = get_bool_opt(xml, 'write', self.w)
        self.x = get_bool_opt(xml, 'exec', self.x)
        self.cache = get_num_opt(xml, 'cache', self.cache)


    # parse all <mem> and <io> and register globally
    @staticmethod
    def parse_all(xml):
        global mem_num
        for node in xml.iter('mem'):
            obj = Pool(mem_num, True)
            obj.parse(node)
            mem_all.append(obj)
            mem_by_name[obj.name] = obj
            mem_num += 1

        global io_num
        for node in xml.iter('io'):
            obj = Pool(io_num, False)
            obj.parse(node)
            io_all.append(obj)
            io_by_name[obj.name] = obj
            io_num += 1

    # perform allocation from pool
    def allocate(self, size):
        start = self.current
        # align up
        end = (start + size + self.align - 1) & ~(self.align - 1)
        if end > self.size:
            die('memory pool "%s" exhausted' % self.name)

        self.current = end
        return self.start + start


# parse hardware.xml
def parse_hardware_xml(filename):
    global cpu_num, sched_num, hw_irq_num, idle_stack_size
    global arm_cortex, is_arm_cortex_a8

    try:
        tree = ET.parse(filename)
    except ET.ParseError as e:
        die('"%s":' % filename, e)

    xml = tree.getroot()
    if xml is None:
        die('"%s": no root element' % filename)
    if xml.tag != "hardware":
        die('"%s": must start with <hardware> tag' % filename)

    cpu_num = get_num(xml, 'cpus')
    if cpu_num < 0:
        die('hardware.xml: invalid number of cpus "%d"' % cpu_num)
    sched_num = cpu_num # FIXME: one scheduler state per CPUs

    hw_irq_num = get_num(xml, 'irqs')
    idle_stack_size = get_num(xml, 'idle_stack_size')

    arm_cortex = get_str_opt(xml, 'arm_cortex', 'a9')
    is_arm_cortex_a8 = (arm_cortex == 'a8')

    Pool.parse_all(xml)
    Partition.parse_kernel(xml)


# --- config.xml ---

# A mapping, which contains the addresses of memrq|io_map|shm_map later
class Mapping:
    # constructor
    def __init__(self, name, start_addr, size, r=False, w=False, x=False, cache=0, is_kernel=False):
        self.name = name
        # align start down addr to 4K
        self.start_addr = start_addr & ~(0x1000 - 1)
        # align size up to next 4K
        offset = start_addr - self.start_addr
        self.size = (offset + size + 0x1000 - 1) & ~(0x1000 - 1)
        self.end_addr = self.start_addr + self.size
        self.r = r
        self.w = w
        self.x = x
        self.cache = cache
        self.is_kernel = is_kernel


# Page tables of a partition
class Pagetable:
    # limit of level 2 page tables, i.e.g. amount of memory virtually coverable
    # by a partition
    # NOTE: we always emit the exact number of level 2 page tables to keep
    # the memory consumed by the tables constant for both dummy and final runs
    TABLE_LIMIT = 32

    # PTE bits for ARM v6
    PTE_XN   = 0x001    # execute never
    PTE_P    = 0x002    # present, type: 4K page
    PTE_B    = 0x004    # buffered
    PTE_C    = 0x008    # cached
    PTE_AP0  = 0x010    # AP0 (always set)
    PTE_AP1  = 0x020    # AP1 (enable user access)
    PTE_TEX0 = 0x040    # TEX0
    PTE_TEX1 = 0x080    # TEX1
    PTE_TEX2 = 0x100    # TEX2
    PTE_AP2  = 0x200    # AP2 (disable write access)
    PTE_S    = 0x400    # shareable
    PTE_NG   = 0x800    # non global


    # constructor
    def __init__(self, part_id, mappings):
        self.id = part_id
        self.mappings = mappings
        self.used_tables = 0

    # find a 4K mapping entry, if any
    def find_4k_mapping(self, addr):
        for m in self.mappings:
            if (addr >= m.start_addr) and (addr < m.end_addr):
                return m
        return None

    # check if there is a mapping in the given 1M area
    def check_1m_range(self, range_start):
        range_end = range_start + 0x100000
        for m in self.mappings:
            if (range_start <= m.end_addr) and (m.start_addr < range_end):
                # overlapping in 1M area
                return True
        return False

    # emit level 2 page table covering 1M of virtual memory
    # this page table has 1K size & alignment, and covers 1M of adspace
    def emit_1m_table(self, fd, addr):
        global is_arm_cortex_a8
        emit(fd, 'const uint32_t pagetable_part%d_l2_0x%08x[256] __aligned(1024) __section(.pagetables.l2) = {' % (self.id, addr))
        for i in range(0, 256):
            m = self.find_4k_mapping(addr)
            if m is not None:
                access = self.PTE_P | self.PTE_AP0
                if not m.w:
                    access |= self.PTE_AP2
                if not m.x:
                    access |= self.PTE_XN
                if not m.is_kernel:
                    access |= self.PTE_AP1

                rwx_name = '%s %c%c%c' % ('kern' if m.is_kernel else 'user',
                                          'r' if m.r else '-',
                                          'w' if m.w else '-',
                                          'x' if m.x else '-')

                # TEX C B  memory type           shareable
                #
                # 000 0 0  UC, strongly ordered  shareable
                # 000 0 1  UC, device            shareable
                # 000 1 0  WT, normal            S
                # 000 1 1  WB, normal            S
                # 001 0 0  UC, normal            S
                # 001 1 1  WBWA, normal          S
                # 010 0 0  non-shared device     not shareable

                # FIXME: simply deciding between cached and uncached for now
                if m.cache != 0:
                    if is_arm_cortex_a8:
                        access |= self.PTE_C | self.PTE_B
                        cache_name = "WB, normal, non-shareable"
                    else:
                        access |= self.PTE_TEX0 | self.PTE_C | self.PTE_B | self.PTE_S
                        cache_name = "WBWA, normal, shareable"
                else:
                    access |= 0
                    cache_name = "UC, strongly ordered"

                emit(fd, '\t/* 0x%08x */ 0x%08x + 0x%03x, /* %s %s: %s */' % (addr, addr, access, rwx_name, cache_name, m.name))
            else:
                emit(fd, '\t/* 0x%08x */ 0,' % addr)

            addr += 0x1000
        emit(fd, '};')
        emit(fd)
        self.used_tables += 1

    # emit level 2 dummy page table
    def emit_1m_dummy_table(self, fd):
        emit(fd, 'const uint32_t pagetable_part%d_l2_dummy%d[256] __aligned(1024) __section(.pagetables.l2_dummy);' % (self.id, self.used_tables))
        emit(fd)
        self.used_tables += 1

    # emit all level 2 page tables
    def emit_1m_table_all(self, fd):
        # walk the adspace and emit the real level 2 page tables
        addr = 0
        for i in range(0, 4096):
            if self.check_1m_range(addr):
                self.emit_1m_table(fd, addr)
            addr += 0x100000

        if self.used_tables >= self.TABLE_LIMIT:
            die('partition %s: too many sub-page tables' % self.name)

        # fill up space with dummy tables
        while self.used_tables < self.TABLE_LIMIT:
            self.emit_1m_dummy_table(fd)

    # emit a single level 1 page tables for a partition
    # this page table has 16K size & alignment, and covers 4G of adspace
    def emit_pagetable(self, fd):
        self.emit_1m_table_all(fd)

        emit(fd, 'const uint32_t pagetable_part%d_l1[4096] __aligned(16384) __section(.pagetables.l1) = {' % self.id)

        addr = 0
        for i in range(0, 4096):
            if self.check_1m_range(addr):
                access = 0x1
                emit(fd, '\t/* 0x%08x */ (uint32_t)&pagetable_part%d_l2_0x%08x + 0x%x,' % (addr, self.id, addr, access))
            else:
                emit(fd, '\t/* 0x%08x */ 0,' % addr)
            addr += 0x100000

        emit(fd, '};')
        emit(fd)



# <shm> entry
class Shm:
    # constructor
    def __init__(self, id):
        self.id = id

        self.name = ''
        self.mem = None
        self.start_addr = 0
        self.size = 0
        self.r = True
        self.w = False
        self.x = False
        self.cache = 0

    # parse XML <shm>
    def parse(self, xml):
        self.name = get_str(xml, 'name')
        if self.name in shm_by_name:
            die('shm named "%s" already exist' % self.name)

        mem_name = get_str(xml, 'mem')
        if mem_name not in mem_by_name:
            die('shm "%s": mem "%s" not found in hardware.xml' % (self.name, mem_name))
        self.mem = mem_by_name[mem_name]

        self.size = get_num(xml, 'size')
        # for dummy config move start_addr after mem address
        self.start_addr = self.mem.start + self.mem.size

        # inherit rwx + cache from memory pool
        self.r = get_bool_opt(xml, 'read', self.mem.r)
        if self.r and not self.mem.r:
            die('shm "%s": mem "%s" does not allow read="1"' % (self.name, mem_name))
        self.w = get_bool_opt(xml, 'write', self.mem.w)
        if self.w and not self.mem.w:
            die('shm "%s": mem "%s" does not allow write="1"' % (self.name, mem_name))
        self.x = get_bool_opt(xml, 'exec', self.mem.x)
        if self.x and not self.mem.x:
            die('shm "%s": mem "%s" does not allow exec="1"' % (self.name, mem_name))
        self.cache = self.mem.cache

    # parse all <shm>
    @staticmethod
    def parse_all(xml):
        global shm_num
        for node in xml.iter('shm'):
            obj = Shm(shm_num)
            obj.parse(node)
            shm_all.append(obj)
            shm_by_name[obj.name] = obj
            shm_num += 1

    # emit single <shm> to memrqs.xml
    def gen_memrqs_xml(self, fd):
        emit(fd, '\t<shm name="%s" mem="%s" size_min="0x%x" align="0x%x" read="%d" write="%d" exec="%d"/>' % (self.name, self.mem.name, self.size, self.mem.align, self.r, self.w, self.x))

    # emit all <shm> to memrqs.xml
    @staticmethod
    def gen_memrqs_xml_all(fd):
        for s in shm_all:
            s.gen_memrqs_xml(fd)

    # perform allocation
    def allocate(self):
        self.start_addr = self.mem.allocate(self.size)

    # perform allocations for all Shms
    @staticmethod
    def allocate_all():
        for s in shm_all:
            s.allocate()


# parse <files> in  <partition> or <kernel>
class Files:
    # constructor
    def __init__(self, part):
        self.part = part

        self.dummy_h = ""
        self.dummy_elf = ""
        self.final_h = ""
        self.final_elf = ""
        self.config_h = ""
        self.bin = ""

    # parse XML <files>
    def parse(self, xml):
        if xml is None:
            die('partition "%s": no <files> found' % self.part.name)

        self.dummy_h   = get_str(xml, 'dummy_h',   'partition "%s":' % self.part.name)
        self.dummy_elf = get_str(xml, 'dummy_elf', 'partition "%s":' % self.part.name)
        self.final_h   = get_str(xml, 'final_h',   'partition "%s":' % self.part.name)
        self.final_elf = get_str(xml, 'final_elf', 'partition "%s":' % self.part.name)
        self.config_h  = get_str(xml, 'config_h',  'partition "%s":' % self.part.name)
        self.bin       = get_str(xml, 'bin',       'partition "%s":' % self.part.name)


# parse <memrq> in given partition
class Memrq:
    # constructor
    def __init__(self, part, id, global_id):
        self.part = part
        self.id = id
        self.global_id = global_id

        self.name = ''
        self.mem = None
        self.start_sym = ''
        self.start_addr = 0
        self.end_sym = ''
        self.end_addr = 0
        self.size = 0
        self.r = False
        self.w = False
        self.x = False
        self.cache = 0

    # parse XML <memrq>
    def parse(self, xml):
        self.name = get_str(xml, 'name', 'partition "%s"' % self.part.name)
        mem_name = get_str(xml, 'mem', 'partition "%s": memrq "%s"' % (self.part.name, self.name))
        if mem_name not in mem_by_name:
            die('partition "%s": memrq "%s": mem "%s" not found in hardware.xml' % (self.part.name, self.name, mem_name))
        mem = mem_by_name[mem_name]
        self.mem = mem

        self.start_sym = get_str(xml, 'start', 'partition "%s": memrq "%s"' % (self.part.name, self.name))
        self.start_addr = self.mem.start
        self.end_sym = get_str(xml, 'end', 'partition "%s": memrq "%s"' % (self.part.name, self.name))
        self.end_addr = self.mem.start

        # inherit rwx + cache from memory pool
        self.r = get_bool_opt(xml, 'read', mem.r)
        if self.r and not mem.r:
            die('partition "%s": memrq "%s": mem "%s" does not allow read="1"' % (self.part.name, self.name, mem_name))
        self.w = get_bool_opt(xml, 'write', mem.w)
        if self.w and not mem.w:
            die('partition "%s": memrq "%s": mem "%s" does not allow write="1"' % (self.part.name, self.name, mem_name))
        self.x = get_bool_opt(xml, 'exec', mem.x)
        if self.x and not mem.x:
            die('partition "%s": memrq "%s": mem "%s" does not allow exec="1"' % (self.part.name, self.name, mem_name))
        self.cache = mem.cache

    # resolve addresses from ELF file
    def resolve(self):
        if self.start_sym not in self.part.syms:
            die('partition "%s": memrq "%s": start symbol "%s" not found in ELF file' % (self.part.name, self.start_sym, self.start_sym))
        if self.end_sym not in self.part.syms:
            die('partition "%s": memrq "%s": end symbol "%s" not found in ELF file' % (self.part.name, self.end_sym, self.end_sym))

        self.start_addr = self.part.syms[self.start_sym]
        self.end_addr = self.part.syms[self.end_sym]
        self.size = self.end_addr - self.start_addr

    # emit single <memrq> to memrqs.xml
    def gen_memrqs_xml(self, fd):
        emit(fd, '\t\t<mem name="%s" mem="%s" size_min="0x%x" align="0x%x" read="%d" write="%d" exec="%d"/>' % (self.name, self.mem.name, self.size, self.mem.align, self.r, self.w, self.x))

    # emit defines for single <memrq> to linker_h
    def gen_linker_h(self, fd):
        emit(fd, '#define CFG_MEMRQ_ADDR_%s 0x%x /* mem "%s" */' % (self.name, self.start_addr, self.mem.name))

    # emit defines for single <memrq> to config_h
    def gen_config_h(self, fd):
        # nothing (requires relocation)
        pass

    # generate memrq config for config_c for all partitions
    def gen_config_c_memrq_cfg(self, fd):
        emit(fd, '\t{ /* %d: partition "%s" memrq "%s" */' % (self.global_id, self.part.name, self.name))
        emit(fd, '\t\t.addr = 0x%x,' % self.start_addr)
        emit(fd, '\t\t.size = 0x%x,' % self.size)
        emit(fd, '\t\t.access = 0%s%s%s,' % (
                                             ' | MEMRQ_READ'  if self.r else '',
                                             ' | MEMRQ_WRITE' if self.w else '',
                                             ' | MEMRQ_EXEC'  if self.x else ''))
        emit(fd, '\t\t.cache = 0x%x,' % self.cache)
        emit(fd, '\t},')

    # perform allocation
    def allocate(self):
        # memrqs smaller than a page can cross a page boundary
        page_offset = self.start_addr & (0x1000 - 1)
        self.start_addr = self.mem.allocate(self.size + page_offset)

    # generate a mapping from a <memrq>
    def get_mapping(self, is_kernel):
        return Mapping(self.name, self.start_addr, self.size, self.r, self.w, self.x, self.cache, is_kernel)


# parse <io_map> in given partition
class Map_io:
    # constructor
    def __init__(self, part, id, global_id):
        self.part = part
        self.id = id
        self.global_id = global_id

        self.name = ''
        self.io = None
        self.r = False
        self.w = False
        self.x = False
        self.cache = 0

    # parse XML <io_map>
    def parse(self, xml):
        self.name = get_str(xml, 'name', 'partition "%s"' % self.part.name)
        io_name = get_str(xml, 'io', 'partition "%s": io_map "%s"' % (self.part.name, self.name))
        if io_name not in io_by_name:
            die('partition "%s": io_map "%s": io "%s" not found in hardware.xml' % (self.part.name, self.name, io_name))
        io = io_by_name[io_name]
        self.io = io

        # inherit rwx + cache from io
        self.r = get_bool_opt(xml, 'read', io.r)
        if self.r and not io.r:
            die('partition "%s": io_map "%s": io "%s" does not allow read="1"' % (self.part.name, self.name, io_name))
        self.w = get_bool_opt(xml, 'write', io.w)
        if self.w and not io.w:
            die('partition "%s": io_map "%s": io "%s" does not allow write="1"' % (self.part.name, self.name, io_name))
        self.x = get_bool_opt(xml, 'exec', io.x)
        if self.x and not io.x:
            die('partition "%s": io_map "%s": io "%s" does not allow exec="1"' % (self.part.name, self.name, io_name))
        self.cache = io.cache

    # emit single <io> to memrqs.xml
    def gen_memrqs_xml(self, fd):
        emit(fd, '\t\t<io name="%s" io="%s" read="%d" write="%d" exec="%d"/>' % (self.name, self.io.name, self.r, self.w, self.x))

    # emit defines for single <io_map> to linker_h
    def gen_linker_h(self, fd):
        # nothing (no relocation for I/O)
        pass

    # emit defines for single <io_map> to config_h
    def gen_config_h(self, fd):
        emit(fd, '#define CFG_IO_ADDR_%s 0x%x /* io "%s" */' % (self.name, self.io.start, self.io.name))
        emit(fd, '#define CFG_IO_SIZE_%s 0x%x /* io "%s" */' % (self.name, self.io.size, self.io.name))
        emit(fd)

    # generate a mapping from an <io_map>
    def get_mapping(self, is_kernel):
        return Mapping(self.name, self.io.start, self.io.size, self.r, self.w, self.x, self.cache, is_kernel)


# parse <shm_map> in given partition
class Map_shm:
    # constructor
    def __init__(self, part, id, global_id):
        self.part = part
        self.id = id
        self.global_id = global_id

        self.name = ''
        self.shm = None
        self.r = False
        self.w = False
        self.x = False
        self.cache = 0

    # parse XML <shm_map>
    def parse(self, xml):
        self.name = get_str(xml, 'name', 'partition "%s"' % self.part.name)
        shm_name = get_str(xml, 'shm', 'partition "%s": shm_map "%s"' % (self.part.name, self.name))
        if shm_name not in shm_by_name:
            die('partition "%s": shm_map "%s": shm "%s" not found in hardware.xml' % (self.part.name, self.name, shm_name))
        shm = shm_by_name[shm_name]
        self.shm = shm

        # inherit rwx + cache from shm
        self.r = get_bool_opt(xml, 'read', shm.r)
        if self.r and not shm.r:
            die('partition "%s": shm_map "%s": shm "%s" does not allow read="1"' % (self.part.name, self.name, shm_name))
        self.w = get_bool_opt(xml, 'write', shm.w)
        if self.w and not shm.w:
            die('partition "%s": shm_map "%s": shm "%s" does not allow write="1"' % (self.part.name, self.name, shm_name))
        self.x = get_bool_opt(xml, 'exec', shm.x)
        if self.x and not shm.x:
            die('partition "%s": shm_map "%s": shm "%s" does not allow exec="1"' % (self.part.name, self.name, shm_name))
        self.cache = shm.cache

    # emit single <shm> to memrqs.xml
    def gen_memrqs_xml(self, fd):
        emit(fd, '\t\t<shm name="%s" shm="%s" read="%d" write="%d" exec="%d"/>' % (self.name, self.shm.name, self.r, self.w, self.x))

    # emit defines for single <shm_map> to linker_h
    def gen_linker_h(self, fd):
        emit(fd, '#define CFG_SHM_ADDR_%s 0x%x /* shm "%s" */' % (self.name, self.shm.start_addr, self.shm.name))
        emit(fd, '#define CFG_SHM_SIZE_%s 0x%x /* shm "%s" */' % (self.name, self.shm.size, self.shm.name))
        emit(fd)

    # emit defines for single <shm_map> to config_h
    def gen_config_h(self, fd):
        emit(fd, '#define CFG_SHM_SIZE_%s 0x%x /* shm "%s" */' % (self.name, self.shm.size, self.shm.name))
        emit(fd)

    # generate a mapping from a Shm
    def get_mapping(self, is_kernel):
        return Mapping(self.name, self.shm.start_addr, self.shm.size, self.r, self.w, self.x, self.cache, is_kernel)


# parse <irq> in given partition
class Irq:
    # constructor
    def __init__(self, part, id, global_id):
        self.part = part
        self.id = id
        self.global_id = global_id

        self.irq_id = -1

    # parse XML <irq>
    def parse(self, xml):
        self.irq_id = get_num(xml, 'id', 'partition "%s"' % (self.part.name))
        if self.irq_id in irq_by_id:
            die('partition "%s": irq "%d": already assigned' % (self.part.name, self.irq_id))
        if (self.irq_id < 0) or (self.irq_id >= hw_irq_num):
            die('partition "%s": irq "%d": out of bounds 0..%d' % (self.part.name, self.irq_id, hw_irq_num-1))

    # emit single <irq> to memrqs.xml
    def gen_memrqs_xml(self, fd):
        # nothing
        pass

    # emit defines for single <irq> to linker_h
    def gen_linker_h(self, fd):
        # nothing
        pass

    # emit defines for single <irq> to config_h
    def gen_config_h(self, fd):
        # nothing
        pass

    # generate IRQ config for config_c for all partitions
    def gen_config_c_irq_cfg(self, fd):
        emit(fd, '\t{ /* %d: partition "%s" IRQ "%d" */' % (self.global_id, self.part.name, self.id))
        #emit(fd, '\t\t.irq = &irq_dyn[%d],' % self.global_id)
        emit(fd, '\t\t.irq_id = %d,' % self.irq_id)
        emit(fd, '\t},')

    # generate IRQ table
    @staticmethod
    def gen_config_c_irq_table(fd):
        for i in range(0, hw_irq_num):
            if i in irq_by_id:
                irq = irq_by_id[i]
                emit(fd, '\t/* %3d */ &irq_dyn[%d], /* part "%s" */' % (i, irq.global_id, irq.part.name))
            else:
                emit(fd, '\t/* %3d */ NULL,' % i)


# parse <event_wait> in given partition
class Event_wait:
    # constructor
    def __init__(self, part, id, global_id):
        self.part = part
        self.id = id
        self.global_id = global_id
        self.name = ''

    # parse XML <event_wait>
    def parse(self, xml):
        self.name = get_str(xml, 'name', 'partition "%s"' % (self.part.name))
        if self.name in self.part.event_wait_by_name:
            die('event_wait named "%s" already exist' % self.name)

    # emit defines for single <event_wait> to config_h
    def gen_config_h(self, fd):
        emit(fd, '#define CFG_EVENT_WAIT_%s %d' % (self.name, self.id))


# parse <event_send> in given partition
class Event_send:
    # constructor
    def __init__(self, part, id, global_id):
        self.part = part
        self.id = id
        self.global_id = global_id
        self.name = ''

        self.to_name = ''
        self.to = None
        self.to_part_name = ''
        self.to_part = None

    # parse XML <event_send>
    def parse(self, xml):
        self.name = get_str(xml, 'name', 'partition "%s"' % (self.part.name))
        if self.name in self.part.event_send_by_name:
            die('event_send named "%s" already exist' % self.name)
        self.to_name = get_str(xml, 'to', 'partition "%s"' % (self.part.name))
        self.to_part_name = get_str_opt(xml, 'part', self.part.name)

    # resolve <event_send> to <event_wait>, probably in other partition
    def link_event_send_to_wait(self):
        if self.to_part_name not in part_by_name:
            die('partition "%s": event_send "%s": target partition "%s" not found' % (self.part.name, self.name, self.to_part_name))
        self.to_part = part_by_name[self.to_part_name]
        if self.to_name not in self.to_part.event_wait_by_name:
            die('partition "%s": event_send "%s": to event_wait "%s" not found in partition "%s"' % (self.part.name, self.name, self.to_name, self.to_part_name))
        self.to = self.to_part.event_wait_by_name[self.to_name]

    # emit defines for single <event_send> to config_h
    def gen_config_h(self, fd):
        emit(fd, '#define CFG_EVENT_SEND_%s %d' % (self.name, self.id))

    # generate event_send config for config_c for all partitions
    def gen_config_c_event_send_cfg(self, fd):
        emit(fd, '\t{ /* %d: partition "%s" event_send %d "%s" */' % (self.global_id, self.part.name, self.id, self.name))
        emit(fd, '\t\t.event_wait = &event_wait_dyn[%d], /* to partition "%s" event_wait %d "%s" */' % (self.to.global_id, self.to.part.name, self.to.id, self.to.name))
        emit(fd, '\t\t.target_cpu = %d,' % (self.to.part.first_cpu))
        emit(fd, '\t},')

# parse <partition> or <kernel>
class Partition:
    # constructor
    def __init__(self, id, is_kernel):
        # data elements
        self.id = id
        self.name = ""
        self.is_kernel = is_kernel
        self.cpu_mask = (0x1 << cpu_num) - 1
        self.first_cpu = 0
        self.max_prio = 255
        self.thread_num = 0
        self.initial_state = 'PART_STATE_RUNNING'
        self.perm = '0'

        self.files = None
        self.syms = {}
        self.sym_sizes = {}

        self.memrq_first = memrq_num
        self.memrq_num = 0
        self.memrq_all = []

        self.io_map_first = io_map_num
        self.io_map_num = 0
        self.io_map_all = []

        self.shm_map_first = shm_map_num
        self.shm_map_num = 0
        self.shm_map_all = []

        self.thread_first = thread_num
        self.thread_num = 0

        self.irq_first = irq_num
        self.irq_num = 0
        self.irq_all = []

        self.mapping_all = []

        self.entry_sym = ''
        self.entry_addr = 0
        self.stack_sym = ''
        self.stack_addr = 0
        self.stack_size = 0
        self.heap_size = 0

        self.event_wait_first = event_wait_num
        self.event_wait_all = []
        self.event_wait_by_name = {}
        self.event_wait_num = 0

        self.event_send_first = event_send_num
        self.event_send_all = []
        self.event_send_by_name = {}
        self.event_send_num = 0

    # parse XML <files>
    def parse(self, xml):
        global thread_num
        if self.is_kernel:
            self.name = '__KERNEL__'
        else:
            self.name = get_str(xml, 'name')
            if self.name in part_by_name:
                die('partition named "%s" already exist' % self.name)

        if self.is_kernel:

            # we let the idle partition use all CPUs
            self.cpu_mask = (0x1 << cpu_num) - 1

            # allocate an idle thread for each CPU
            self.thread_num += cpu_num
            thread_num += cpu_num

            self.entry_sym = 'kernel_idle'
            self.stack_sym = ''
        else:
            self.cpu_mask = get_num_opt(xml, 'cpu_mask', self.cpu_mask)
            self.cpu_mask &= (0x1 << cpu_num) - 1
            if self.cpu_mask == 0:
                die('partition "%s" has no valid CPUs in cpu_mask' % self.name)
            # use first CPU in cpu_mask by default (extract lowest bit)
            self.first_cpu = (self.cpu_mask & -self.cpu_mask).bit_length()-1
            # FIXME: artificial limitation for AMP model:
            # do not allow more than one CPU per partition!
            if (0x1 << self.first_cpu) != self.cpu_mask:
                die('partition "%s" has more than one CPU assigned' % self.name)
            initial_state_str = get_str_opt(xml, 'state', 'IDLE')
            if initial_state_str.upper() == 'IDLE':
                self.initial_state = 'PART_STATE_IDLE';
            elif initial_state_str.upper() == 'RUNNING':
                self.initial_state = 'PART_STATE_RUNNING';
            else:
                die('partition "%s" has invalid state "%s"' % (self.name, initial_state_str))
            perm = get_str_opt(xml, 'perm', '')
            if perm != '':
                self.perm = perm
            self.max_prio = get_num_opt(xml, 'max_prio', self.max_prio)
            self.thread_num = get_num_opt(xml, 'threads', self.thread_num)
            thread_num += self.thread_num
            if self.thread_num == 0:
                die('partition "%s": no threads configured' % self.name)

            self.entry_sym = get_str(xml, 'entry', 'partition "%s"' % self.name)
            self.stack_sym = get_str(xml, 'stack', self.stack_sym)
            self.heap_size = get_num_opt(xml, 'heap_size', self.heap_size)


        self.files = Files(self)
        self.files.parse(xml.find('files'))

        # parse all <memrq>
        for node in xml.iter('memrq'):
            global memrq_num
            obj = Memrq(self, self.memrq_num, memrq_num)
            obj.parse(node)
            self.memrq_all.append(obj)
            self.memrq_num += 1
            memrq_num += 1

        # parse all <io_map>
        for node in xml.iter('io_map'):
            global io_map_num
            obj = Map_io(self, self.io_map_num, io_map_num)
            obj.parse(node)
            self.io_map_all.append(obj)
            self.io_map_num += 1
            io_map_num += 1

        # parse all <shm_map>
        for node in xml.iter('shm_map'):
            global shm_map_num
            obj = Map_shm(self, self.shm_map_num, shm_map_num)
            obj.parse(node)
            self.shm_map_all.append(obj)
            self.shm_map_num += 1
            shm_map_num += 1

        # parse all <irq>
        for node in xml.iter('irq'):
            global irq_num
            obj = Irq(self, self.irq_num, irq_num)
            obj.parse(node)
            self.irq_all.append(obj)
            self.irq_num += 1
            irq_num += 1
            irq_by_id[obj.irq_id] = obj

        # parse all <event_wait>
        for node in xml.iter('event_wait'):
            global event_wait_num
            obj = Event_wait(self, self.event_wait_num, event_wait_num)
            obj.parse(node)
            self.event_wait_all.append(obj)
            self.event_wait_by_name[obj.name] = obj
            self.event_wait_num += 1
            event_wait_num += 1

        # parse all <event_send>
        for node in xml.iter('event_send'):
            global event_send_num
            obj = Event_send(self, self.event_send_num, event_send_num)
            obj.parse(node)
            self.event_send_all.append(obj)
            self.event_send_by_name[obj.name] = obj
            self.event_send_num += 1
            event_send_num += 1


    # parse all <kernel>
    @staticmethod
    def parse_kernel(xml):
        global part_num
        for node in xml.iter('kernel'):
            obj = Partition(part_num, True)
            obj.parse(node)
            part_all.append(obj)
            part_by_name[obj.name] = obj
            part_num += 1
        if part_num == 0:
            die('no kernel found')
        if part_num > 1:
            die('multiple kernel entries found')

    # parse all <partition>
    @staticmethod
    def parse_all(xml):
        global part_num
        for node in xml.iter('partition'):
            obj = Partition(part_num, False)
            obj.parse(node)
            part_all.append(obj)
            part_by_name[obj.name] = obj
            part_num += 1

        # link event sender to event waiter
        for p in part_all:
            for e in p.event_send_all:
                e.link_event_send_to_wait()


    # resolve addresses from ELF file
    def resolve(self):
        # we only have to resolve the memrqs here
        for m in self.memrq_all:
            m.resolve()

        # resolve entry point and initial stack
        if not self.is_kernel:
            if self.entry_sym not in self.syms:
                die('partition "%s": entry symbol "%s" not found in ELF file' % (self.name, self.entry_sym))
            self.entry_addr = self.syms[self.entry_sym]

            if self.stack_sym not in self.syms:
                die('partition "%s": stack symbol "%s" not found in ELF file' % (self.name, self.stack_sym))
            self.stack_addr = self.syms[self.stack_sym]
            if self.stack_sym not in self.sym_sizes:
                die('partition "%s": stack symbol "%s" in ELF file has no size' % (self.name, self.stack_sym))
            self.stack_size = self.sym_sizes[self.stack_sym]


    # resolve addresses from ELF files for all partitions
    @staticmethod
    def resolve_all(final):
        for p in part_all:
            if final:
                elf = p.files.final_elf
            else:
                elf = p.files.dummy_elf
            p.syms = get_syms(elf, p.sym_sizes, nm_tool)
            p.resolve()

    # emit single <shm> to memrqs.xml
    def gen_memrqs_xml(self, fd):
        emit(fd, '\t<part name="%s" is_kernel="%d">' % (self.name, self.is_kernel))
        for m in self.memrq_all:
            m.gen_memrqs_xml(fd)
        for m in self.io_map_all:
            m.gen_memrqs_xml(fd)
        for m in self.shm_map_all:
            m.gen_memrqs_xml(fd)
        emit(fd, '\t</part>')

    # emit all <shm> to memrqs.xml
    @staticmethod
    def gen_memrqs_xml_all(fd):
        for p in part_all:
            p.gen_memrqs_xml(fd)


    # generate linker_h file for given partition
    def gen_linker_h(self, fd):
        emit(fd, '/* partition "%s" is_kernel="%d" */' % (self.name, self.is_kernel))
        emit(fd, '#define CFG_THREADS %d' % self.thread_num)
        emit(fd, '#define CFG_HEAP_SIZE 0x%x' % self.heap_size)
        emit(fd)

        for m in self.memrq_all:
            m.gen_linker_h(fd)
        for m in self.io_map_all:
            m.gen_linker_h(fd)
        for m in self.shm_map_all:
            m.gen_linker_h(fd)

    # generate config_h file for given partition
    def gen_config_h(self, fd):
        emit(fd, '/* partition "%s" is_kernel="%d" */' % (self.name, self.is_kernel))
        emit(fd, '#define CFG_THREADS %d' % self.thread_num)
        emit(fd, '#define CFG_HEAP_SIZE 0x%x' % self.heap_size)
        emit(fd, '#define CFG_MAX_PRIO %d' % self.max_prio)
        emit(fd, '#define CFG_CPU_MASK 0x%x' % self.cpu_mask)
        emit(fd, '#define CFG_FIRST_CPU %d' % self.first_cpu)
        emit(fd)

        for e in self.event_wait_all:
            e.gen_config_h(fd)
        emit(fd, '#define CFG_EVENT_WAITS %d' % self.event_wait_num)
        emit(fd)

        for e in self.event_send_all:
            e.gen_config_h(fd)
        emit(fd, '#define CFG_EVENT_SENDS %d' % self.event_send_num)
        emit(fd)

        for m in self.memrq_all:
            m.gen_config_h(fd)
        for m in self.io_map_all:
            m.gen_config_h(fd)
        for m in self.shm_map_all:
            m.gen_config_h(fd)

    # generate partition config for config_c for given partition
    def gen_config_c_part_cfg(self, fd):
        emit(fd, '\t{ /* partition "%s" is_kernel="%d" */' % (self.name, self.is_kernel))
        emit(fd, '\t\t.id = %d,' % self.id)
        emit(fd, '\t\t.name = "%s",' % self.name)
        emit(fd, '\t\t.part = &part_dyn[%d],' % self.id)

        emit(fd, '\t\t.cpu_mask = 0x%x,' % self.cpu_mask)
        emit(fd, '\t\t.first_cpu = %d,' % self.first_cpu)
        emit(fd, '\t\t.max_prio = %d,' % self.max_prio)
        emit(fd, '\t\t.initial_state = %s,' % self.initial_state)
        emit(fd, '\t\t.perm = %s,' % self.perm)

        emit(fd, '\t\t.memrq_num = %d,' % self.memrq_num)
        emit(fd, '\t\t.memrq_cfg = &memrq_cfg[%d],' % self.memrq_first)

        emit(fd, '\t\t.thread_num = %d,' % self.thread_num)
        emit(fd, '\t\t.thread = &thread_dyn[%d],' % self.thread_first)

        emit(fd, '\t\t.irq_num = %d,' % self.irq_num)
        emit(fd, '\t\t.irq_cfg = &irq_cfg[%d],' % self.irq_first)

        emit(fd, '\t\t.entry = 0x%x, /* %s */' % (self.entry_addr, self.entry_sym))
        emit(fd, '\t\t.stack_base = 0x%x, /* %s */' % (self.stack_addr, self.stack_sym))
        emit(fd, '\t\t.stack_size = 0x%x,' % self.stack_size)

        emit(fd, '\t\t.event_wait_num = %d,' % self.event_wait_num)
        emit(fd, '\t\t.event_wait = &event_wait_dyn[%d],' % self.event_wait_first)

        emit(fd, '\t\t.event_send_num = %d,' % self.event_send_num)
        emit(fd, '\t\t.event_send_cfg = &event_send_cfg[%d],' % self.event_send_first)

        emit(fd, '\t},')


    # generate IRQ config for config_c for given partitions
    def gen_config_c_irq_cfg(self, fd):
        for i in self.irq_all:
            i.gen_config_c_irq_cfg(fd)

    # generate event_send config for config_c for given partitions
    def gen_config_c_event_send_cfg(self, fd):
        for e in self.event_send_all:
            e.gen_config_c_event_send_cfg(fd)

    # generate memrq config for config_c for given partitions
    def gen_config_c_memrq_cfg(self, fd):
        for m in self.memrq_all:
            m.gen_config_c_memrq_cfg(fd)

    # perform allocations for given partition
    def allocate(self):
        for m in self.memrq_all:
            m.allocate()

    # perform allocations for all Partitions
    @staticmethod
    def allocate_all():
        for p in part_all:
            p.allocate()

    # generate MMU config for given partition
    def gen_adspace(self, fd):
        # get all mappings in the partition
        for m in self.memrq_all:
            self.mapping_all.append(m.get_mapping(self.is_kernel))
        for m in self.io_map_all:
            self.mapping_all.append(m.get_mapping(self.is_kernel))
        for m in self.shm_map_all:
            self.mapping_all.append(m.get_mapping(self.is_kernel))

        # also add kernel mappings
        if not self.is_kernel:
            self.mapping_all.extend(part_all[0].mapping_all)

        # order them by: primary: start address, secondary: user mappings first
        # this makes sure that the generator emits the user entries before the kernel entries
        self.mapping_all.sort(key=lambda mapping: (mapping.start_addr, mapping.is_kernel))

        # dump all mappings
        #print("#### %s" % self.name)
        #for m in self.mapping_all:
        #    print("  %s: addr: %x, size: %x, is_kernel: %d" % (m.name, m.start_addr, m.size, m.is_kernel))

        # generate page tables
        pt = Pagetable(self.id, self.mapping_all)
        pt.emit_pagetable(fd)


    # generate MMU config for all Partitions
    @staticmethod
    def gen_adspace_all(fd):
        global is_arm_cortex_a8
        # first generate all page tables
        for p in part_all:
            p.gen_adspace(fd)

        # then provide a global config table
        emit(fd, '/* partition MMU configuration */')
        emit(fd, 'const struct arch_adspace_cfg adspace_cfg[%d] = {' % part_num)
        for p in part_all:
            emit(fd, '\t{ /* partition "%s" is_kernel="%d" */' % (p.name, p.is_kernel))
            emit(fd, '\t\t.asid = %d,' % p.id)
            if is_arm_cortex_a8:
                emit(fd, '\t\t.ttbr0 = (uint32_t)&pagetable_part%d_l1 + 0x59, /* inner: WB, outer: WB */' % p.id)
            else:
                emit(fd, '\t\t.ttbr0 = (uint32_t)&pagetable_part%d_l1 + 0x6a, /* inner: WA + S, outer: WA + NOS */' % p.id)
            emit(fd, '\t},')
        emit(fd, '};')
        emit(fd)


# parse config.xml
def parse_config_xml(filename):
    try:
        tree = ET.parse(filename)
    except ET.ParseError as e:
        die('"%s":' % filename, e)

    xml = tree.getroot()
    if xml is None:
        die('"%s": no root element' % filename)
    if xml.tag != "config":
        die('"%s": must start with <config> tag' % filename)

    Shm.parse_all(xml)
    Partition.parse_all(xml)


# --- generators ---

# generate memrqs.xml
def gen_memrqs(filename):
    try:
        fd = open(filename, 'w')

        emit(fd, '<!-- generated on:', time.strftime("%Y-%m-%d %H:%M:%S"), '-->')

        emit(fd, '<memrqs>')

        # dump all SHMs
        Shm.gen_memrqs_xml_all(fd)
        # dump all Partitions
        Partition.gen_memrqs_xml_all(fd)

        emit(fd, '</memrqs>')

    except OSError as e:
        die('error writing to "%s":' % filename, e)

    finally:
        fd.close()


# generate dummy_h files for all or a specific partition (given by filename)
def gen_dummy_h_all(generate_all, generate_filename):
    generated_files = 0
    for p in part_all:
        # skip file if it should not be generated
        filename = p.files.dummy_h
        if generate_all or os.path.normpath(generate_filename) == os.path.normpath(filename):
            try:
                fd = open(filename, 'w')

                emit(fd, '/* dummy_h generated on: %s */' % time.strftime("%Y-%m-%d %H:%M:%S"))
                emit(fd)

                p.gen_linker_h(fd)

            except OSError as e:
                die('error writing to "%s":' % filename, e)

            finally:
                fd.close()

            generated_files += 1

    if not generate_all and generated_files == 0:
        die('header "%s" not found in configuration' % generate_filename)

# generate final_h files for all or a specific partition (given by filename)
def gen_final_h_all(generate_all, generate_filename):
    generated_files = 0
    for p in part_all:
        # skip file if it should not be generated
        filename = p.files.final_h
        if generate_all or os.path.normpath(generate_filename) == os.path.normpath(filename):
            try:
                fd = open(filename, 'w')

                emit(fd, '/* final_h generated on: %s */' % time.strftime("%Y-%m-%d %H:%M:%S"))
                emit(fd)

                p.gen_linker_h(fd)

            except OSError as e:
                die('error writing to "%s":' % filename, e)

            finally:
                fd.close()

            generated_files += 1

    if not generate_all and generated_files == 0:
        die('header "%s" not found in configuration' % generate_filename)


# generate config_h files for all or a specific partition (given by filename)
def gen_config_h_all(generate_all, generate_filename):
    generated_files = 0
    for p in part_all:
        # skip file if it should not be generated
        filename = p.files.config_h
        if generate_all or os.path.normpath(generate_filename) == os.path.normpath(filename):
            try:
                fd = open(filename, 'w')

                emit(fd, '/* config_h generated on: %s */' % time.strftime("%Y-%m-%d %H:%M:%S"))
                emit(fd)

                p.gen_config_h(fd)

            except OSError as e:
                die('error writing to "%s":' % filename, e)

            finally:
                fd.close()

            generated_files += 1

    if not generate_all and generated_files == 0:
        die('header "%s" not found in configuration' % generate_filename)


# generate config_c file with full kernel configuration
def gen_config_c(filename, final):
    try:
        fd = open(filename, 'w')

        emit(fd, '/* config_c generated on: %s */' % time.strftime("%Y-%m-%d %H:%M:%S"))
        if final:
            emit(fd, '/* final configuration with resolved addresses */')
        else:
            emit(fd, '/* dummy configuration for first run */')
        emit(fd)

        emit(fd, '#include <marron/types.h>')
        emit(fd, '#include <part_types.h>')
        emit(fd, '#include <thread_types.h>')
        emit(fd, '#include <memrq_types.h>')
        emit(fd, '#include <sched_types.h>')
        emit(fd, '#include <irq_types.h>')
        emit(fd, '#include <percpu_types.h>')
        emit(fd, '#include <arch_adspace_types.h>')
        emit(fd, '#include <event_types.h>')
        emit(fd, '#include <smp.h>')
        emit(fd)

        emit(fd, '/* forward declarations */')
        emit(fd, 'extern const struct part_cfg part_cfg[%d];' % part_num)
        emit(fd, 'extern const struct memrq_cfg memrq_cfg[%d];' % memrq_num)
        emit(fd, 'extern const struct percpu_cfg percpu_cfg[%d];' % cpu_num)
        emit(fd)

        emit(fd, '/* limits */')
        emit(fd, 'const uint8_t thread_num = %d;' % thread_num)
        emit(fd, 'const uint8_t part_num = %d;' % part_num)
        emit(fd, 'const uint8_t memrq_num = %d;' % memrq_num)
        emit(fd, 'const uint8_t sched_num = %d;' % sched_num)
        emit(fd, 'const uint8_t cpu_num = %d;' % cpu_num)
        emit(fd, 'const uint8_t irq_num = %d;' % irq_num)
        emit(fd, 'const uint16_t irq_table_num = %d;' % hw_irq_num)
        emit(fd, 'const uint8_t event_wait_num = %d;' % event_wait_num)
        emit(fd, 'const uint8_t event_send_num = %d;' % event_send_num)
        emit(fd)

        emit(fd, '/* data allocation */')
        emit(fd, 'struct thread thread_dyn[%d];' % thread_num)
        emit(fd, 'struct regs thread_regs[%d];' % thread_num)
        emit(fd, 'struct part part_dyn[%d];' % part_num)
        emit(fd, 'struct sched sched_dyn[%d];' % sched_num)
        emit(fd, 'struct irq irq_dyn[%d];' % irq_num)
        emit(fd, 'struct event_wait event_wait_dyn[%d];' % event_wait_num)
        for i in range(0, cpu_num):
            emit(fd, 'static struct percpu percpu_dyn_cpu%d __section(.percpu.cpu%d);' % (i, i))
            emit(fd, 'static char idle_stack_cpu%d[0x%x] __aligned(16);' % (i, idle_stack_size))
        emit(fd, '#ifdef SMP')
        emit(fd, 'struct smp_call smp_call_dyn[%d];' % cpu_num)
        emit(fd, '#endif')
        emit(fd)

        emit(fd, '/* event send configuration */')
        emit(fd, 'const struct event_send_cfg event_send_cfg[%d] = {' % event_send_num)
        for p in part_all:
            p.gen_config_c_event_send_cfg(fd)
        emit(fd, '};')

        emit(fd, '/* memrq configuration */')
        emit(fd, 'const struct memrq_cfg memrq_cfg[%d] = {' % memrq_num)
        for p in part_all:
            p.gen_config_c_memrq_cfg(fd)
        emit(fd, '};')

        emit(fd, '/* IRQ configuration */')
        emit(fd, 'const struct irq_cfg irq_cfg[%d] = {' % irq_num)
        for p in part_all:
            p.gen_config_c_irq_cfg(fd)
        emit(fd, '};')
        emit(fd)

        emit(fd, '/* IRQ table */')
        emit(fd, 'struct irq * const irq_table[%d] = {' % hw_irq_num)
        Irq.gen_config_c_irq_table(fd)
        emit(fd, '};')
        emit(fd)

        emit(fd, '/* partition configuration */')
        emit(fd, 'const struct part_cfg part_cfg[%d] = {' % part_num)
        for p in part_all:
            p.gen_config_c_part_cfg(fd)
        emit(fd, '};')
        emit(fd)

        emit(fd, '/* per-CPU configuration */')
        emit(fd, 'const struct percpu_cfg percpu_cfg[%d] = {' % cpu_num)
        for i in range(0, cpu_num):
            emit(fd, '\t{')
            emit(fd, '\t\t.percpu = &percpu_dyn_cpu%d,' % i)
            emit(fd, '\t\t.idle_stack_top = &idle_stack_cpu%d[0x%x],' % (i, idle_stack_size))
            emit(fd, '\t},')
        emit(fd, '};')
        emit(fd)

        # generate page tables for all partitions (with all SHMs)
        # FIXME: the current implementation only handles linear MMU page tables
        Partition.gen_adspace_all(fd)


    except OSError as e:
        die('error writing to "%s":' % filename, e)

    finally:
        fd.close()


# perform allocations for each partition (simple allocator)
def gen_allocate_all():
    # allocate memory for all partitions and all SHMs
    # FIXME: the current allocator allocates all memory linearly!
    Partition.allocate_all()
    Shm.allocate_all()


def read_file(filename):
    try:
        fd = open(filename, 'rb')
        blob = fd.read()
        fd.close()
        return blob

    except OSError as e:
        die('error reading from "%s":' % filename, e)


# generate final image for given <mem> section
def gen_image(filename, section):
    if section not in mem_by_name:
        die('no <mem> names "%s" found' % section)

    base = mem_by_name[section].start

    try:
        fd = open(filename, 'w+b')

    except OSError as e:
        die('error creating "%s":' % filename, e)

    try:
        for p in part_all:
            for m in p.memrq_all:
                if m.mem.name == section:
                    pos = m.start_addr - base
                    blob = read_file(p.files.bin)

                    vprint("# importing for part %s memrq %s: %s -> 0x%x / 0x%x" % (p.name, m.name, p.files.bin, pos, len(blob)))

                    fd.seek(pos)
                    fd.write(blob)

    except OSError as e:
        die('error writing to "%s":' % filename, e)

    finally:
        fd.close()


# --- main ---
if __name__ == '__main__':
    die("use specific generators")
