#!/usr/bin/env python3
# SPDX-License-Identifier: MIT
#
# gen_header.py -- Generate dummy headers from XML config
#
# azuepke, 2017-09-22: initial
# azuepke, 2017-10-06: header generator
# azuepke, 2017-10-19: first working version
# azuepke, 2017-10-23: new data model
# azuepke, 2017-10-26: factored out common code library and merged generators
#
#
# What does this tool do?
#
# This tool generates two sets of header files:
# - one header file with link-time informations, e.g. addresses and sizes
# - one header file with static partition specific information,
#    e.g. stack sizes, number of threads
#
# Detailed steps for link-time information:
# - scan hardware.xml and remember the start addresses of all memory regions
# - from config.xml, iterate all partitions and the kernel
# - generate defines from resolved start addresses of memory requirements
#
#
# example for dummy run:
# $ ./gen_header.py -hw hardware.xml -c config.xml --all
#
# example for final run:
# $ ./gen_header.py -hw hardware.xml -c config.xml --final --all

import gen_core as gen
import argparse


# --- main ---

# main
def main():
    prog = 'gen_header.py'
    gen.set_progname(prog)

    parser = argparse.ArgumentParser(prog=prog,
                                     description='Generate headers from XML config.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='increase output verbosity')
    parser.add_argument('--config', action='store_true',
                        help='config run: generate (static) config_h files (before first compilation)')
    parser.add_argument('--dummy', action='store_true',
                        help='dummy run: generate dummy_h files (before first compilation)')
    parser.add_argument('--final', action='store_true',
                        help='final run: generate final_h files (from dummy ELF files)')
    parser.add_argument('-a', '--all', action='store_true',
                        help='generate all files')
    parser.add_argument('-hw', metavar='hardware.xml', nargs=1, type=str,
                        help='hardware description (XML)')
    #parser.add_argument('-m', metavar='memrqs.xml', nargs=1, type=str,
    #                    help='memory requirements (XML)')
    parser.add_argument('-c', metavar='config.xml', nargs=1, type=str,
                        help='configuration (XML)')
    parser.add_argument('-nm', metavar='nm tool', nargs=1, type=str,
                        help='name mangling tool to use, e.g. arm-linux-gnueabihf-nm')
    parser.add_argument('header', metavar='header.h', nargs='?', type=str,
                        help='header (C)')

    args = parser.parse_args()
    gen.set_verbose(args.verbose)

    # default argument for nm
    if args.nm is not None:
        gen.set_nm_tool(args.nm[0])

    # parse hardware.xml
    if args.hw is None:
        gen.die("need hardware.xml")
    gen.parse_hardware_xml(args.hw[0])

    # parse config.xml
    if args.c is None:
        gen.die("need config.xml")
    gen.parse_config_xml(args.c[0])

    # final run or dummy run
    dummy = False
    final = False
    config = False
    if args.dummy is not None:
        dummy = args.dummy
    if args.final is not None:
        final = args.final
    if args.config is not None:
        config = args.config

    if dummy and final:
        gen.die('specify either --dummy or --final')
    if not dummy and not final and not config:
        gen.die('specify at least --dummy, --final or --config')

    # resolve addresses
    if final:
        # we resolve sizes from the dummy ELF files and allocate memory
        gen.Partition.resolve_all(False)
        gen.gen_allocate_all()
    else:
        # resolved addresses are not needed
        #gen.Partition.resolve_all(False)
        # no allocation run needed
        pass

    # either --all or a specific header file must be provided
    generate_all = args.all
    if not generate_all and args.header is None:
        gen.die('need at least a header.h to generate or --all for all headers')
    if generate_all and args.header is not None:
        gen.die('either provide a header.h to generate or --all for all headers')

    if dummy:
        gen.gen_dummy_h_all(generate_all, args.header)
    if final:
        gen.gen_final_h_all(generate_all, args.header)
    if config:
        gen.gen_config_h_all(generate_all, args.header)

if __name__ == '__main__':
    main()
