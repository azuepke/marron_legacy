#!/bin/bash
function checkExit() {
	if [ $? != 0 ]; then
		echo "Error"
		exit 1
	fi
}
if [ ! -e download ]; then
	mkdir download
	checkExit
fi
which arm-linux-gnueabihf-gcc
if [ $? != 0 ]; then
	if [ ! -e download/gcc-arm-none-eabi.tar.bz2 ]; then
		echo "Download gcc"
		wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/7-2017q4/gcc-arm-none-eabi-7-2017-q4-major-linux.tar.bz2?revision=375265d4-e9b5-41c8-bf23-56cbe927e156?product=GNU%20Arm%20Embedded%20Toolchain,64-bit,,Linux,7-2017-q4-major -O download/gcc-arm-none-eabi.tar.bz2
		checkExit
	fi
	if [ ! -e gcc ]; then
		echo "Extract gcc"
		mkdir gcc
		checkExit
		tar -xf download/gcc-arm-none-eabi.tar.bz2 -C gcc/
		checkExit
		mv gcc/gcc-arm-none-eabi-*-*-*-*/* gcc
		checkExit
		rm -R gcc/gcc-arm-none-eabi-*-*-*-*
		checkExit
	fi
else
	echo "gcc version: "
	arm-linux-gnueabihf-gcc --version
fi
which qemu-system-arm
if [ $? != 0 ]; then
	if [ ! -e download/qemu-2.11.1.tar.xz ]; then
		echo "Download qemu"
		wget https://download.qemu.org/qemu-2.11.1.tar.xz -O download/qemu-2.11.1.tar.xz
		checkExit
	fi
	if [ ! -e qemu ]; then
		echo "Compile and Install qemu"
		mkdir qemu
		tar -xf download/qemu-2.11.1.tar.xz -C qemu
		checkExit
		pushd qemu/qemu-*.*.*/
		./configure --disable-vnc --disable-xen --target-list="arm-softmmu arm-linux-user" --prefix=`pwd`/..
		checkExit
		make -j5
		checkExit
		make install
		checkExit
		popd
	fi
else
	echo "qemu version: "
	qemu-system-arm --version
fi
export PATH=`pwd`/gcc/bin:qemu/bin:$PATH
echo $PATH
source BUILDENV.sh-qemu-arm
checkExit
echo "Build kernel"
AUTOBUILD=true make
checkExit
echo "Test kernel"
AUTOBUILD=true make
scripts/kill_qemu.sh & # start qemu kill script
AUTOBUILD=true make run | tee run.log
checkExit
echo "Cleanup"
AUTOBUILD=true make zap &> /dev/null
checkExit
# kill sleep process
ps -A | grep sleep
if [ $? == 0 ]; then
	echo "sleep found killing..."
	killall -KILL sleep
else
	echo "sleep is dead"
fi
tmp=`cat run.log  | grep "# Shutdown"`
checkExit
rm run.log
checkExit
