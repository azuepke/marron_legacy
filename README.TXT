# README.TXT
#
# azuepke, 2018-04-20: initial
# azuepke, 2020-01-31: update on cross-toolchains

"Marron" is a small, statically configured microkernel targetting MMU
based systems. The current implementation focusses on MMU-based 32-bit ARM
Cortex-A multicore processors, e.g. Cortex A8, A9 or A15.

The Marron kernel is a C language baseline for operating systems research by
Alex Zuepke of RheinMain University of Applied Sciences https://www.cs.hs-rm.de/
to explore SPARK Ada for teaching and research in the context of the AQUAS
(Aggregated Quality Assurance for Systems) project https://aquas-project.eu/.

Marron is licensed under MIT license, see LICENSE.TXT


Compiling
==========

For the QEMU board (running on an ARM emulator), you need to setup the build
environment, e.g. the cross compiling toolchain, and then compile the kernel
and two demo applications:

$ source BUILDENV.sh-qemu-arm
$ make

To run the example, just type:

$ make run

To re-build the project, type:

$ make distclean  # or "make zap" for short
$ make run

The system is always build with all asserts enabled. For a release build, use:

$ make distclean
$ make DEBUG=no all

Similarly, one can disable any multicore features by compiling the system with:

$ make distclean
$ make SMP=no all


Suitable Cross-Toolchain
=========================
As cross toolchains on Ubuntu, use gcc-arm-linux-gnueabihf.
Note that the Ubuntu package gcc-arm-none-eabi is unsuitable due to different
handling of enums in the ARM default ABI.
Alternatively, look for statically compiled toolchains, e.g. http://musl.cc/


System Overview
================

Marron comprises a kernel part and user space application.
The kernel part has four components:
- the generic kernel (in kernel/src/*.c)
- the architecture layer (in kernel/arch/xxx/)
- a board support package (BSP, in bsp/xxx)
- a static kernel configuration (kernel/xxx_config.c)

The kernel and the architecture layer compile to a kernel.o file, which is
then linked with the BSP object files and the configuration object file
into a kernel binary.

For the user space applications, the kernel provides a system call library
(libsys) and a small libc implementation (libcmini) which provides a printf()
implementation and basic C services, like strcmp() and memset().
All user space applications are compiled into dedicated binaries.

The system configuration is XML-based and comprises two files:
- hardware_xxx.xml describes the BSP configuration, and
- config.xml describes the user space configuration in the default project.
A Python-based code generator (in scripts/) reads the XML files and generates
C data structures for the kernel, and header files with #defines for inclusion
in the user applications and during linkage of the binaries.

Roughly, the whole build process uses the following steps to generate
a bootable image. Ssince we have a statically configured system, we need
to know at compile time _where_ to place the images in memory. Therefore,
we compile and link all components once to read their required memory demands,
then allocate memory, and finally link the components to their final addresses.
Therefore, the build process takes the following steps:
1. Build a dummy configuration for the kernel. The dummy configuration does
   not have the final addresses, but has exactly the same size as the final
   configuration, so we get the final RAM and flash size demands of the kernel.
2. Compile and link kernel + BSP + configuration into dummy_kernel.elf
3. Compile and link the applications to dummy_app.elf files
4. Read the memory demands from these ELF files, allocate memory,
   and then generate new linker files with the final addresses.
5. Alsoc create a "final configuration" for the kernel,
   based on the previous memory layout
6. Link all components to their final addresses
7. Extract the binary pieces from the final ELF files
   and assemble a bootable image

Note that we target execution from flash memory. This means that all
applications need to copy their static data from flash to RAM first.
This is automatically handled by the crt0.o startup code, which is
provided by the libcmini/__start_arm.S file.


Static System Configuration
============================

The static system configuration is kept in config.xml. A system comprises
multiple "partitions", which resemble processes in UNIX systems.
Each partition has a dedicated address space associated with it, and has
multiple threads executing in this address space. Each partition has its
"limits" statically configured, i.e. we assign each partition its resources
(or resource limitations) it can use:
- Initial state: a partition is in either RUNNING or IDLE state. A transition
  from RUNNING to IDLE state halts and resets the whole partition.
- Partition ID: each partition has a unique ID starting from 1,
  with ID=0 reserved for the kernel.
- Number of threads: Next to the initial "main" thread, a partition can create
  further threads up the configured limits.
- Maximum priority: The initial thread starts with the maximum priority,
  and all threads can change their priority up to this limit. We support
  priorities in the range from 0 .. 255.
- Assigned CPUs: The configuration allows to assign a mask of eligible CPUs
  to a partition. However, due to internal design constraints, a partition
  must only get _one_ CPU assigned.
- Permissions: currently, the system support two permissions:
  - PART_PERM_PART_OTHER enables to halt and start other partitions
  - PART_PERM_SHUTDOWN allows to shutdown the whole system
- Assigned resources: all resources are statically assigned at compile time:
  - <memrq> entries manage flash and RAM resources and reference symbols
    in the ELF file.
  - <shm_map> entries handles access to shared memory segments
  - <io_map> entries handles access to memory mapped I/O resources
  - <irq> entries enable the handling of an interrupt
  - <event_send> and <event_wait> handle communication with other partitions
    - <event_wait> is the receiving endpoint
    - <event_send> is the sending side and must be connected to a receiver


Stacks and Heaps
=================

An application is responsible for its own thread's stacks and a heap.
To define stacks, use the DEFINE_STACK(symbol, size) macro, and
to get a stack pointer for sys_thread_create(), use GET_STACK(symbol).
The heap is handled by the "heap_size" attribute of a partition,
which manages the free space between the __heap_start and __heap_end symbols
in the linker script.
