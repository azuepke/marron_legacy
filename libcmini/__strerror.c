/* SPDX-License-Identifier: MIT */
/*
 * __strerror.c
 *
 * Internal strerror().
 *
 * azuepke, 2013-09-10: initial
 * azuepke, 2018-01-04: imported
 */

#include <marron/error.h>
#include <string.h>

/** strerror() */
const char *__strerror(unsigned int err)
{
	switch (err) {
	case EOK:			return "EOK";
	case ENOSYS:		return "ENOSYS";
	case EINVAL:		return "EINVAL";
	case ELIMIT:		return "ELIMIT";
	case EFAULT:		return "EFAULT";
	case ETIMEOUT:		return "ETIMEOUT";
	case EAGAIN:		return "EAGAIN";
	case EBUSY:			return "EBUSY";
	case ESTATE:		return "ESTATE";
	case ETYPE:			return "ETYPE";
	case EPERM:			return "EPERM";
	case ECANCEL:		return "ECANCEL";

	default:			return "E???";
	}
}
