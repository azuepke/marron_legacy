/* SPDX-License-Identifier: MIT */
/*
 * raise.c
 *
 * Libc functionality
 *
 * awerner, 2020-01-30: initial
 */
#include <marron/api.h>

int raise(int sig);
int raise(int sig)
{
	sys_abort();
	(void) sig;
	return 0;
}
