/* SPDX-License-Identifier: MIT */
/*
 * test_timer_irq.c
 *
 * ARM SP804 timer driver
 * http://infocenter.arm.com/help/topic/com.arm.doc.ddi0271d/DDI0271.pdf
 *
 * The SP804 hardware has two identical programmable "Free Running Counters".
 * The timers decrement down and fire at the 1 -> 0 transition.
 * We program them in 32-bit mode as periodic timers.
 *
 * azuepke, 2013-11-20: initial
 * azuepke, 2018-03-12: repurposed timer driver as interrupt test case
 */

#include <marron/api.h>
#include <stdio.h>
#include <string.h>
#undef NDEBUG /* always keep assert() acivated */
#include <assert.h>
#include "app.h"


/* QEMU board specific address of the SP804 timer */
#define BOARD_IO1_VIRT			0x1c000000
#define SP804_TIMER_BASE		BOARD_IO1_VIRT + 0x110000
#define SP804_TIMER_CLOCK		1000000	/* 1 MHz */
#define SP804_TIMER_IRQ			34

/** Registers of the first timer. */
#define SP804_LOAD			0x000	/* load value */
#define SP804_VALUE			0x004	/* current timer value (read) */
#define SP804_CTRL			0x008	/* control register */
#define SP804_ACK			0x00c	/* interrupt clear register (write) */
#define SP804_RIS			0x010	/* raw interrupt status register */
#define SP804_MIS			0x014	/* masked interrupt status register */
#define SP804_BGLOAD		0x018	/* background load value */

/* Timer 2 starts at offset +0x20 to the first timer. */

/** Bits in control register */
#define SP804_CTRL_EN		0x80	/* enable timer */
#define SP804_CTRL_PERIODIC	0x40	/* periodic mode */
#define SP804_CTRL_INT		0x20	/* interrupt enable */
#define SP804_CTRL_PRE1		0x08	/* prescaler bit 1 */
#define SP804_CTRL_PRE0		0x04	/* prescaler bit 0 */
#define SP804_CTRL_32BIT	0x02	/* 32-bit mode */
#define SP804_CTRL_ONESHOT	0x01	/* oneshot mode */

/* prescaler bits:
 * 00: divide clock by 1
 * 01: divide clock by 16
 * 10: divide clock by 256
 */

/** Bits in ACK, RIS, MIS */
#define SP804_IRQ_BIT		0x01	/* IRQ bit */

/* IO accessors */
static inline uint32_t readl(const volatile void *addr)
{
	uint32_t ret;
	__asm__ volatile("ldr %0, %1" : "=r" (ret)
	                 : "m" (*(const volatile uint32_t *)addr) : "memory");
	return ret;
}

static inline void writel(volatile void *addr, uint32_t val)
{
	__asm__ volatile ("str %0, %1" : : "r" (val),
	                  "m" (*(volatile uint32_t *)addr) : "memory");
}

/* access to per-CPU specific registers */
static inline uint32_t sp804_read32(unsigned long reg)
{
	return readl((volatile void *)(SP804_TIMER_BASE + reg));
}

static inline void sp804_write32(unsigned long reg, uint32_t val)
{
	writel((volatile void *)(SP804_TIMER_BASE + reg), val);
}

static void timer_tester(unsigned int freq)
{
	unsigned int reload;
	unsigned int err;
	unsigned int i;

	reload = SP804_TIMER_CLOCK / freq;

	/* disable and initialize the timer, but do not start it yet */
	sp804_write32(SP804_CTRL, SP804_CTRL_32BIT);
	sp804_write32(SP804_LOAD, reload);

	/* enable timer in 32-bit periodic mode with interrupts */
	sp804_write32(SP804_CTRL, SP804_CTRL_EN | SP804_CTRL_PERIODIC |
	                          SP804_CTRL_INT | SP804_CTRL_32BIT);

	printf("# IRQ enable + wait for: ");

	/* enable timer interrupt */
	sp804_write32(SP804_MIS, SP804_IRQ_BIT);
	err = sys_irq_enable(SP804_TIMER_IRQ, 0);
	if (err != EOK) printf("sys_irq_enable: %s\n", __strerror(err));
	assert(err == EOK);

	/* wait for timer interrupt (unmasks it) */
	for (i = 0; i < 10; i++) {
		err = sys_irq_wait(SP804_TIMER_IRQ, 0, TIMEOUT_INFINITE);
		if (err != EOK) printf("sys_irq_wait: %s\n", __strerror(err));
		assert(err == EOK);

		printf(".");
		sp804_write32(SP804_ACK, SP804_IRQ_BIT);
	}

	/* and we are done ... */
	err = sys_irq_disable(SP804_TIMER_IRQ);
	if (err != EOK) printf("sys_irq_disable: %s\n", __strerror(err));
	assert(err == EOK);

	/* enable timer in 32-bit periodic mode with interrupts */
	sp804_write32(SP804_CTRL, 0);

	printf("\n");
}

void test_timer_irq(void)
{
	char bsp_name[32];
	err_t err;

	print_header("IRQ-based Timer");

	err = sys_bsp_name(bsp_name, sizeof(bsp_name));
	assert(err == EOK);
	if (strcmp(bsp_name, "qemu-arm") != 0) {
		printf("# timer not supported on %s, skipping test\n", bsp_name);
		return;
	}

	printf("# testing 1 Hz, 10 timer interrupts\n");
	timer_tester(1);
	printf("\n");
}
