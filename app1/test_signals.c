/* SPDX-License-Identifier: MIT */
/*
 * test_signals.c
 *
 * Signal testcases
 *
 * azuepke, 2018-03-19: initial
 */

#include <marron/api.h>
#include <stdio.h>
#include <string.h>
#undef NDEBUG /* always keep assert() acivated */
#include <assert.h>
#include <marron/regs.h>
#include "app.h"

/* raise an illegal instruction */
static void do_illegal(void)
{
#if defined __thumb__
	__asm__ volatile (".inst.n 0xeee8, 0x1a10" : : : "memory");	/* vmsr fpexc, r1 */
#elif defined __arm__
	__asm__ volatile (".inst 0xeee81a10" : : : "memory");	/* vmsr fpexc, r1 */
#else
#error Adapt this file to your CPU architecture!
#endif
}

/* continue execution after illegal instruction  */
static void fix_illegal(regs_t *regs)
{
#if defined __thumb__
	regs->pc += 4;
#elif defined __arm__
	regs->pc += 4;
#else
#error Adapt this file to your CPU architecture!
#endif
}

/* raise a pagefault exception */
static void do_pagefault(void)
{
#if defined __thumb__ || defined __arm__
	addr_t addr = 0xffff0000;
	unsigned long tmp;

	__asm__ volatile ("ldr	%0, [%1]" : "=&r"(tmp) : "r"(addr) : "memory");
#else
#error Adapt this file to your CPU architecture!
#endif
}

/* continue execution after pagefault */
static void fix_pagefault(regs_t *regs)
{
#if defined __thumb__
	regs->pc += 2;
#elif defined __arm__
	regs->pc += 4;
#else
#error Adapt this file to your CPU architecture!
#endif
}

/* signame() */
static const char *signame(unsigned int sig)
{
	switch (sig) {
	case SIG_ILLEGAL:		return "SIG_ILLEGAL";
	case SIG_DEBUG:			return "SIG_DEBUG";
	case SIG_TRAP:			return "SIG_TRAP";
	case SIG_ARITHMETIC:	return "SIG_ARITHMETIC";
	case SIG_PAGEFAULT:		return "SIG_PAGEFAULT";
	case SIG_ALIGN:			return "SIG_ALIGN";
	case SIG_BUS:			return "SIG_BUS";
	case SIG_FPU_UNAVAIL:	return "SIG_FPU_UNAVAIL";
	case SIG_FPU_ERROR:		return "SIG_FPU_ERROR";
	case SIG_SYSCALL:		return "SIG_SYSCALL";
	case SIG_ABORT:			return "SIG_ABORT";
	case SIG_CANCEL:		return "SIG_CANCEL";
	case 30:				return "SIG_30";
	case 31:				return "SIG_31";
	default:				return "SIG_???";
	}
}

static volatile unsigned int sig_last;
static volatile unsigned int sig_count;

static void handler(regs_t *regs, uint32_t sig_mask, unsigned int sig)
{
	sig_last = sig;
	sig_count++;
	printf("got signal %d %s\n", sig, signame(sig));

	switch (sig) {
	case SIG_ILLEGAL:
		fix_illegal(regs);
		break;
	case SIG_CANCEL:
		/* just OK */
		break;
	case SIG_PAGEFAULT:
		fix_pagefault(regs);
		break;
	case 30:
		/* just OK */
		break;
	case 31:
		/* just OK */
		break;
	default:
		printf("got signal %d %s\n", sig, signame(sig));
		assert(0);
		break;
	}

	sys_sig_return(regs, sig_mask);
	/* must not return */
	assert(0);
}

void test_signals(void)
{
	uint32_t sig_mask;
	err_t err;
	print_header("Signal Handling");

	/* register handlers for SIG_ILLEGAL, SIG_CANCEL and signal #31 */
	err = sys_sig_register(SIG_ILLEGAL, handler, SIG_MASK_NONE, 0);
	assert(err == EOK);
	err = sys_sig_register(SIG_CANCEL, handler, SIG_MASK_NONE, 0);
	assert(err == EOK);
	err = sys_sig_register(SIG_PAGEFAULT, handler, SIG_MASK_NONE, 0);
	assert(err == EOK);
	err = sys_sig_register(30, handler, SIG_MASK_ALL, 0);
	assert(err == EOK);
	err = sys_sig_register(31, handler, SIG_MASK_NONE, 0);
	assert(err == EOK);

	sig_count = 0;

	printf("* test illegal instruction: ");
	sig_last = 0;
	do_illegal();
	assert(sig_last == SIG_ILLEGAL);
	assert(sig_count == 1);
	printf("OK\n");

	printf("* test async signal #1: ");
	sig_last = 0;
	err = sys_sig_send(sys_thread_self(), SIG_CANCEL);
	assert(err == EOK);
	assert(sig_last == SIG_CANCEL);
	assert(sig_count == 2);
	printf("OK\n");

	printf("* test async signal #2: ");
	sig_last = 0;
	err = sys_sig_send(sys_thread_self(), 31);
	assert(err == EOK);
	assert(sig_last == 31);
	assert(sig_count == 3);
	printf("OK\n");

	printf("* mask signals: ");
	sig_mask = sys_sig_mask(SIG_MASK_NONE, SIG_TO_MASK(SIG_CANCEL) | SIG_TO_MASK(31));
	assert(sig_mask == 0);
	sig_mask = sys_sig_mask(SIG_MASK_NONE, SIG_MASK_NONE);
	assert(sig_mask == (SIG_TO_MASK(SIG_CANCEL) | SIG_TO_MASK(31)));
	printf("OK\n");

	printf("* test masked signals: ");
	sig_last = 0;
	err = sys_sig_send(sys_thread_self(), 31);
	assert(err == EOK);
	assert(sig_last == 0);
	assert(sig_count == 3);
	err = sys_sig_send(sys_thread_self(), SIG_CANCEL);
	assert(err == EOK);
	/* no signal must be delivered while the signals are masked */
	assert(sig_last == 0);
	assert(sig_count == 3);
	printf("OK\n");

	printf("* test nested signals: ");
	/* unmask both signals, they get delivered in a nested fashion */
	sig_mask = sys_sig_mask(SIG_MASK_ALL, SIG_MASK_NONE);
	assert(sig_mask == (SIG_TO_MASK(SIG_CANCEL) | SIG_TO_MASK(31)));
	/* first, SIG_CANCEL, then signal #31 is delivered */
	/* but signal #31 preempts SIG_CANCEL, so SIG_CANCEL is observed last */
	assert(sig_last == SIG_CANCEL);
	assert(sig_count == 5);
	printf("OK\n");

	printf("* test nested signals with other signals masked in handler: ");
	sig_last = 0;
	/* block all signals first */
	sig_mask = sys_sig_mask(SIG_MASK_NONE, SIG_MASK_ALL);
	assert(sig_mask == SIG_MASK_NONE);
	/* send both signals 30 and 31 */
	err = sys_sig_send(sys_thread_self(), 30);
	assert(err == EOK);
	err = sys_sig_send(sys_thread_self(), 31);
	assert(err == EOK);
	/* unmask both signals, they get delivered in a nested fashion */
	sig_mask = sys_sig_mask(SIG_MASK_ALL, SIG_MASK_NONE);
	assert(sig_mask == SIG_MASK_ALL);
	/* at first, signal #30 and then signal #31 is delivered,
	 * but this time signal #31 cannot preempts #30,
	 * so signal #31 is observed last
	 */
	assert(sig_last == 31);
	assert(sig_count == 7);
	printf("OK\n");

	printf("* test pagefault handler: ");
	sig_last = 0;
	do_pagefault();
	assert(sig_last == SIG_PAGEFAULT);
	assert(sig_count == 8);
	/* unregister handlers */
	printf("OK\n");

	/* unregister handlers */
	err = sys_sig_register(SIG_ILLEGAL, NULL, 0x0, 0);
	assert(err == EOK);
	err = sys_sig_register(SIG_CANCEL, NULL, 0x0, 0);
	assert(err == EOK);
	err = sys_sig_register(SIG_PAGEFAULT, NULL, 0x0, 0);
	assert(err == EOK);
	err = sys_sig_register(30, NULL, 0x0, 0);
	assert(err == EOK);
	err = sys_sig_register(31, NULL, 0x0, 0);
	assert(err == EOK);

	/* restore signal mask */
	sig_mask = sys_sig_mask(SIG_MASK_ALL, SIG_MASK_NONE);
	assert(sig_mask == 0);

	printf("\n");
}
