/* SPDX-License-Identifier: MIT */
/*
 * test_sched.c
 *
 * Example OS -- test code
 *
 * azuepke, 2017-07-17: initial
 * azuepke, 2018-04-10: moved from main.c
 */

#include <marron/api.h>
#include <stdio.h>
#undef NDEBUG /* always keep assert() acivated */
#include <assert.h>
#include "app.h"

static void thread_func1(void *arg __unused)
{
	sys_time_t now;

	printf("* Thread: Hi there!\n");
	now = sys_time_get();
	for (unsigned int i = 1; i <= 10; i++) {
		printf("* Thread: loop %d\n", i);
		now += 500*1000000;
		sys_sleep(now);
	}
	printf("* Thread: exit\n");
	sys_thread_exit();
}

static void thread_func2(void *arg __unused)
{
	unsigned int err;

	printf("* Thread: resume\n");
	err = sys_thread_resume(0);
	assert(err == EOK);

	printf("* Thread: exit\n");
	sys_thread_exit();
}

void test_sched(void)
{
	unsigned int prio;
	unsigned int fpu_state;
	err_t err;

	print_header("Basic Scheduler Tests");

	/* test yield */
	printf("# yield: ");
	sys_yield();
	printf("OK\n");

	/* test CPU changes */
	printf("# cpu_get: %d\n", sys_cpu_get());
	printf("# cpu_set: ");
	err = sys_cpu_set(sys_cpu_get());
	assert(err == EOK);
	printf("OK\n");

	/* test prio changes */
	prio = sys_prio_get();
	printf("# prio_get: %d\n", prio);
	printf("# prio_set: ");
	err = sys_prio_set(prio);
	assert(err == prio);
	assert(prio == sys_prio_get());
	printf("OK\n");

	/* test FPU state changes */
	fpu_state = sys_fpu_state_get();
	printf("# fpu_state_get: %d\n", fpu_state);
	assert(fpu_state == FPU_STATE_AUTO);
	printf("# fpu_state_set(OFF): ");
	err = sys_fpu_state_set(FPU_STATE_OFF);
	assert(err == EOK);
	assert(sys_fpu_state_get() == FPU_STATE_OFF);
	printf("OK\n");
	printf("# fpu_state_set(ON): ");
	err = sys_fpu_state_set(FPU_STATE_ON);
	assert(err == EOK);
	assert(sys_fpu_state_get() == FPU_STATE_ON);
	printf("OK\n");
	printf("# fpu_state_set(AUTO): ");
	err = sys_fpu_state_set(FPU_STATE_AUTO);
	assert(err == EOK);
	assert(sys_fpu_state_get() == FPU_STATE_AUTO);
	printf("OK\n");
	printf("\n");
}

void test_suspend_resume(void)
{
	sys_time_t now;
	err_t err;

	print_header("Thread Suspend / Resume");

	/* test timer + suspend + wakeup */
	create_thread_1(thread_func1, NULL, 50);

	for (unsigned int i = 1; i <= 10; i++) {
		now = sys_time_get();
		printf("# loop %d: now it is %lld ns since boot\n", i, (unsigned long long)now);
		sys_sleep(now + 1000*1000000);
	}

	printf("\n");

	printf("# 2nd test:\n");
	create_thread_1(thread_func2, NULL, 99);

	printf("# suspending now\n");
	err = sys_thread_suspend();
	assert(err == EOK);
	printf("# resumed -> sleep\n");

	sys_sleep(sys_time_get() + 1000*1000000);

	printf("# other should be dead by now\n");
	printf("\n");
}
