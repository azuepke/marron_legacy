/* SPDX-License-Identifier: MIT */
/*
 * test_partition.c
 *
 * Partition testcases
 *
 * awerner, 2018-03-28: initial
 */

#include <marron/api.h>
#include <stdio.h>
#include <string.h>
#undef NDEBUG /* always keep assert() acivated */
#include <assert.h>
#include "app.h"
#include "config.h"
/*#define VERBOSE*/
#ifdef VERBOSE
# define vprint(fmt, ...) printf(fmt, ##__VA_ARGS__)
#else
# define vprint(fmt, ...)
#endif

static void print_partion_state(unsigned int state)
{
#ifdef VERBOSE
	sys_time_t now;
	now = sys_time_get();
	switch (state) {
		case PART_STATE_IDLE:
			vprint("# Part 2 is in IDLE now is %lld\n", now);
			break;
		case PART_STATE_RUNNING:
			vprint("# Part 2 is in RUNNING now is %lld\n", now);
			break;
	}
#else
	(void) state;
#endif
}

void test_partition(volatile uint32_t *shm1)
{
	sys_time_t now;
	err_t err;
	unsigned int state;
	unsigned int oldstate;

	print_header("Partition Implementation");

	for (int i = 0; i < 2; i++) {
		/* test get Partition state */
		printf("# Test get partition state: ");
		vprint("\n");
		vprint("# Get State of Partition 2\n");
		err = sys_part_state_other(2, &state);
		assert(err == EOK);
		print_partion_state(state);
		assert(state == PART_STATE_IDLE);
		vprint("# ");
		printf("ok\n");

		/* test start partition */
		printf("# Test start partition: ");
		vprint("\n");
		vprint("# Start Partition 2\n");
		shm1[2] = 0x0; /* reset spinlock */
		shm1[3] = 0x0; /* select first test */
		shm1[4] = 0x0; /* reset state variable */
		err = sys_part_restart_other(2);
		assert(err == EOK);
		/* let run partition */
		sys_sleep(sys_time_get() + 100*1000000);
		/* check state */
		err = sys_part_state_other(2, &state);
		assert(err == EOK);
		print_partion_state(state);
		assert(state == PART_STATE_RUNNING);
		/* check Partition execute code */
		assert(shm1[4] == 1);
		vprint("# ");
		printf("ok\n");

		/* Test wait for self termination */
		printf("# Test wait for termination: ");
		vprint("\n");
		vprint("# Let Partition 2 run until self termination\n");
		/*start app2*/
		shm1[2] = 0x1;
		/* wait self termination */
		do {
			oldstate = state;
			err = sys_part_state_other(2, &state);
			assert(err == EOK);
			now = sys_time_get();
			if (oldstate != state) {
				print_partion_state(state);
			}
			sys_sleep(now + 1000000);
		} while (state != 0);
		vprint("# ");
		printf("ok\n");

		/* Test restart partition after dead*/
		printf("# Test Restart Partition after termination: ");
		vprint("\n");
		vprint("# Restart Partition 2");
		/* restart Partition */
		shm1[2] = 0x1; /* set partition run throw spinlock */
		shm1[3] = 0x1; /* set part 2 test 2 */
		err = sys_part_restart_other(2);
		assert(err == EOK);
		/* wait a bit */
		sys_sleep(sys_time_get() + 2000*1000000);
		/* check state */
		err = sys_part_state_other(2, &state);
		assert(err == EOK);
		print_partion_state(state);
		assert(state == PART_STATE_RUNNING);
		vprint("# ");
		printf("ok\n");

		/* test Interrupt Partition and restart while running */
		/* partition is running test 2 (endless loop) */
		printf("# Test Interrupt Partition and restart: ");
		vprint("\n");
		vprint("# Interrupt Partition 2 and restart\n");
		/* Interrupt Partion while running */
		shm1[2] = 0x0; /* clear spinlock */
		shm1[3] = 0x1; /* set part 2 test 2 */
		shm1[4] = 0x0; /* reset state variable */
		err = sys_part_restart_other(2);
		assert(err == EOK);
		sys_sleep(sys_time_get() + 100*1000000);
		err = sys_part_state_other(2, &state);
		assert(err == EOK);
		print_partion_state(state);
		assert(state == PART_STATE_RUNNING);
		assert(shm1[4] == 1);
		vprint("# ");
		printf("ok\n");

		/* test kill partition while running */
		/* partition is running test 2 (endless loop) */
		printf("# Test Kill Partition while running: ");
		vprint("\n");
		shm1[2] = 0x1; /* start endless loop */
		sys_sleep(sys_time_get() + 2000*1000000);
		vprint("# Kill Partition 2 \n");
		/* kill Partition */
		err = sys_part_shutdown_other(2);
		assert(err == EOK);
		err = sys_part_state_other(2, &state);
		assert(err == EOK);
		print_partion_state(state);
		assert(state == PART_STATE_IDLE);
		vprint("# ");
		printf("ok\n");

		/* test partition self restart */
		printf("# Test Partition self restart: ");
		vprint("\n");
		vprint("# Restart Partition\n");
		shm1[2] = 0x0;
		shm1[3] = 0x2; /* set part 2 test 3 (self restart) */
		shm1[4] = 0x0;
		err = sys_part_restart_other(2);
		assert(err == EOK);
		sys_sleep(sys_time_get() + 1000ULL*1000000ULL);
		err = sys_part_state_other(2, &state);
		assert(err == EOK);
		print_partion_state(state);
		assert(state == PART_STATE_RUNNING);
		/* check Partion is in init */
		assert(shm1[4] == 1);
		vprint("# State Varable: %u\n", shm1[4]);
		vprint("# Start self restart test\n");
		shm1[2] = 0x1; /* start test */
		sys_sleep(sys_time_get() + 1000ULL*1000000ULL);
		err = sys_part_state_other(2, &state);
		assert(err == EOK);
		print_partion_state(state);
		assert(state == PART_STATE_RUNNING);
		sys_sleep(sys_time_get() + 3000ULL*1000000ULL);
		err = sys_part_state_other(2, &state);
		assert(err == EOK);
		print_partion_state(state);
		assert(state == PART_STATE_RUNNING);
		vprint("# State Varable: %u\n", shm1[4]);
		/* check Partion is in init */
		assert(shm1[4] == 2);
		vprint("# ");
		printf("ok\n");

		/* test inter-partition communication */
		printf("# Test inter-partition communication: ");
		vprint("\n");
		vprint("# Restart Partition");
		shm1[2] = 0x1; /* run to wait */
		shm1[3] = 0x3; /* set part 2 test 4 (ping / pong) */
		shm1[4] = 0x0;
		err = sys_part_restart_other(2);
		assert(err == EOK);
		sys_sleep(sys_time_get() + 1000ULL*1000000ULL);
		err = sys_part_state_other(2, &state);
		assert(err == EOK);
		print_partion_state(state);
		assert(state == PART_STATE_RUNNING);
		/* check Partion is in init */
		assert(shm1[4] == 1);
		vprint("# send ping\n");
		err = sys_event_send(CFG_EVENT_SEND_ping);
		assert(err == EOK);
		vprint("# wait for pong\n");
		err = sys_event_wait(CFG_EVENT_WAIT_pong, 0, sys_time_get() + 100*1000000);
		assert(err == EOK);
		vprint("# ");
		printf("ok\n");

		/* Test Partition Permissions */
		shm1[2] = 0x1; /* run to test */
		shm1[3] = 0x4; /* set part 2 test 5 (Permission Test) */
		shm1[4] = 0x0;
		err = sys_part_restart_other(2);
		assert(err == EOK);
		/* Test send event on finish */
		err = sys_event_wait(CFG_EVENT_WAIT_pong, 0, sys_time_get() + 100*1000000);
		assert(err == EOK);

		/* shutdown partition after last test */
		err = sys_part_shutdown_other(2);
		assert(err == EOK);
		err = sys_part_state_other(2, &state);
		assert(err == EOK);
		print_partion_state(state);
		assert(state == PART_STATE_IDLE);
	}
}
