/* SPDX-License-Identifier: MIT */
/*
 * main.c
 *
 * Example OS -- test code
 *
 * azuepke, 2017-07-17: initial
 */

#include <marron/api.h>
#include <stack.h>
#include <stdio.h>
#include <string.h>
#undef NDEBUG /* always keep assert() acivated */
#include <assert.h>
#include "app.h"

/* stacks */
#define STACK_SIZE 2048
DEFINE_STACK(main_stack, STACK_SIZE);
DEFINE_STACK(thread_stack, STACK_SIZE);

void create_thread_1(void (*func)(void *), void *arg, unsigned int prio)
{
	err_t err;

	err = sys_thread_create(1, func, arg, GET_STACK(thread_stack), NULL, prio, 0);
	assert(err == EOK);
}

void print_line(void)
{
	printf("************************************************************************\n");
}

void print_header(const char *s)
{
	printf("\n");
	print_line();
	printf("*** Testcase: %s\n", s);
	print_line();
}

unsigned long long var64 = 0x0123456789abcdef;

extern uint32_t _shm1_start[];

void main(void *arg __unused)
{
	volatile uint32_t *shm1;
	char bsp_name[32];
	err_t err;

	printf("\n");
	print_line();
	printf("*** TESTSUITE\n");
	print_line();

	/*
	 * shm1[0] == Magic Word (0x42424242)
	 * shm1[1] == Test Counter
	 * shm1[2] == Spinlock for Part 2
	 * shm1[3] == Test Select for Part 2
	 * shm1[4] == Part 2 Test State Variable
	 */
	shm1 = (volatile uint32_t *) _shm1_start;
	printf("# SHM1 addess: %p\n", shm1);
	/* check shm is init */
	if (shm1[0] != 0x42424242) {
		/* init shm */
		shm1[0] = 0x42424242;
		shm1[1] = 0;
		shm1[2] = 0;
		shm1[3] = 0;
		shm1[4] = 0;
	}

	printf("# Hello World from user space\n");
	/* test crt0, i.e. if the data segment is set up properly */
	printf("# Variable var64 is 0x%llx\n", var64);
	assert(var64 == 0x0123456789abcdef);

	printf("# partition ID %d state %d\n", sys_part_self(), sys_part_state());
	assert(sys_part_state() == PART_STATE_RUNNING);

	err = sys_bsp_name(bsp_name, sizeof(bsp_name));
	assert(err == EOK);
	printf("# bsp name: %s\n", bsp_name);
	printf("\n");

	/* benchmarks */
	test_bench_cpu();
	test_bench_api();

	/* run tests */
	test_sched();
	test_suspend_resume();
	test_signals();
	test_timer_irq();
	test_futex();
	test_event();
	test_partition(shm1);

	printf("### all tests done! ###\n");
	printf("Test Count: %u \n", shm1[1] + 1);
#ifndef AUTOBUILD
	shm1[1]++;
	printf("# last Test restart partion self\n");
	sys_part_restart();
#else
	if (shm1[1]++ < 1) {
		printf("# last Test restart partion self\n");
		sys_part_restart();
	} else {
		printf("# Shutdown\n");
		/* poweroff */
		sys_bsp_shutdown(2);
	}
#endif
}
