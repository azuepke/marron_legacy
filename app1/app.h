/* SPDX-License-Identifier: MIT */
/*
 * app.h
 *
 * Application main include file
 *
 * azuepke, 2018-03-19: initial
 */

#ifndef __APP_H__
#define __APP_H__

/* main.c */
void create_thread_1(void (*func)(void *), void *arg, unsigned int prio);
void print_line(void);
void print_header(const char *s);

/* various */
void test_sched(void);
void test_suspend_resume(void);
void test_timer_irq(void);
void test_signals(void);
void test_futex(void);
void test_event(void);
void test_partition(volatile uint32_t *shm1);

/* test_bench_* */
void test_bench_cpu(void);
void test_bench_api(void);

#endif
