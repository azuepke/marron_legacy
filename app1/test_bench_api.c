/*
 * test_bench_api.c
 *
 * CPU core benchmark
 *
 * azuepke, 2014-03-04: initial
 * azuepke, 2015-01-26: add low-level syscall benchmarks
 * azuepke, 2020-01-31: adapted for Marron
 */

#include <stdint.h>
#include <stdio.h>
#include <marron/api.h>
#include <compiler.h>
#include "app.h"
#include "assert.h"
#include "bench.h"
#include <syscalls.h>	/* for NUM_SYSCALLS */


void test_bench_api(void)
{
	TIME_T before, after;
	unsigned int runs;
	uint32_t futex;

	printf("\n*** core API:\n");

	printf("- null syscall\t\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_thread_self();
	}
	after = GETTS();
	delta(before, after);


	printf("- GETTS()\t\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		GETTS();
	}
	after = GETTS();
	delta(before, after);


	printf("- sys_gettime()\t\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_time_get();
	}
	after = GETTS();
	delta(before, after);

	printf("- sys_ulock_wake(&ulock, 0)\t\t\t\t");
	before = GETTS();
	futex = 0;
	for (runs = 0; runs < RUNS; runs++) {
		sys_futex_wake(&futex, 0);
	}
	after = GETTS();
	delta(before, after);


	printf("- sys_ulock_wake(NULL, 0)\t\t\t\t");
	before = GETTS();
	futex = 0;
	for (runs = 0; runs < RUNS; runs++) {
		sys_futex_wake(NULL, 0);
	}
	after = GETTS();
	delta(before, after);

	printf("- sys_yield()\t\t\t\t\t\t");
	before = GETTS();
	for (runs = 0; runs < RUNS; runs++) {
		sys_yield();
	}
	after = GETTS();
	delta(before, after);
}
